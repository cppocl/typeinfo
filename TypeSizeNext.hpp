/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPESIZENEXT_HPP
#define OCL_GUARD_TYPEINFO_TYPESIZENEXT_HPP

#include <cstdint>

namespace ocl
{

template<typename Type>
struct TypeSizeNext;

template<>
struct TypeSizeNext<int8_t>
{
    typedef std::int8_t type;
    typedef std::int16_t next_type;
};

template<>
struct TypeSizeNext<uint8_t>
{
    typedef std::uint8_t type;
    typedef std::uint16_t next_type;
};

template<>
struct TypeSizeNext<int16_t>
{
    typedef std::int16_t type;
    typedef std::int32_t next_type;
};

template<>
struct TypeSizeNext<uint16_t>
{
    typedef std::uint16_t type;
    typedef std::uint32_t next_type;
};

template<>
struct TypeSizeNext<int32_t>
{
    typedef std::int32_t type;
    typedef std::int64_t next_type;
};

template<>
struct TypeSizeNext<uint32_t>
{
    typedef std::uint32_t type;
    typedef std::uint64_t next_type;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPESIZENEXT_HPP
