/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeName.hpp"
#include "../TypePrevious.hpp"
#include <string.h>

using ocl::TypePrevious;
using ocl::TypeName;

TEST_MEMBER_FUNCTION(TypePrevious, previous_type, NA)
{
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<signed short>::previous_type>::GetName(), "signed char"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<unsigned short>::previous_type>::GetName(), "unsigned char"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<signed int>::previous_type>::GetName(), "signed short"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<unsigned int>::previous_type>::GetName(), "unsigned short"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<signed long int>::previous_type>::GetName(), "signed int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<unsigned long int>::previous_type>::GetName(), "unsigned int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<signed long long int>::previous_type>::GetName(), "signed long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<unsigned long long int>::previous_type>::GetName(), "unsigned long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<double>::previous_type>::GetName(), "float"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypePrevious<long double>::previous_type>::GetName(), "double"), 0);
}
