/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEPOINTER_HPP
#define OCL_GUARD_TYPEINFO_TYPEPOINTER_HPP

#include "TypeReference.hpp"

namespace ocl
{

template<typename Type>
struct TypePointer
{
    typedef Type type;
    typedef typename TypeReference<Type>::no_ref_type  no_pointer_type;
    typedef typename TypeReference<Type>::no_ref_type* pointer_type;

    static bool const is_pointer = false;
};

template<typename Type>
struct TypePointer<Type*>
{
    typedef Type* type;
    typedef Type  no_pointer_type;
    typedef Type* pointer_type;

    static bool const is_pointer = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEPOINTER_HPP
