/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEPRINTFFORMAT_HPP
#define OCL_GUARD_TYPEINFO_TYPEPRINTFFORMAT_HPP

#include <cstddef>

namespace ocl
{

template<typename FmtType, typename CharType>
struct TypePrintFormat
{
    typedef FmtType fmt_type;   // data type being converted to string.
    typedef CharType char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef FmtType promotion_type;
};

template<typename FmtType>
struct TypePrintFormat<FmtType, char>
{
    typedef FmtType fmt_type; // data type being converted to string.
    typedef char char_type;   // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef FmtType promotion_type;

    // Unsupported types return '\0' for char and null for string.
    static char_type GetFormatChar() throw() { return '\0'; }           // e.g. 'd' part of "%d".
    static char_type const* GetLengthString() throw() { return ""; }    // e.g. "ll" part of "%lld".
    static char_type const* GetFormatString() throw() { return ""; }    // e.g. "%lld".
};

template<typename FmtType>
struct TypePrintFormat<FmtType, wchar_t>
{
    typedef FmtType fmt_type;  // data type being converted to string.
    typedef wchar_t char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef FmtType promotion_type;

    // Unsupported types return '\0' for char and null for string.
    static char_type GetFormatChar() throw() { return L'\0'; }           // e.g. L'd' part of L"%d".
    static char_type const* GetLengthString() throw() { return L""; }    // e.g. L"ll" part of L"%lld".
    static char_type const* GetFormatString() throw() { return L""; }    // e.g. L"%lld".
};

template<>
struct TypePrintFormat<char, char>
{
    typedef char fmt_type;  // data type being converted to string.
    typedef char char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() throw() { return 'c'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%c"; }
};

template<>
struct TypePrintFormat<char, wchar_t>
{
    typedef char fmt_type;     // data type being converted to string.
    typedef wchar_t char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() throw() { return L'c'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%c"; }
};

template<>
struct TypePrintFormat<wchar_t, char>
{
    typedef wchar_t fmt_type; // data type being converted to string.
    typedef char char_type;   // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() throw() { return 'c'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%c"; }
};

template<>
struct TypePrintFormat<wchar_t, wchar_t>
{
    typedef wchar_t fmt_type;  // data type being converted to string.
    typedef wchar_t char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() throw() { return L'c'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%c"; }
};

template<>
struct TypePrintFormat<signed char, char>
{
    typedef signed char fmt_type; // data type being converted to string.
    typedef char char_type;       // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() throw() { return 'd'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%d"; }
};

template<>
struct TypePrintFormat<signed char, wchar_t>
{
    typedef signed char fmt_type; // data type being converted to string.
    typedef wchar_t char_type;    // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() throw() { return L'd'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%d"; }
};

template<>
struct TypePrintFormat<unsigned char, char>
{
    typedef unsigned char fmt_type; // data type being converted to string.
    typedef char char_type;         // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() throw() { return 'u'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%u"; }
};

template<>
struct TypePrintFormat<unsigned char, wchar_t>
{
    typedef unsigned char fmt_type; // data type being converted to string.
    typedef wchar_t char_type;      // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() throw() { return L'u'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%u"; }
};

template<>
struct TypePrintFormat<signed short, char>
{
    typedef signed short fmt_type; // data type being converted to string.
    typedef char char_type;        // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() throw() { return 'd'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%d"; }
};

template<>
struct TypePrintFormat<signed short, wchar_t>
{
    typedef signed short fmt_type; // data type being converted to string.
    typedef wchar_t char_type;     // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() throw() { return L'd'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%d"; }
};

template<>
struct TypePrintFormat<unsigned short, char>
{
    typedef unsigned short fmt_type; // data type being converted to string.
    typedef char char_type;          // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() throw() { return 'u'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%u"; }
};

template<>
struct TypePrintFormat<unsigned short, wchar_t>
{
    typedef unsigned short fmt_type; // data type being converted to string.
    typedef wchar_t char_type;       // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() throw() { return L'u'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%u"; }
};

template<>
struct TypePrintFormat<signed int, char>
{
    typedef signed int fmt_type; // data type being converted to string.
    typedef char char_type;      // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() throw() { return 'd'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%d"; }
};

template<>
struct TypePrintFormat<signed int, wchar_t>
{
    typedef signed int fmt_type; // data type being converted to string.
    typedef wchar_t char_type;   // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() throw() { return L'd'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%d"; }
};

template<>
struct TypePrintFormat<unsigned int, char>
{
    typedef unsigned int fmt_type; // data type being converted to string.
    typedef char char_type;        // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() throw() { return 'u'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString() throw() { return "%u"; }
};

template<>
struct TypePrintFormat<unsigned int, wchar_t>
{
    typedef unsigned int fmt_type; // data type being converted to string.
    typedef wchar_t char_type;     // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() throw() { return L'u'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString() throw() { return L"%u"; }
};

template<>
struct TypePrintFormat<signed long int, char>
{
    typedef signed long int fmt_type; // data type being converted to string.
    typedef char char_type;           // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed long int promotion_type;

    static char_type GetFormatChar() throw() { return 'd'; }
    static char_type const* GetLengthString() throw() { return "l"; }
    static char_type const* GetFormatString() throw() { return "%ld"; }
};

template<>
struct TypePrintFormat<signed long int, wchar_t>
{
    typedef signed long int fmt_type; // data type being converted to string.
    typedef wchar_t char_type;        // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed long int promotion_type;

    static char_type GetFormatChar() throw() { return L'd'; }
    static char_type const* GetLengthString() throw() { return L"l"; }
    static char_type const* GetFormatString() throw() { return L"%ld"; }
};

template<>
struct TypePrintFormat<unsigned long int, char>
{
    typedef unsigned long int fmt_type; // data type being converted to string.
    typedef char char_type;             // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long int promotion_type;

    static char_type GetFormatChar() throw() { return 'u'; }
    static char_type const* GetLengthString() throw() { return "l"; }
    static char_type const* GetFormatString() throw() { return "%lu"; }
};

template<>
struct TypePrintFormat<unsigned long int, wchar_t>
{
    typedef unsigned long int fmt_type; // data type being converted to string.
    typedef wchar_t char_type;          // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long int promotion_type;

    static char_type GetFormatChar() throw() { return L'u'; }
    static char_type const* GetLengthString() throw() { return L"l"; }
    static char_type const* GetFormatString() throw() { return L"%lu"; }
};

template<>
struct TypePrintFormat<signed long long int, char>
{
    typedef signed long long int fmt_type; // data type being converted to string.
    typedef char char_type;                // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed long long int promotion_type;

    static char_type GetFormatChar() throw() { return 'd'; }
    static char_type const* GetLengthString() throw() { return "ll"; }
    static char_type const* GetFormatString() throw() { return "%lld"; }
};

template<>
struct TypePrintFormat<signed long long int, wchar_t>
{
    typedef signed long long int fmt_type; // data type being converted to string.
    typedef wchar_t char_type;             // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef signed long long int promotion_type;

    static char_type GetFormatChar() throw() { return L'd'; }
    static char_type const* GetLengthString() throw() { return L"ll"; }
    static char_type const* GetFormatString() throw() { return L"%lld"; }
};

template<>
struct TypePrintFormat<unsigned long long int, char>
{
    typedef unsigned long long int fmt_type; // data type being converted to string.
    typedef char char_type;                  // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long long int promotion_type;

    static char_type GetFormatChar() throw() { return 'u'; }
    static char_type const* GetLengthString() throw() { return "ll"; }
    static char_type const* GetFormatString() throw() { return "%llu"; }
};

template<>
struct TypePrintFormat<unsigned long long int, wchar_t>
{
    typedef unsigned long long int fmt_type; // data type being converted to string.
    typedef wchar_t char_type;               // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long long int promotion_type;

    static char_type GetFormatChar() throw() { return L'u'; }
    static char_type const* GetLengthString() throw() { return L"ll"; }
    static char_type const* GetFormatString() throw() { return L"%llu"; }
};

template<>
struct TypePrintFormat<float, char>
{
    typedef float fmt_type; // data type being converted to string.
    typedef char char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar(bool const is_lowercase = true) throw() { return is_lowercase ? 'f' : 'F'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString(bool const is_lowercase = true) throw() { return is_lowercase ? "%f" : "%F"; }
};

template<>
struct TypePrintFormat<float, wchar_t>
{
    typedef float fmt_type;    // data type being converted to string.
    typedef wchar_t char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar(bool const is_lowercase = true) throw() { return is_lowercase ? L'f' : L'F'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString(bool const is_lowercase = true) throw() { return is_lowercase ? L"%f" : L"%F"; }
};

template<>
struct TypePrintFormat<double, char>
{
    typedef double fmt_type; // data type being converted to string.
    typedef char char_type;  // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar(bool const is_lowercase = true) throw() { return is_lowercase ? 'f' : 'F'; }
    static char_type const* GetLengthString() throw() { return ""; }
    static char_type const* GetFormatString(bool const is_lowercase = true) throw() { return is_lowercase ? "%f" : "%F"; }
};

template<>
struct TypePrintFormat<double, wchar_t>
{
    typedef double fmt_type;   // data type being converted to string.
    typedef wchar_t char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar(bool const is_lowercase = true) throw() { return is_lowercase ? L'f' : L'F'; }
    static char_type const* GetLengthString() throw() { return L""; }
    static char_type const* GetFormatString(bool const is_lowercase = true) throw() { return is_lowercase ? L"%f" : L"%F"; }
};

template<>
struct TypePrintFormat<long double, char>
{
    typedef long double fmt_type; // data type being converted to string.
    typedef char char_type;       // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef long double promotion_type;

    static char_type GetFormatChar(bool const is_lowercase = true) throw() { return is_lowercase ? 'f' : 'F'; }
    static char_type const* GetLengthString() throw() { return "L"; }
    static char_type const* GetFormatString(bool const is_lowercase = true) throw() { return is_lowercase ? "%Lf" : "%LF"; }
};

template<>
struct TypePrintFormat<long double, wchar_t>
{
    typedef long double fmt_type; // data type being converted to string.
    typedef wchar_t char_type;    // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef long double promotion_type;

    static char_type GetFormatChar(bool const is_lowercase = true) throw() { return is_lowercase ? L'f' : L'F'; }
    static char_type const* GetLengthString() throw() { return L"L"; }
    static char_type const* GetFormatString(bool const is_lowercase = true) throw() { return is_lowercase ? L"%Lf" : L"%LF"; }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEPRINTFFORMAT_HPP
