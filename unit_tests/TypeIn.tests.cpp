/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIn.hpp"
#include "../TypeIsSame.hpp"

using ocl::TypeIn;
using ocl::TypeIsSame;

namespace
{

struct Huge
{
    double data[1000]; // This won't be smaller than a void*
};

}

TEST_MEMBER_FUNCTION(TypeIn, in_type, NA)
{
    CHECK_TRUE((TypeIsSame<bool, typename TypeIn<bool>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<char, TypeIn<char>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<wchar_t, TypeIn<wchar_t>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<signed char, TypeIn<signed char>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<unsigned char, TypeIn<unsigned char>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<signed short, TypeIn<signed short>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<unsigned short, TypeIn<unsigned short>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<signed int, TypeIn<signed int>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<unsigned int, TypeIn<unsigned int>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<signed long int, TypeIn<signed long int>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<unsigned long int, TypeIn<unsigned long int>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<signed long long int, TypeIn<signed long long int>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<unsigned long long int, TypeIn<unsigned long long int>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<float, TypeIn<float>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<double, TypeIn<double>::in_type>::is_same));
    CHECK_TRUE((TypeIsSame<long double, TypeIn<long double>::in_type>::is_same));
    CHECK_FALSE((TypeIsSame<Huge, TypeIn<Huge>::in_type>::is_same));
}
