/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeHasNext.hpp"

namespace
{

struct Huge
{
    double data[1000];
};

}

using ocl::TypeSizeHasNext;

TEST_MEMBER_FUNCTION(TypeSizeHasNext, has_next, NA)
{
    CHECK_FALSE(TypeSizeHasNext<bool>::has_next);
    CHECK_FALSE(TypeSizeHasNext<char>::has_next);
    CHECK_FALSE(TypeSizeHasNext<wchar_t>::has_next);
    CHECK_TRUE(TypeSizeHasNext<int8_t>::has_next);
    CHECK_TRUE(TypeSizeHasNext<uint8_t>::has_next);
    CHECK_TRUE(TypeSizeHasNext<int16_t>::has_next);
    CHECK_TRUE(TypeSizeHasNext<uint16_t>::has_next);
    CHECK_TRUE(TypeSizeHasNext<int32_t>::has_next);
    CHECK_TRUE(TypeSizeHasNext<uint32_t>::has_next);
    CHECK_FALSE(TypeSizeHasNext<int64_t>::has_next);
    CHECK_FALSE(TypeSizeHasNext<uint64_t>::has_next);
    CHECK_FALSE(TypeSizeHasNext<float>::has_next);
    CHECK_FALSE(TypeSizeHasNext<double>::has_next);
    CHECK_FALSE(TypeSizeHasNext<long double>::has_next);
    CHECK_FALSE(TypeSizeHasNext<Huge>::has_next);
}
