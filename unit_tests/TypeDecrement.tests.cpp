/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeLimits.hpp"
#include "../TypeDecrement.hpp"

using ocl::TypeLimits;
using ocl::TypeDecrement;

TEST_MEMBER_FUNCTION(TypeDecrement, Decrement, type_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME("Decrement<type, void>");
    TEST_OVERRIDE_ARGS("type&");

    {
        typedef signed char type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned char type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed short type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned short type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef float type;

        type value = 0.0f;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1.0f);
    }

    {
        typedef double type;

        type value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1.0);
    }

    {
        typedef long double type;

        type value = 0;
        TypeDecrement<type>::Decrement(value);
        CHECK_EQUAL(value, -1.0l);
    }
}

TEST_MEMBER_FUNCTION(TypeDecrement, Decrement_bool, type_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME("Decrement<type, bool>");
    TEST_OVERRIDE_ARGS("type&");

    {
        typedef signed char type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned char type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed short type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned short type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        CHECK_FALSE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef float type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = 0.0f;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1.0f);
    }

    {
        typedef double type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = 0.0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1.0);
    }

    {
        typedef long double type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = 0;
        CHECK_TRUE(type_decrement_type::Decrement(value));
        CHECK_EQUAL(value, -1.0l);
    }
}

TEST_MEMBER_FUNCTION(TypeDecrement, Decrement, type_ref_type)
{
    TEST_OVERRIDE_FUNCTION_NAME("Decrement<type, void>");
    TEST_OVERRIDE_ARGS("type&, type");

    {
        typedef signed char type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned char type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed short type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned short type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long long int type;

        type value = TypeLimits<type>::MAX;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        TypeDecrement<type>::Decrement(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        TypeDecrement<type>::Decrement(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef float type;

        type value = 0.0f;
        TypeDecrement<type>::Decrement(value, 1.0f);
        CHECK_EQUAL(value, -1.0f);
    }

    {
        typedef double type;

        type value = 0.0;
        TypeDecrement<type>::Decrement(value, 1.0);
        CHECK_EQUAL(value, -1.0);
    }

    {
        typedef long double type;

        type value = 0.0l;
        TypeDecrement<type>::Decrement(value, 1.0l);
        CHECK_EQUAL(value, -1.0l);
    }
}

TEST_MEMBER_FUNCTION(TypeDecrement, Decrement_bool, type_ref_type)
{
    TEST_OVERRIDE_FUNCTION_NAME("Decrement<type, bool>");
    TEST_OVERRIDE_ARGS("type&, type");

    bool success;

    {
        typedef signed char type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned char type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed short type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned short type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef signed long long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = 0;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef unsigned long long int type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = TypeLimits<type>::MAX;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MAX - 1);

        value = TypeLimits<type>::MIN + 1;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN + 2;
        success = type_decrement_type::Decrement(value, 2);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);

        value = TypeLimits<type>::MIN;
        success = type_decrement_type::Decrement(value, 1);
        CHECK_FALSE(success);
        CHECK_EQUAL(value, TypeLimits<type>::MIN);
    }

    {
        typedef float type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        float value = 0.0f;
        success = type_decrement_type::Decrement(value, 1.0f);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1.0f);
    }

    {
        typedef double type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = 0.0;
        success = type_decrement_type::Decrement(value, 1.0);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1.0);
    }

    {
        typedef long double type;
        typedef TypeDecrement<type, bool> type_decrement_type;

        type value = 0.0l;
        success = type_decrement_type::Decrement(value, 1.0l);
        CHECK_TRUE(success);
        CHECK_EQUAL(value, -1.0l);
    }
}
