/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypePrintfFormat.hpp"
#include <string.h>

using ocl::TypePrintFormat;

namespace
{

int widecmp(wchar_t const* str1, wchar_t const* str2)
{
    while ((*str1 != L'\0') && (*str2 != L'\0'))
    {
        ++str1;
        ++str2;
    }
    return static_cast<int>(*str1 - *str2);
}

}

TEST_MEMBER_FUNCTION(TypePrintFormat, GetFormatChar, NA)
{
    {
        typedef char char_type;
        char_type fmt_char = TypePrintFormat<bool, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, '\0');
        fmt_char = TypePrintFormat<char, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'c');
        fmt_char = TypePrintFormat<wchar_t, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'c');
        fmt_char = TypePrintFormat<signed char, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'd');
        fmt_char = TypePrintFormat<unsigned char, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'u');
        fmt_char = TypePrintFormat<signed short, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'd');
        fmt_char = TypePrintFormat<unsigned short, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'u');
        fmt_char = TypePrintFormat<signed int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'd');
        fmt_char = TypePrintFormat<unsigned int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'u');
        fmt_char = TypePrintFormat<signed long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'd');
        fmt_char = TypePrintFormat<unsigned long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'u');
        fmt_char = TypePrintFormat<signed long long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'd');
        fmt_char = TypePrintFormat<unsigned long long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, 'u');
        fmt_char = TypePrintFormat<float, char_type>::GetFormatChar(true);
        CHECK_EQUAL(fmt_char, 'f');
        fmt_char = TypePrintFormat<float, char_type>::GetFormatChar(false);
        CHECK_EQUAL(fmt_char, 'F');
        fmt_char = TypePrintFormat<double, char_type>::GetFormatChar(true);
        CHECK_EQUAL(fmt_char, 'f');
        fmt_char = TypePrintFormat<double, char_type>::GetFormatChar(false);
        CHECK_EQUAL(fmt_char, 'F');
        fmt_char = TypePrintFormat<long double, char_type>::GetFormatChar(true);
        CHECK_EQUAL(fmt_char, 'f');
        fmt_char = TypePrintFormat<long double, char_type>::GetFormatChar(false);
        CHECK_EQUAL(fmt_char, 'F');
    }

    {
        typedef wchar_t char_type;
        char_type fmt_char = TypePrintFormat<bool, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'\0');
        fmt_char = TypePrintFormat<char, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'c');
        fmt_char = TypePrintFormat<wchar_t, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'c');
        fmt_char = TypePrintFormat<signed char, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'd');
        fmt_char = TypePrintFormat<unsigned char, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'u');
        fmt_char = TypePrintFormat<signed short, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'd');
        fmt_char = TypePrintFormat<unsigned short, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'u');
        fmt_char = TypePrintFormat<signed int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'd');
        fmt_char = TypePrintFormat<unsigned int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'u');
        fmt_char = TypePrintFormat<signed long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'd');
        fmt_char = TypePrintFormat<unsigned long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'u');
        fmt_char = TypePrintFormat<signed long long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'd');
        fmt_char = TypePrintFormat<unsigned long long int, char_type>::GetFormatChar();
        CHECK_EQUAL(fmt_char, L'u');
        fmt_char = TypePrintFormat<float, char_type>::GetFormatChar(true);
        CHECK_EQUAL(fmt_char, L'f');
        fmt_char = TypePrintFormat<float, char_type>::GetFormatChar(false);
        CHECK_EQUAL(fmt_char, L'F');
        fmt_char = TypePrintFormat<double, char_type>::GetFormatChar(true);
        CHECK_EQUAL(fmt_char, L'f');
        fmt_char = TypePrintFormat<double, char_type>::GetFormatChar(false);
        CHECK_EQUAL(fmt_char, L'F');
        fmt_char = TypePrintFormat<long double, char_type>::GetFormatChar(true);
        CHECK_EQUAL(fmt_char, L'f');
        fmt_char = TypePrintFormat<long double, char_type>::GetFormatChar(false);
        CHECK_EQUAL(fmt_char, L'F');
    }
}

TEST_MEMBER_FUNCTION(TypePrintFormat, GetLengthString, NA)
{
    {
        typedef char char_type;
        char_type const* len_str = TypePrintFormat<bool, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<char, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<wchar_t, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<signed char, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<unsigned char, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<signed short, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<unsigned short, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<signed int, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<unsigned int, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<signed long int, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, "l"), 0);
        len_str = TypePrintFormat<unsigned long int, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, "l"), 0);
        len_str = TypePrintFormat<signed long long int, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, "ll"), 0);
        len_str = TypePrintFormat<unsigned long long int, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, "ll"), 0);
        len_str = TypePrintFormat<float, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<double, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, ""), 0);
        len_str = TypePrintFormat<long double, char_type>::GetLengthString();
        CHECK_EQUAL(::strcmp(len_str, "L"), 0);
    }

    {
        typedef wchar_t char_type;
        char_type const* len_str = TypePrintFormat<bool, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<char, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<wchar_t, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<signed char, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<unsigned char, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<signed short, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<unsigned short, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<signed int, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<unsigned int, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<signed long int, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L"l"), 0);
        len_str = TypePrintFormat<unsigned long int, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L"l"), 0);
        len_str = TypePrintFormat<signed long long int, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L"ll"), 0);
        len_str = TypePrintFormat<unsigned long long int, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L"ll"), 0);
        len_str = TypePrintFormat<float, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<double, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L""), 0);
        len_str = TypePrintFormat<long double, char_type>::GetLengthString();
        CHECK_EQUAL(widecmp(len_str, L"L"), 0);
    }
}

TEST_MEMBER_FUNCTION(TypePrintFormat, GetFormatString, NA)
{
    {
        typedef char char_type;
        char_type const* fmt_str = TypePrintFormat<bool, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, ""), 0);
        fmt_str = TypePrintFormat<char, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%c"), 0);
        fmt_str = TypePrintFormat<wchar_t, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%c"), 0);
        fmt_str = TypePrintFormat<signed char, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%d"), 0);
        fmt_str = TypePrintFormat<unsigned char, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%u"), 0);
        fmt_str = TypePrintFormat<signed short, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%d"), 0);
        fmt_str = TypePrintFormat<unsigned short, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%u"), 0);
        fmt_str = TypePrintFormat<signed int, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%d"), 0);
        fmt_str = TypePrintFormat<unsigned int, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%u"), 0);
        fmt_str = TypePrintFormat<signed long int, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%ld"), 0);
        fmt_str = TypePrintFormat<unsigned long int, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%lu"), 0);
        fmt_str = TypePrintFormat<signed long long int, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%lld"), 0);
        fmt_str = TypePrintFormat<unsigned long long int, char_type>::GetFormatString();
        CHECK_EQUAL(::strcmp(fmt_str, "%llu"), 0);
        fmt_str = TypePrintFormat<float, char_type>::GetFormatString(true);
        CHECK_EQUAL(::strcmp(fmt_str, "%f"), 0);
        fmt_str = TypePrintFormat<float, char_type>::GetFormatString(false);
        CHECK_EQUAL(::strcmp(fmt_str, "%F"), 0);
        fmt_str = TypePrintFormat<double, char_type>::GetFormatString(true);
        CHECK_EQUAL(::strcmp(fmt_str, "%f"), 0);
        fmt_str = TypePrintFormat<double, char_type>::GetFormatString(false);
        CHECK_EQUAL(::strcmp(fmt_str, "%F"), 0);
        fmt_str = TypePrintFormat<long double, char_type>::GetFormatString(true);
        CHECK_EQUAL(::strcmp(fmt_str, "%Lf"), 0);
        fmt_str = TypePrintFormat<long double, char_type>::GetFormatString(false);
        CHECK_EQUAL(::strcmp(fmt_str, "%LF"), 0);
    }

    {
        typedef wchar_t char_type;
        char_type const* fmt_str = TypePrintFormat<bool, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L""), 0);
        fmt_str = TypePrintFormat<char, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%c"), 0);
        fmt_str = TypePrintFormat<wchar_t, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%c"), 0);
        fmt_str = TypePrintFormat<signed char, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%d"), 0);
        fmt_str = TypePrintFormat<unsigned char, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%u"), 0);
        fmt_str = TypePrintFormat<signed short, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%d"), 0);
        fmt_str = TypePrintFormat<unsigned short, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%u"), 0);
        fmt_str = TypePrintFormat<signed int, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%d"), 0);
        fmt_str = TypePrintFormat<unsigned int, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%u"), 0);
        fmt_str = TypePrintFormat<signed long int, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%ld"), 0);
        fmt_str = TypePrintFormat<unsigned long int, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%lu"), 0);
        fmt_str = TypePrintFormat<signed long long int, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%lld"), 0);
        fmt_str = TypePrintFormat<unsigned long long int, char_type>::GetFormatString();
        CHECK_EQUAL(widecmp(fmt_str, L"%llu"), 0);
        fmt_str = TypePrintFormat<float, char_type>::GetFormatString(true);
        CHECK_EQUAL(widecmp(fmt_str, L"%f"), 0);
        fmt_str = TypePrintFormat<float, char_type>::GetFormatString(false);
        CHECK_EQUAL(widecmp(fmt_str, L"%F"), 0);
        fmt_str = TypePrintFormat<double, char_type>::GetFormatString(true);
        CHECK_EQUAL(widecmp(fmt_str, L"%f"), 0);
        fmt_str = TypePrintFormat<double, char_type>::GetFormatString(false);
        CHECK_EQUAL(widecmp(fmt_str, L"%F"), 0);
        fmt_str = TypePrintFormat<long double, char_type>::GetFormatString(true);
        CHECK_EQUAL(widecmp(fmt_str, L"%Lf"), 0);
        fmt_str = TypePrintFormat<long double, char_type>::GetFormatString(false);
        CHECK_EQUAL(widecmp(fmt_str, L"%LF"), 0);
    }
}
