/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeCompare.hpp"

using ocl::TypeCompare;

TEST_MEMBER_FUNCTION(TypeCompare, IsEqual, type_type)
{
    // test for equality.
    CHECK_TRUE(TypeCompare<bool>::IsEqual(false, false));
    CHECK_TRUE(TypeCompare<bool>::IsEqual(true, true));
    CHECK_TRUE(TypeCompare<char>::IsEqual('A', 'A'));
    CHECK_TRUE(TypeCompare<char>::IsEqual('a', 'a'));

    // test equality doesn't find match.
    CHECK_FALSE(TypeCompare<bool>::IsEqual(false, true));
    CHECK_FALSE(TypeCompare<bool>::IsEqual(true, false));
    CHECK_FALSE(TypeCompare<char>::IsEqual('A', 'a'));
    CHECK_FALSE(TypeCompare<char>::IsEqual('a', 'A'));
}

TEST_MEMBER_FUNCTION(TypeCompare, IsNotEqual, type_type)
{
    // test for equality.
    CHECK_TRUE(TypeCompare<bool>::IsNotEqual(false, true));
    CHECK_TRUE(TypeCompare<bool>::IsNotEqual(true, false));
    CHECK_TRUE(TypeCompare<char>::IsNotEqual('A', 'a'));
    CHECK_TRUE(TypeCompare<char>::IsNotEqual('a', 'A'));

    // test equality doesn't find match.
    CHECK_FALSE(TypeCompare<bool>::IsNotEqual(false, false));
    CHECK_FALSE(TypeCompare<bool>::IsNotEqual(true, true));
    CHECK_FALSE(TypeCompare<char>::IsNotEqual('A', 'A'));
    CHECK_FALSE(TypeCompare<char>::IsNotEqual('a', 'a'));
}

TEST_MEMBER_FUNCTION(TypeCompare, IsLess, type_type)
{
    // test for less.
    CHECK_TRUE(TypeCompare<bool>::IsLess(false, true));
    CHECK_TRUE(TypeCompare<int>::IsLess(1, 2));

    // test less doesn't find match.
    CHECK_FALSE(TypeCompare<bool>::IsLess(false, false));
    CHECK_FALSE(TypeCompare<bool>::IsLess(true, true));
    CHECK_FALSE(TypeCompare<bool>::IsLess(true, false));
    CHECK_FALSE(TypeCompare<int>::IsLess(1, 1));
    CHECK_FALSE(TypeCompare<int>::IsLess(2, 1));
}

TEST_MEMBER_FUNCTION(TypeCompare, IsLessEqual, type_type)
{
    // test for less or equal.
    CHECK_TRUE(TypeCompare<bool>::IsLessEqual(false, true));
    CHECK_TRUE(TypeCompare<bool>::IsLessEqual(false, false));
    CHECK_TRUE(TypeCompare<bool>::IsLessEqual(true, true));
    CHECK_TRUE(TypeCompare<int>::IsLessEqual(1, 1));
    CHECK_TRUE(TypeCompare<int>::IsLessEqual(1, 2));

    // test less or equal doesn't find match.
    CHECK_FALSE(TypeCompare<bool>::IsLessEqual(true, false));
    CHECK_FALSE(TypeCompare<int>::IsLessEqual(2, 1));
}

TEST_MEMBER_FUNCTION(TypeCompare, IsGreater, type_type)
{
    // test for greater.
    CHECK_TRUE(TypeCompare<bool>::IsGreater(true, false));
    CHECK_TRUE(TypeCompare<int>::IsGreater(2, 1));

    // test greater doesn't find match.
    CHECK_FALSE(TypeCompare<bool>::IsGreater(false, false));
    CHECK_FALSE(TypeCompare<bool>::IsGreater(true, true));
    CHECK_FALSE(TypeCompare<bool>::IsGreater(false, true));
    CHECK_FALSE(TypeCompare<int>::IsGreater(1, 1));
    CHECK_FALSE(TypeCompare<int>::IsGreater(1, 2));
}

TEST_MEMBER_FUNCTION(TypeCompare, IsGreaterEqual, type_type)
{
    // test for less or equal.
    CHECK_TRUE(TypeCompare<bool>::IsGreaterEqual(true, false));
    CHECK_TRUE(TypeCompare<bool>::IsGreaterEqual(false, false));
    CHECK_TRUE(TypeCompare<bool>::IsGreaterEqual(true, true));
    CHECK_TRUE(TypeCompare<int>::IsGreaterEqual(1, 1));
    CHECK_TRUE(TypeCompare<int>::IsGreaterEqual(2, 1));

    // test less or equal doesn't find match.
    CHECK_FALSE(TypeCompare<bool>::IsGreaterEqual(false, true));
    CHECK_FALSE(TypeCompare<int>::IsGreaterEqual(1, 2));
}
