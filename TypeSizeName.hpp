/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPESIZENAME_HPP
#define OCL_GUARD_TYPEINFO_TYPESIZENAME_HPP

#include <cstdint>

namespace ocl
{

/// TypeSizeName cane be used as debug aid for obtaining the type from a template class.
template<typename Type>
struct TypeSizeName
{
    typedef Type type;

    static char const* GetName() throw() { return NULL; }
    static wchar_t const* GetWideName() throw() { return NULL; }
};

template<>
struct TypeSizeName<int8_t>
{
    typedef std::int8_t type;

    static char const* GetName() throw() { return "int8_t"; }
    static wchar_t const* GetWideName() throw() { return L"int8_t"; }
};

template<>
struct TypeSizeName<uint8_t>
{
    typedef std::uint8_t type;

    static char const* GetName() throw() { return "uint8_t"; }
    static wchar_t const* GetWideName() throw() { return L"uint8_t"; }
};

template<>
struct TypeSizeName<int16_t>
{
    typedef std::int16_t type;

    static char const* GetName() throw() { return "int16_t"; }
    static wchar_t const* GetWideName() throw() { return L"int16_t"; }
};

template<>
struct TypeSizeName<uint16_t>
{
    typedef std::uint16_t type;

    static char const* GetName() throw() { return "uint16_t"; }
    static wchar_t const* GetWideName() throw() { return L"uint16_t"; }
};

template<>
struct TypeSizeName<int32_t>
{
    typedef std::int32_t type;

    static char const* GetName() throw() { return "int32_t"; }
    static wchar_t const* GetWideName() throw() { return L"int32_t"; }
};

template<>
struct TypeSizeName<uint32_t>
{
    typedef std::int32_t type;

    static char const* GetName() throw() { return "uint32_t"; }
    static wchar_t const* GetWideName() throw() { return L"uint32_t"; }
};

template<>
struct TypeSizeName<int64_t>
{
    typedef std::int64_t type;

    static char const* GetName() throw() { return "int64_t"; }
    static wchar_t const* GetWideName() throw() { return L"int64_t"; }
};

template<>
struct TypeSizeName<uint64_t>
{
    typedef std::uint64_t type;

    static char const* GetName() throw() { return "uint64_t"; }
    static wchar_t const* GetWideName() throw() { return L"uint64_t"; }
};

template<>
struct TypeSizeName<int8_t const*>
{
    typedef std::int8_t const* type;

    static char const* GetName() throw() { return "int8_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"int8_t const*"; }
};

template<>
struct TypeSizeName<uint8_t const*>
{
    typedef std::uint8_t const* type;

    static char const* GetName() throw() { return "uint8_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"uint8_t const*"; }
};

template<>
struct TypeSizeName<int16_t const*>
{
    typedef std::int16_t const* type;

    static char const* GetName() throw() { return "int16_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"int16_t const*"; }
};

template<>
struct TypeSizeName<uint16_t const*>
{
    typedef std::uint16_t const* type;

    static char const* GetName() throw() { return "uint16_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"uint16_t const*"; }
};

template<>
struct TypeSizeName<int32_t const*>
{
    typedef std::int32_t const* type;

    static char const* GetName() throw() { return "int32_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"int32_t const*"; }
};

template<>
struct TypeSizeName<uint32_t const*>
{
    typedef std::uint32_t const* type;

    static char const* GetName() throw() { return "uint32_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"uint32_t const*"; }
};

template<>
struct TypeSizeName<int64_t const*>
{
    typedef std::int64_t const* type;

    static char const* GetName() throw() { return "int64_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"int64_t const*"; }
};

template<>
struct TypeSizeName<uint64_t const*>
{
    typedef std::uint64_t const* type;

    static char const* GetName() throw() { return "uint64_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"uint64_t const*"; }
};

template<>
struct TypeSizeName<int8_t*>
{
    typedef std::int8_t* type;

    static char const* GetName() throw() { return "int8_t*"; }
    static wchar_t const* GetWideName() throw() { return L"int8_t*"; }
};

template<>
struct TypeSizeName<uint8_t*>
{
    typedef std::uint8_t* type;

    static char const* GetName() throw() { return "uint8_t*"; }
    static wchar_t const* GetWideName() throw() { return L"uint8_t*"; }
};

template<>
struct TypeSizeName<int16_t*>
{
    typedef std::int16_t* type;

    static char const* GetName() throw() { return "int16_t*"; }
    static wchar_t const* GetWideName() throw() { return L"int16_t*"; }
};

template<>
struct TypeSizeName<uint16_t*>
{
    typedef std::uint16_t* type;

    static char const* GetName() throw() { return "uint16_t*"; }
    static wchar_t const* GetWideName() throw() { return L"uint16_t*"; }
};

template<>
struct TypeSizeName<int32_t*>
{
    typedef std::int32_t* type;

    static char const* GetName() throw() { return "int32_t*"; }
    static wchar_t const* GetWideName() throw() { return L"int32_t*"; }
};

template<>
struct TypeSizeName<uint32_t*>
{
    typedef std::uint32_t* type;

    static char const* GetName() throw() { return "uint32_t*"; }
    static wchar_t const* GetWideName() throw() { return L"uint32_t*"; }
};

template<>
struct TypeSizeName<int64_t*>
{
    typedef std::int64_t* type;

    static char const* GetName() throw() { return "int64_t*"; }
    static wchar_t const* GetWideName() throw() { return L"int64_t*"; }
};

template<>
struct TypeSizeName<uint64_t*>
{
    typedef std::uint64_t* type;

    static char const* GetName() throw() { return "uint64_t*"; }
    static wchar_t const* GetWideName() throw() { return L"uint64_t*"; }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPESIZENAME_HPP
