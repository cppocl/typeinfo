/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeDigitToInteger.hpp"

TEST_MEMBER_FUNCTION(TypeDigitToInteger, ToInteger, char_type)
{
    // char + wchar_t with signed char + unsigned char
    {
        typedef char char_type;
        typedef signed char int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef signed char int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef char char_type;
        typedef unsigned char int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef unsigned char int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    // char + wchar_t with signed short + unsigned short
    {
        typedef char char_type;
        typedef signed short int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef signed short int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef char char_type;
        typedef unsigned short int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef unsigned short int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    // char + wchar_t with signed int + unsigned int
    {
        typedef char char_type;
        typedef signed int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef signed int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef char char_type;
        typedef unsigned int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef unsigned int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    // char + wchar_t with signed long int + unsigned long int
    {
        typedef char char_type;
        typedef signed long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef signed long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef char char_type;
        typedef unsigned long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef unsigned long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    // char + wchar_t with signed long long int + unsigned long long int
    {
        typedef char char_type;
        typedef signed long long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef signed long long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef char char_type;
        typedef unsigned long long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }

    {
        typedef wchar_t char_type;
        typedef unsigned long long int int_type;
        typedef ocl::TypeDigitToInteger<char_type, int_type> to_integer_type;

        CHECK_EQUAL(to_integer_type::ToInteger('0'), (int_type)0);
        CHECK_EQUAL(to_integer_type::ToInteger('1'), (int_type)1);
        CHECK_EQUAL(to_integer_type::ToInteger('2'), (int_type)2);
        CHECK_EQUAL(to_integer_type::ToInteger('3'), (int_type)3);
        CHECK_EQUAL(to_integer_type::ToInteger('4'), (int_type)4);
        CHECK_EQUAL(to_integer_type::ToInteger('5'), (int_type)5);
        CHECK_EQUAL(to_integer_type::ToInteger('6'), (int_type)6);
        CHECK_EQUAL(to_integer_type::ToInteger('7'), (int_type)7);
        CHECK_EQUAL(to_integer_type::ToInteger('8'), (int_type)8);
        CHECK_EQUAL(to_integer_type::ToInteger('9'), (int_type)9);
        CHECK_EQUAL(to_integer_type::ToInteger('A'), to_integer_type::invalid);
    }
}
