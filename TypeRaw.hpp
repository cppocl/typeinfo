/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPERAW_H
#define OCL_GUARD_TYPEINFO_TYPERAW_H

#include <cstddef>

namespace ocl
{

template<typename Type>
struct TypeRaw
{
    typedef Type type;
    typedef Type raw_type;

    // If the type is not modified with const, *, & or volatile then it is raw.
    static bool const is_raw = true;

    // Each modification is counted as a single depth.
    // E.g. int is depth 1
    //      signed int is depth 1
    //      signed int const is depth 2
    //      signed int const* is depth 3
    static size_t const depth = 1U;
};

template<typename Type>
struct TypeRaw<Type const>
{
    typedef Type const type;
    typedef typename TypeRaw<Type>::raw_type raw_type;

    static bool const is_raw = false;
    static size_t const depth = TypeRaw<Type>::depth + 1U;
};

template<typename Type>
struct TypeRaw<Type*>
{
    typedef Type* type;
    typedef typename TypeRaw<Type>::raw_type raw_type;

    static bool const is_raw = false;
    static size_t const depth = TypeRaw<Type>::depth + 1U;
};

template<typename Type>
struct TypeRaw<Type&>
{
    typedef Type& type;
    typedef typename TypeRaw<Type>::raw_type raw_type;

    static bool const is_raw = false;
    static size_t const depth = TypeRaw<Type>::depth + 1U;
};

template<typename Type>
struct TypeRaw<Type volatile>
{
    typedef Type volatile type;
    typedef typename TypeRaw<Type>::raw_type raw_type;

    static bool const is_raw = false;
    static size_t const depth = TypeRaw<Type>::depth + 1U;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPERAW_H
