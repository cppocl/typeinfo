/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeName.hpp"
#include "../TypeNext.hpp"
#include <string.h>

using ocl::TypeNext;
using ocl::TypeName;

TEST_MEMBER_FUNCTION(TypeNext, next_type, NA)
{
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<signed char>::next_type>::GetName(), "signed short"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<unsigned char>::next_type>::GetName(), "unsigned short"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<signed short>::next_type>::GetName(), "signed int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<unsigned short>::next_type>::GetName(), "unsigned int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<signed int>::next_type>::GetName(), "signed long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<unsigned int>::next_type>::GetName(), "unsigned long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<signed long int>::next_type>::GetName(), "signed long long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<unsigned long int>::next_type>::GetName(), "unsigned long long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<float>::next_type>::GetName(), "double"), 0);
    CHECK_EQUAL(::strcmp(TypeName<TypeNext<double>::next_type>::GetName(), "long double"), 0);
}
