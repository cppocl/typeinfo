/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEISNUMERIC_HPP
#define OCL_GUARD_TYPEINFO_TYPEISNUMERIC_HPP

#include "TypeIsInteger.hpp"
#include "TypeIsDecimal.hpp"

namespace ocl
{

template<typename Type>
struct TypeIsNumeric
{
    typedef Type type;

    static bool const is_integer = TypeIsInteger<Type>::is_integer; // is integer type?
    static bool const is_decimal = TypeIsDecimal<Type>::is_decimal; // is floating point type?
    static bool const is_numeric = is_integer || is_decimal;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEISNUMERIC_HPP
