/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeConstant.hpp"

using ocl::TypeConstant;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeConstant, is_constant, NA)
{
    CHECK_TRUE(TypeConstant<Huge const>::is_constant);
    CHECK_TRUE(TypeConstant<bool const>::is_constant);
    CHECK_TRUE(TypeConstant<char const>::is_constant);
    CHECK_TRUE(TypeConstant<wchar_t const>::is_constant);
    CHECK_TRUE(TypeConstant<signed char const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned char const>::is_constant);
    CHECK_TRUE(TypeConstant<signed short const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned short const>::is_constant);
    CHECK_TRUE(TypeConstant<signed int const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned int const>::is_constant);
    CHECK_TRUE(TypeConstant<signed long int const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned long int const>::is_constant);
    CHECK_TRUE(TypeConstant<signed long long int const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned long long int const>::is_constant);
    CHECK_TRUE(TypeConstant<float const>::is_constant);
    CHECK_TRUE(TypeConstant<double const>::is_constant);
    CHECK_TRUE(TypeConstant<long double const>::is_constant);

    CHECK_TRUE(TypeConstant<Huge const* const>::is_constant);
    CHECK_TRUE(TypeConstant<void const* const>::is_constant);
    CHECK_TRUE(TypeConstant<bool const* const>::is_constant);
    CHECK_TRUE(TypeConstant<char const* const>::is_constant);
    CHECK_TRUE(TypeConstant<wchar_t const* const>::is_constant);
    CHECK_TRUE(TypeConstant<signed char const* const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned char const* const>::is_constant);
    CHECK_TRUE(TypeConstant<signed short const* const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned short const* const>::is_constant);
    CHECK_TRUE(TypeConstant<signed int const* const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned int const* const>::is_constant);
    CHECK_TRUE(TypeConstant<signed long int const* const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned long int const* const>::is_constant);
    CHECK_TRUE(TypeConstant<signed long long int const* const>::is_constant);
    CHECK_TRUE(TypeConstant<unsigned long long int const* const>::is_constant);
    CHECK_TRUE(TypeConstant<float const* const>::is_constant);
    CHECK_TRUE(TypeConstant<double const* const>::is_constant);
    CHECK_TRUE(TypeConstant<long double const* const>::is_constant);

    CHECK_FALSE(TypeConstant<Huge>::is_constant);
    CHECK_FALSE(TypeConstant<bool>::is_constant);
    CHECK_FALSE(TypeConstant<char>::is_constant);
    CHECK_FALSE(TypeConstant<wchar_t>::is_constant);
    CHECK_FALSE(TypeConstant<signed char>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned char>::is_constant);
    CHECK_FALSE(TypeConstant<signed short>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned short>::is_constant);
    CHECK_FALSE(TypeConstant<signed int>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned int>::is_constant);
    CHECK_FALSE(TypeConstant<signed long int>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long int>::is_constant);
    CHECK_FALSE(TypeConstant<signed long long int>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long long int>::is_constant);
    CHECK_FALSE(TypeConstant<float>::is_constant);
    CHECK_FALSE(TypeConstant<double>::is_constant);
    CHECK_FALSE(TypeConstant<long double>::is_constant);

    CHECK_FALSE(TypeConstant<Huge&>::is_constant);
    CHECK_FALSE(TypeConstant<bool&>::is_constant);
    CHECK_FALSE(TypeConstant<char&>::is_constant);
    CHECK_FALSE(TypeConstant<wchar_t&>::is_constant);
    CHECK_FALSE(TypeConstant<signed char&>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned char&>::is_constant);
    CHECK_FALSE(TypeConstant<signed short&>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned short&>::is_constant);
    CHECK_FALSE(TypeConstant<signed int&>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned int&>::is_constant);
    CHECK_FALSE(TypeConstant<signed long int&>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long int&>::is_constant);
    CHECK_FALSE(TypeConstant<signed long long int&>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long long int&>::is_constant);
    CHECK_FALSE(TypeConstant<float&>::is_constant);
    CHECK_FALSE(TypeConstant<double&>::is_constant);
    CHECK_FALSE(TypeConstant<long double&>::is_constant);

    CHECK_FALSE(TypeConstant<Huge volatile>::is_constant);
    CHECK_FALSE(TypeConstant<bool volatile>::is_constant);
    CHECK_FALSE(TypeConstant<char volatile>::is_constant);
    CHECK_FALSE(TypeConstant<wchar_t volatile>::is_constant);
    CHECK_FALSE(TypeConstant<signed char volatile>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned char volatile>::is_constant);
    CHECK_FALSE(TypeConstant<signed short volatile>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned short volatile>::is_constant);
    CHECK_FALSE(TypeConstant<signed int volatile>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned int volatile>::is_constant);
    CHECK_FALSE(TypeConstant<signed long int volatile>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long int volatile>::is_constant);
    CHECK_FALSE(TypeConstant<signed long long int volatile>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long long int volatile>::is_constant);
    CHECK_FALSE(TypeConstant<float volatile>::is_constant);
    CHECK_FALSE(TypeConstant<double volatile>::is_constant);
    CHECK_FALSE(TypeConstant<long double volatile>::is_constant);

    CHECK_FALSE(TypeConstant<Huge*>::is_constant);
    CHECK_FALSE(TypeConstant<void*>::is_constant);
    CHECK_FALSE(TypeConstant<bool*>::is_constant);
    CHECK_FALSE(TypeConstant<char*>::is_constant);
    CHECK_FALSE(TypeConstant<wchar_t*>::is_constant);
    CHECK_FALSE(TypeConstant<signed char*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned char*>::is_constant);
    CHECK_FALSE(TypeConstant<signed short*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned short*>::is_constant);
    CHECK_FALSE(TypeConstant<signed int*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned int*>::is_constant);
    CHECK_FALSE(TypeConstant<signed long int*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long int*>::is_constant);
    CHECK_FALSE(TypeConstant<signed long long int*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long long int*>::is_constant);
    CHECK_FALSE(TypeConstant<float*>::is_constant);
    CHECK_FALSE(TypeConstant<double*>::is_constant);
    CHECK_FALSE(TypeConstant<long double*>::is_constant);

    CHECK_FALSE(TypeConstant<Huge**>::is_constant);
    CHECK_FALSE(TypeConstant<void**>::is_constant);
    CHECK_FALSE(TypeConstant<bool**>::is_constant);
    CHECK_FALSE(TypeConstant<char**>::is_constant);
    CHECK_FALSE(TypeConstant<wchar_t**>::is_constant);
    CHECK_FALSE(TypeConstant<signed char**>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned char**>::is_constant);
    CHECK_FALSE(TypeConstant<signed short**>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned short**>::is_constant);
    CHECK_FALSE(TypeConstant<signed int**>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned int**>::is_constant);
    CHECK_FALSE(TypeConstant<signed long int**>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long int**>::is_constant);
    CHECK_FALSE(TypeConstant<signed long long int**>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long long int**>::is_constant);
    CHECK_FALSE(TypeConstant<float**>::is_constant);
    CHECK_FALSE(TypeConstant<double**>::is_constant);
    CHECK_FALSE(TypeConstant<long double**>::is_constant);

    CHECK_FALSE(TypeConstant<Huge const*>::is_constant);
    CHECK_FALSE(TypeConstant<void const*>::is_constant);
    CHECK_FALSE(TypeConstant<bool const*>::is_constant);
    CHECK_FALSE(TypeConstant<char const*>::is_constant);
    CHECK_FALSE(TypeConstant<wchar_t const*>::is_constant);
    CHECK_FALSE(TypeConstant<signed char const*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned char const*>::is_constant);
    CHECK_FALSE(TypeConstant<signed short const*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned short const*>::is_constant);
    CHECK_FALSE(TypeConstant<signed int const*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned int const*>::is_constant);
    CHECK_FALSE(TypeConstant<signed long int const*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long int const*>::is_constant);
    CHECK_FALSE(TypeConstant<signed long long int const*>::is_constant);
    CHECK_FALSE(TypeConstant<unsigned long long int const*>::is_constant);
    CHECK_FALSE(TypeConstant<float const*>::is_constant);
    CHECK_FALSE(TypeConstant<double const*>::is_constant);
    CHECK_FALSE(TypeConstant<long double const*>::is_constant);
}
