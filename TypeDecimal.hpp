/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEDECIMAL_HPP
#define OCL_GUARD_TYPEINFO_TYPEDECIMAL_HPP

#include "TypeIsDecimal.hpp"

namespace ocl
{

template<typename Type>
struct TypeDecimal
{
    typedef Type type;

    static bool const is_decimal = TypeIsDecimal<type>::is_decimal; // is integer type?
};

template<>
struct TypeDecimal<float>
{
    typedef float type;
    typedef float decimal_type;

    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
};

template<>
struct TypeDecimal<double>
{
    typedef double type;
    typedef double decimal_type;

    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
};

template<>
struct TypeDecimal<long double>
{
    typedef long double type;
    typedef long double decimal_type;

    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEDECIMAL_HPP
