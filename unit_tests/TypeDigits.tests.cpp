/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeDigits.hpp"
#include <cstring>

namespace
{
    bool Compare(char const* str1, char const* str2)
    {
        return ::strcmp(str1, str2) == 0;
    }

    bool Compare(wchar_t const* str1, wchar_t const* str2)
    {
        while (*str1 != L'\0' && *str2 != L'\0')
            if (*str1++ != *str2++)
                return false;
        return *str1 == *str2;
    }
}

TEST_MEMBER_FUNCTION(TypeDigits, GetDigits, NA)
{
    {
        char const* digits = ocl::TypeDigits<char>::GetDigits();
        CHECK_TRUE(Compare(digits, "0123456789"));
    }

    {
        wchar_t const* digits = ocl::TypeDigits<wchar_t>::GetDigits();
        CHECK_TRUE(Compare(digits, L"0123456789"));
    }
}
