/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeToDigit.hpp"

TEST_MEMBER_FUNCTION(TypeToDigit, Decrement, type_ref)
{
    // char + wchar_t with signed char + unsigned char
    {
        typedef char char_type;
        typedef signed char int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef signed char int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    {
        typedef char char_type;
        typedef unsigned char int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef unsigned char int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    // char + wchar_t with signed short + unsigned short
    {
        typedef char char_type;
        typedef signed short int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef signed short int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    {
        typedef char char_type;
        typedef unsigned short int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef unsigned short int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    // char + wchar_t with signed int + unsigned int
    {
        typedef char char_type;
        typedef signed int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef signed int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    {
        typedef char char_type;
        typedef unsigned int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef unsigned int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    // char + wchar_t with signed long int + unsigned long int
    {
        typedef char char_type;
        typedef signed long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef signed long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    {
        typedef char char_type;
        typedef unsigned long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef unsigned long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    // char + wchar_t with signed long int + unsigned long int
    {
        typedef char char_type;
        typedef signed long long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef signed long long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }

    {
        typedef char char_type;
        typedef unsigned long long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), '0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), '1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), '2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), '3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), '4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), '5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), '6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), '7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), '8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), '9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), '\0');
    }

    {
        typedef wchar_t char_type;
        typedef unsigned long long int int_type;
        typedef ocl::TypeToDigit<char_type> to_digit_type;
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(0), L'0');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(1), L'1');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(2), L'2');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(3), L'3');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(4), L'4');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(5), L'5');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(6), L'6');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(7), L'7');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(8), L'8');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(9), L'9');
        CHECK_EQUAL(to_digit_type::ToDigit<int_type>(10), L'\0');
    }
}
