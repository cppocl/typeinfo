/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPESIZELIMITS_HPP
#define OCL_GUARD_TYPEINFO_TYPESIZELIMITS_HPP

#include <cstdint>
#include <cstddef>

namespace ocl
{

/// Define MIN, MAX, BITS and 
template<std::size_t const size_of_type_in_bits, bool const is_signed>
struct TypeSizeLimits
{
    // Unknown number of characters for conversion to string for other types.
    static std::size_t const MAX_CHARS = 0;
};

template<>
struct TypeSizeLimits<8, true>
{
    typedef std::int8_t type;

    static bool const        is_signed = true;
    static type const        MIN = static_cast<type>(-128);
    static type const        MAX = static_cast<type>(127);
    static type const        TOP_BIT = static_cast<type>(0x40);
    static std::size_t const NUM_BITS = 8u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 4u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type) noexcept
    {
        return sizeof(type);
    }
};

template<>
struct TypeSizeLimits<8, false>
{
    typedef std::uint8_t type;

    static bool const        is_signed = false;
    static type const        MIN = static_cast<type>(0);
    static type const        MAX = static_cast<type>(255);
    static type const        TOP_BIT = static_cast<type>(0x80);
    static std::size_t const NUM_BITS = 8u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 3u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type) noexcept
    {
        return sizeof(type);
    }
};

template<>
struct TypeSizeLimits<16, true>
{
    typedef std::int16_t type;

    static bool const        is_signed = true;
    static type const        MIN = static_cast<type>(-32768);
    static type const        MAX = static_cast<type>(32767);
    static type const        TOP_BIT = static_cast<type>(0x4000);
    static std::size_t const NUM_BITS = 16u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 6u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return (value >= TypeSizeLimits<8, is_signed>::MIN) && (value <= TypeSizeLimits<8, is_signed>::MAX)
                                                ? sizeof(TypeSizeLimits<8, is_signed>::type) : sizeof(type);
    }
};

template<>
struct TypeSizeLimits<16, false>
{
    typedef std::uint16_t type;

    static bool const        is_signed = false;
    static type const        MIN = static_cast<type>(0);
    static type const        MAX = static_cast<type>(65535);
    static type const        TOP_BIT = static_cast<type>(0x8000);
    static std::size_t const NUM_BITS = 16u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 5u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return (value >= TypeSizeLimits<8, is_signed>::MIN) && (value <= TypeSizeLimits<8, is_signed>::MAX)
                                                ? sizeof(TypeSizeLimits<8, is_signed>::type) : sizeof(type);
    }
};

template<>
struct TypeSizeLimits<32, true>
{
    typedef std::int32_t type;

    static bool const        is_signed = true;
    static type const        MIN = static_cast<type>(-2147483647l - 1l);
    static type const        MAX = static_cast<type>(2147483647);
    static type const        TOP_BIT = static_cast<type>(0x40000000ul);
    static std::size_t const NUM_BITS = 32u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 11u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return (value >= TypeSizeLimits<8, is_signed>::MIN) && (value <= TypeSizeLimits<8, is_signed>::MAX)
            ? sizeof(TypeSizeLimits<8, is_signed>::type) :
            ((value >= TypeSizeLimits<16, is_signed>::MIN) && (value <= TypeSizeLimits<16, is_signed>::MAX)
                ? sizeof(TypeSizeLimits<16, is_signed>::type) : sizeof(type));
    }
};

template<>
struct TypeSizeLimits<32, false>
{
    typedef std::uint32_t type;

    static bool const        is_signed = false;
    static type const        MIN = static_cast<type>(0);
    static type const        MAX = static_cast<type>(4294967295ul);
    static type const        TOP_BIT = static_cast<type>(0x80000000ul);
    static std::size_t const NUM_BITS = 32u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 10u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return (value >= TypeSizeLimits<8, is_signed>::MIN) && (value <= TypeSizeLimits<8, is_signed>::MAX)
            ? sizeof(TypeSizeLimits<8, is_signed>::type) :
            ((value >= TypeSizeLimits<16, is_signed>::MIN) && (value <= TypeSizeLimits<16, is_signed>::MAX)
                ? sizeof(TypeSizeLimits<16, is_signed>::type) : sizeof(type));
    }
};

template<>
struct TypeSizeLimits<64, true>
{
    typedef std::int64_t type;

    static bool const        is_signed = true;
    static type const        MIN = static_cast<type>(-9223372036854775807ll - 1ll);
    static type const        MAX = static_cast<type>(9223372036854775807ll);
    static type const        TOP_BIT = static_cast<type>(0x4000000000000000ull);
    static std::size_t const NUM_BITS = 64u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 20u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return (value >= TypeSizeLimits<8, is_signed>::MIN) && (value <= TypeSizeLimits<8, is_signed>::MAX)
            ? sizeof(TypeSizeLimits<8, is_signed>::type) :
            ((value >= TypeSizeLimits<16, is_signed>::MIN) && (value <= TypeSizeLimits<16, is_signed>::MAX)
                ? sizeof(TypeSizeLimits<16, is_signed>::type) :
                    ((value >= TypeSizeLimits<32, is_signed>::MIN) && (value <= TypeSizeLimits<32, is_signed>::MAX)
                        ? sizeof(TypeSizeLimits<32, is_signed>::type) : sizeof(type)));
    }
};

template<>
struct TypeSizeLimits<64, false>
{
    typedef std::uint64_t type;

    static bool const        is_signed = false;
    static type const        MIN = static_cast<type>(0);
    static type const        MAX = static_cast<type>(0xffffffffffffffffull);
    static type const        TOP_BIT = static_cast<type>(0x8000000000000000ull);
    static std::size_t const NUM_BITS = 64u;

    // Number of characters required for convert to string (excluding null).
    static std::size_t const MAX_CHARS = 20u;

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return (value >= TypeSizeLimits<8, is_signed>::MIN) && (value <= TypeSizeLimits<8, is_signed>::MAX)
            ? sizeof(TypeSizeLimits<8, is_signed>::type) :
            ((value >= TypeSizeLimits<16, is_signed>::MIN) && (value <= TypeSizeLimits<16, is_signed>::MAX)
                ? sizeof(TypeSizeLimits<16, is_signed>::type) :
                ((value >= TypeSizeLimits<32, is_signed>::MIN) && (value <= TypeSizeLimits<32, is_signed>::MAX)
                    ? sizeof(TypeSizeLimits<32, is_signed>::type) : sizeof(type)));
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPESIZELIMITS_HPP
