/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeLimits.hpp"
#include "../TypeSizeLimits.hpp"

using ocl::TypeLimits;
using ocl::TypeSizeLimits;

#define TEST_VAR_MIN(type, is_signed) \
    type value    = TypeLimits<type>::MIN; \
    type expected = static_cast<type>(TypeSizeLimits<sizeof(type) * 8, is_signed>::MIN)

#define TEST_VAR_MAX(type, is_signed) \
    type value    = TypeLimits<type>::MAX; \
    type expected = static_cast<type>(TypeSizeLimits<sizeof(type) * 8, is_signed>::MAX)

#define TEST_VAR_NUM_BITS(type, is_signed) \
    type value    = TypeLimits<type>::NUM_BITS; \
    type expected = static_cast<type>(TypeSizeLimits<sizeof(type) * 8, is_signed>::NUM_BITS)

TEST_MEMBER_FUNCTION(TypeLimits, MIN, NA)
{
    {
        TEST_VAR_MIN(signed char, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(unsigned char, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(signed short, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(unsigned short, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(signed int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(unsigned int, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(signed long int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(unsigned long int, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(signed long long int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MIN(unsigned long long int, false);
        CHECK_EQUAL(value, expected);
    }
}

TEST_MEMBER_FUNCTION(TypeLimits, MAX, NA)
{
    {
        TEST_VAR_MAX(signed char, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(unsigned char, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(signed short, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(unsigned short, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(signed int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(unsigned int, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(signed long int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(unsigned long int, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(signed long long int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_MAX(unsigned long long int, false);
        CHECK_EQUAL(value, expected);
    }
}

TEST_MEMBER_FUNCTION(TypeLimits, NUM_BITS, NA)
{
    {
        TEST_VAR_NUM_BITS(signed char, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(unsigned char, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(signed short, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(unsigned short, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(signed int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(unsigned int, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(signed long int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(unsigned long int, false);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(signed long long int, true);
        CHECK_EQUAL(value, expected);
    }

    {
        TEST_VAR_NUM_BITS(unsigned long long int, false);
        CHECK_EQUAL(value, expected);
    }
}

TEST_MEMBER_FUNCTION(TypeLimits, TOP_BIT, NA)
{
    CHECK_EQUAL(CHAR_BIT, 8);
    CHECK_EQUAL(TypeLimits<signed char>::TOP_BIT, static_cast<signed char>(0x40));
    CHECK_EQUAL(TypeLimits<unsigned char>::TOP_BIT, static_cast<unsigned char>(0x80));
}

TEST_MEMBER_FUNCTION(TypeLimits, GetByteCount, type)
{
    typedef TypeLimits<std::int8_t> size_limits8_signed_type;
    typedef TypeLimits<std::uint8_t> size_limits8_unsigned_type;
    typedef TypeLimits<std::int16_t> size_limits16_signed_type;
    typedef TypeLimits<std::uint16_t> size_limits16_unsigned_type;
    typedef TypeLimits<std::int32_t> size_limits32_signed_type;
    typedef TypeLimits<std::uint32_t> size_limits32_unsigned_type;
    typedef TypeLimits<std::int64_t> size_limits64_signed_type;
    typedef TypeLimits<std::uint64_t> size_limits64_unsigned_type;

    static const std::int8_t int8_min_value = TypeLimits<std::int8_t>::MIN;
    static const std::int8_t int8_max_value = TypeLimits<std::int8_t>::MAX;
    static const std::int16_t int16_min_value = TypeLimits<std::int16_t>::MIN;
    static const std::int16_t int16_max_value = TypeLimits<std::int16_t>::MAX;
    static const std::int32_t int32_min_value = TypeLimits<std::int32_t>::MIN;
    static const std::int32_t int32_max_value = TypeLimits<std::int32_t>::MAX;
    static const std::int64_t int64_min_value = TypeLimits<std::int64_t>::MIN;
    static const std::int64_t int64_max_value = TypeLimits<std::int64_t>::MAX;

    static const std::uint8_t uint8_min_value = TypeLimits<std::uint8_t>::MIN;
    static const std::uint8_t uint8_max_value = TypeLimits<std::uint8_t>::MAX;
    static const std::uint16_t uint16_min_value = TypeLimits<std::uint16_t>::MIN;
    static const std::uint16_t uint16_max_value = TypeLimits<std::uint16_t>::MAX;
    static const std::uint32_t uint32_min_value = TypeLimits<std::uint32_t>::MIN;
    static const std::uint32_t uint32_max_value = TypeLimits<std::uint32_t>::MAX;
    static const std::uint64_t uint64_min_value = TypeLimits<std::uint64_t>::MIN;
    static const std::uint64_t uint64_max_value = TypeLimits<std::uint64_t>::MAX;

    CHECK_EQUAL(size_limits8_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits8_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int16_min_value), 2);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int16_max_value), 2);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int16_min_value), 2);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int32_min_value), 4);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int16_max_value), 2);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int32_max_value), 4);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int16_min_value), 2);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int32_min_value), 4);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int64_min_value), 8);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int16_max_value), 2);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int32_max_value), 4);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int64_max_value), 8);

    CHECK_EQUAL(size_limits8_unsigned_type::GetByteCount(uint8_min_value), 1);
    CHECK_EQUAL(size_limits8_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits16_unsigned_type::GetByteCount(uint16_min_value), 1);
    CHECK_EQUAL(size_limits16_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits16_unsigned_type::GetByteCount(uint16_max_value), 2);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint32_min_value), 1);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint16_max_value), 2);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint32_max_value), 4);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint64_min_value), 1);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint16_max_value), 2);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint32_max_value), 4);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint64_max_value), 8);
}
