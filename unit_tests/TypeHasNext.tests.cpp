/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeHasNext.hpp"

namespace
{

struct Huge
{
    double data[1000];
};

}

using ocl::TypeHasNext;

TEST_MEMBER_FUNCTION(TypeHasNext, has_next, NA)
{
    CHECK_FALSE(TypeHasNext<bool>::has_next);
    CHECK_FALSE(TypeHasNext<char>::has_next);
    CHECK_FALSE(TypeHasNext<wchar_t>::has_next);
    CHECK_TRUE(TypeHasNext<signed char>::has_next);
    CHECK_TRUE(TypeHasNext<unsigned char>::has_next);
    CHECK_TRUE(TypeHasNext<signed short>::has_next);
    CHECK_TRUE(TypeHasNext<unsigned short>::has_next);
    CHECK_TRUE(TypeHasNext<signed int>::has_next);
    CHECK_TRUE(TypeHasNext<unsigned int>::has_next);
    CHECK_TRUE(TypeHasNext<signed long int>::has_next);
    CHECK_TRUE(TypeHasNext<unsigned long int>::has_next);
    CHECK_TRUE(TypeHasNext<float>::has_next);
    CHECK_TRUE(TypeHasNext<double>::has_next);
    CHECK_FALSE(TypeHasNext<signed long long int>::has_next);
    CHECK_FALSE(TypeHasNext<unsigned long long int>::has_next);
    CHECK_FALSE(TypeHasNext<long double>::has_next);
    CHECK_FALSE(TypeHasNext<Huge>::has_next);
}
