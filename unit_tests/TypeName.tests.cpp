/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeName.hpp"
#include <string.h>

using ocl::TypeName;

namespace
{

int widecmp(wchar_t const* str1, wchar_t const* str2)
{
    while ((*str1 != L'\0') && (*str2 != L'\0'))
    {
        ++str1;
        ++str2;
    }
    return static_cast<int>(*str1 - *str2);
}

}

TEST_MEMBER_FUNCTION(TypeName, GetName, NA)
{
    CHECK_EQUAL(::strcmp(TypeName<bool>::GetName(), "bool"), 0);
    CHECK_EQUAL(::strcmp(TypeName<char>::GetName(), "char"), 0);
    CHECK_EQUAL(::strcmp(TypeName<wchar_t>::GetName(), "wchar_t"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed char>::GetName(), "signed char"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned char>::GetName(), "unsigned char"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed short>::GetName(), "signed short"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned short>::GetName(), "unsigned short"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed int>::GetName(), "signed int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned int>::GetName(), "unsigned int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed long int>::GetName(), "signed long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned long int>::GetName(), "unsigned long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed long long int>::GetName(), "signed long long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned long long int>::GetName(), "unsigned long long int"), 0);
    CHECK_EQUAL(::strcmp(TypeName<float>::GetName(), "float"), 0);
    CHECK_EQUAL(::strcmp(TypeName<double>::GetName(), "double"), 0);
    CHECK_EQUAL(::strcmp(TypeName<long double>::GetName(), "long double"), 0);

    CHECK_EQUAL(::strcmp(TypeName<bool const*>::GetName(), "bool const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<char const*>::GetName(), "char const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<wchar_t const*>::GetName(), "wchar_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed char const*>::GetName(), "signed char const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned char const*>::GetName(), "unsigned char const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed short const*>::GetName(), "signed short const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned short const*>::GetName(), "unsigned short const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed int const*>::GetName(), "signed int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned int const*>::GetName(), "unsigned int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed long int const*>::GetName(), "signed long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned long int const*>::GetName(), "unsigned long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed long long int const*>::GetName(), "signed long long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned long long int const*>::GetName(), "unsigned long long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<float const*>::GetName(), "float const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<double const*>::GetName(), "double const*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<long double const*>::GetName(), "long double const*"), 0);

    CHECK_EQUAL(::strcmp(TypeName<bool*>::GetName(), "bool*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<char*>::GetName(), "char*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<wchar_t*>::GetName(), "wchar_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed char*>::GetName(), "signed char*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned char*>::GetName(), "unsigned char*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed short*>::GetName(), "signed short*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned short*>::GetName(), "unsigned short*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed int*>::GetName(), "signed int*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned int*>::GetName(), "unsigned int*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed long int*>::GetName(), "signed long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned long int*>::GetName(), "unsigned long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<signed long long int*>::GetName(), "signed long long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<unsigned long long int*>::GetName(), "unsigned long long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<float*>::GetName(), "float*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<double*>::GetName(), "double*"), 0);
    CHECK_EQUAL(::strcmp(TypeName<long double*>::GetName(), "long double*"), 0);
}

TEST_MEMBER_FUNCTION(TypeName, GetWideName, NA)
{
    CHECK_EQUAL(widecmp(TypeName<bool>::GetWideName(), L"bool"), 0);
    CHECK_EQUAL(widecmp(TypeName<char>::GetWideName(), L"char"), 0);
    CHECK_EQUAL(widecmp(TypeName<wchar_t>::GetWideName(), L"wchar_t"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed char>::GetWideName(), L"signed char"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned char>::GetWideName(), L"unsigned char"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed short>::GetWideName(), L"signed short"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned short>::GetWideName(), L"unsigned short"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed int>::GetWideName(), L"signed int"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned int>::GetWideName(), L"unsigned int"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed long int>::GetWideName(), L"signed long int"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned long int>::GetWideName(), L"unsigned long int"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed long long int>::GetWideName(), L"signed long long int"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned long long int>::GetWideName(), L"unsigned long long int"), 0);
    CHECK_EQUAL(widecmp(TypeName<float>::GetWideName(), L"float"), 0);
    CHECK_EQUAL(widecmp(TypeName<double>::GetWideName(), L"double"), 0);
    CHECK_EQUAL(widecmp(TypeName<long double>::GetWideName(), L"long double"), 0);

    CHECK_EQUAL(widecmp(TypeName<bool const*>::GetWideName(), L"bool const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<char const*>::GetWideName(), L"char const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<wchar_t const*>::GetWideName(), L"wchar_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed char const*>::GetWideName(), L"signed char const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned char const*>::GetWideName(), L"unsigned char const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed short const*>::GetWideName(), L"signed short const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned short const*>::GetWideName(), L"unsigned short const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed int const*>::GetWideName(), L"signed int const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned int const*>::GetWideName(), L"unsigned int const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed long int const*>::GetWideName(), L"signed long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned long int const*>::GetWideName(), L"unsigned long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed long long int const*>::GetWideName(), L"signed long long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned long long int const*>::GetWideName(), L"unsigned long long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<float const*>::GetWideName(), L"float const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<double const*>::GetWideName(), L"double const*"), 0);
    CHECK_EQUAL(widecmp(TypeName<long double const*>::GetWideName(), L"long double const*"), 0);

    CHECK_EQUAL(widecmp(TypeName<bool*>::GetWideName(), L"bool*"), 0);
    CHECK_EQUAL(widecmp(TypeName<char*>::GetWideName(), L"char*"), 0);
    CHECK_EQUAL(widecmp(TypeName<wchar_t*>::GetWideName(), L"wchar_t*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed char*>::GetWideName(), L"signed char*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned char*>::GetWideName(), L"unsigned char*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed short*>::GetWideName(), L"signed short*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned short*>::GetWideName(), L"unsigned short*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed int*>::GetWideName(), L"signed int*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned int*>::GetWideName(), L"unsigned int*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed long int*>::GetWideName(), L"signed long int*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned long int*>::GetWideName(), L"unsigned long int*"), 0);
    CHECK_EQUAL(widecmp(TypeName<signed long long int*>::GetWideName(), L"signed long long int*"), 0);
    CHECK_EQUAL(widecmp(TypeName<unsigned long long int*>::GetWideName(), L"unsigned long long int*"), 0);
    CHECK_EQUAL(widecmp(TypeName<float*>::GetWideName(), L"float*"), 0);
    CHECK_EQUAL(widecmp(TypeName<double*>::GetWideName(), L"double*"), 0);
    CHECK_EQUAL(widecmp(TypeName<long double*>::GetWideName(), L"long double*"), 0);
}
