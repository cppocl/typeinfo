/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEPREVIOUS_HPP
#define OCL_GUARD_TYPEINFO_TYPEPREVIOUS_HPP

namespace ocl
{

template<typename Type>
struct TypePrevious;

template<>
struct TypePrevious<signed short>
{
    typedef signed short type;
    typedef signed char previous_type;
};

template<>
struct TypePrevious<unsigned short>
{
    typedef unsigned short type;
    typedef unsigned char previous_type;
};

template<>
struct TypePrevious<signed int>
{
    typedef signed int type;
    typedef signed short previous_type;
};

template<>
struct TypePrevious<unsigned int>
{
    typedef unsigned int type;
    typedef unsigned short previous_type;
};

template<>
struct TypePrevious<signed long int>
{
    typedef signed long int type;
    typedef signed int previous_type;
};

template<>
struct TypePrevious<unsigned long int>
{
    typedef unsigned long int type;
    typedef unsigned int previous_type;
};

template<>
struct TypePrevious<signed long long int>
{
    typedef signed long long int type;
    typedef signed long int previous_type;
};

template<>
struct TypePrevious<unsigned long long int>
{
    typedef unsigned long long int type;
    typedef unsigned long int previous_type;
};

template<>
struct TypePrevious<double>
{
    typedef double type;
    typedef float previous_type;
};

template<>
struct TypePrevious<long double>
{
    typedef long double type;
    typedef double previous_type;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEPREVIOUS_HPP
