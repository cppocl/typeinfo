/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEPOD_HPP
#define OCL_GUARD_TYPEINFO_TYPEPOD_HPP

namespace ocl
{

/// Pod (Plain old data) detection.
/// Note this does not support C++ 11 types for portability reasons.
template<typename Type>
struct TypePod
{
    typedef Type type;
    static bool const is_pod = false;
};

template<>
struct TypePod<bool>
{
    typedef bool type;
    static bool const is_pod = true;
};

template<>
struct TypePod<char>
{
    typedef char type;
    static bool const is_pod = true;
};

template<>
struct TypePod<wchar_t>
{
    typedef wchar_t type;
    static bool const is_pod = true;
};

template<>
struct TypePod<signed char>
{
    typedef signed char type;
    static bool const is_pod = true;
};

template<>
struct TypePod<unsigned char>
{
    typedef unsigned char type;
    static bool const is_pod = true;
};

template<>
struct TypePod<signed short>
{
    typedef signed short type;
    static bool const is_pod = true;
};

template<>
struct TypePod<unsigned short>
{
    typedef unsigned short type;
    static bool const is_pod = true;
};

template<>
struct TypePod<signed int>
{
    typedef signed int type;
    static bool const is_pod = true;
};

template<>
struct TypePod<unsigned int>
{
    typedef unsigned int type;
    static bool const is_pod = true;
};

template<>
struct TypePod<signed long int>
{
    typedef signed long int type;
    static bool const is_pod = true;
};

template<>
struct TypePod<unsigned long int>
{
    typedef unsigned long int type;
    static bool const is_pod = true;
};

template<>
struct TypePod<signed long long int>
{
    typedef signed long long int type;
    static bool const is_pod = true;
};

template<>
struct TypePod<unsigned long long int>
{
    typedef unsigned long long int type;
    static bool const is_pod = true;
};

template<>
struct TypePod<float>
{
    typedef float type;
    static bool const is_pod = true;
};

template<>
struct TypePod<double>
{
    typedef double type;
    static bool const is_pod = true;
};

template<>
struct TypePod<long double>
{
    typedef long double type;
    static bool const is_pod = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEPOD_HPP
