/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeReference.hpp"

using ocl::TypeReference;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeReference, is_reference, NA)
{
    CHECK_TRUE(TypeReference<Huge&>::is_reference);
    CHECK_TRUE(TypeReference<bool&>::is_reference);
    CHECK_TRUE(TypeReference<char&>::is_reference);
    CHECK_TRUE(TypeReference<wchar_t&>::is_reference);
    CHECK_TRUE(TypeReference<signed char&>::is_reference);
    CHECK_TRUE(TypeReference<unsigned char&>::is_reference);
    CHECK_TRUE(TypeReference<signed short&>::is_reference);
    CHECK_TRUE(TypeReference<unsigned short&>::is_reference);
    CHECK_TRUE(TypeReference<signed int&>::is_reference);
    CHECK_TRUE(TypeReference<unsigned int&>::is_reference);
    CHECK_TRUE(TypeReference<signed long int&>::is_reference);
    CHECK_TRUE(TypeReference<unsigned long int&>::is_reference);
    CHECK_TRUE(TypeReference<signed long long int&>::is_reference);
    CHECK_TRUE(TypeReference<unsigned long long int&>::is_reference);
    CHECK_TRUE(TypeReference<float&>::is_reference);
    CHECK_TRUE(TypeReference<double&>::is_reference);
    CHECK_TRUE(TypeReference<long double&>::is_reference);

    CHECK_FALSE(TypeReference<Huge>::is_reference);
    CHECK_FALSE(TypeReference<bool>::is_reference);
    CHECK_FALSE(TypeReference<char>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t>::is_reference);
    CHECK_FALSE(TypeReference<signed char>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char>::is_reference);
    CHECK_FALSE(TypeReference<signed short>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short>::is_reference);
    CHECK_FALSE(TypeReference<signed int>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int>::is_reference);
    CHECK_FALSE(TypeReference<signed long int>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int>::is_reference);
    CHECK_FALSE(TypeReference<float>::is_reference);
    CHECK_FALSE(TypeReference<double>::is_reference);
    CHECK_FALSE(TypeReference<long double>::is_reference);

    CHECK_FALSE(TypeReference<Huge const>::is_reference);
    CHECK_FALSE(TypeReference<bool const>::is_reference);
    CHECK_FALSE(TypeReference<char const>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t const>::is_reference);
    CHECK_FALSE(TypeReference<signed char const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char const>::is_reference);
    CHECK_FALSE(TypeReference<signed short const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short const>::is_reference);
    CHECK_FALSE(TypeReference<signed int const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int const>::is_reference);
    CHECK_FALSE(TypeReference<signed long int const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int const>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int const>::is_reference);
    CHECK_FALSE(TypeReference<float const>::is_reference);
    CHECK_FALSE(TypeReference<double const>::is_reference);
    CHECK_FALSE(TypeReference<long double const>::is_reference);

    CHECK_FALSE(TypeReference<Huge volatile>::is_reference);
    CHECK_FALSE(TypeReference<bool volatile>::is_reference);
    CHECK_FALSE(TypeReference<char volatile>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t volatile>::is_reference);
    CHECK_FALSE(TypeReference<signed char volatile>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char volatile>::is_reference);
    CHECK_FALSE(TypeReference<signed short volatile>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short volatile>::is_reference);
    CHECK_FALSE(TypeReference<signed int volatile>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int volatile>::is_reference);
    CHECK_FALSE(TypeReference<signed long int volatile>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int volatile>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int volatile>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int volatile>::is_reference);
    CHECK_FALSE(TypeReference<float volatile>::is_reference);
    CHECK_FALSE(TypeReference<double volatile>::is_reference);
    CHECK_FALSE(TypeReference<long double volatile>::is_reference);

    CHECK_FALSE(TypeReference<Huge*>::is_reference);
    CHECK_FALSE(TypeReference<void*>::is_reference);
    CHECK_FALSE(TypeReference<bool*>::is_reference);
    CHECK_FALSE(TypeReference<char*>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t*>::is_reference);
    CHECK_FALSE(TypeReference<signed char*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char*>::is_reference);
    CHECK_FALSE(TypeReference<signed short*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short*>::is_reference);
    CHECK_FALSE(TypeReference<signed int*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int*>::is_reference);
    CHECK_FALSE(TypeReference<signed long int*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int*>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int*>::is_reference);
    CHECK_FALSE(TypeReference<float*>::is_reference);
    CHECK_FALSE(TypeReference<double*>::is_reference);
    CHECK_FALSE(TypeReference<long double*>::is_reference);

    CHECK_FALSE(TypeReference<Huge**>::is_reference);
    CHECK_FALSE(TypeReference<void**>::is_reference);
    CHECK_FALSE(TypeReference<bool**>::is_reference);
    CHECK_FALSE(TypeReference<char**>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t**>::is_reference);
    CHECK_FALSE(TypeReference<signed char**>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char**>::is_reference);
    CHECK_FALSE(TypeReference<signed short**>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short**>::is_reference);
    CHECK_FALSE(TypeReference<signed int**>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int**>::is_reference);
    CHECK_FALSE(TypeReference<signed long int**>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int**>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int**>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int**>::is_reference);
    CHECK_FALSE(TypeReference<float**>::is_reference);
    CHECK_FALSE(TypeReference<double**>::is_reference);
    CHECK_FALSE(TypeReference<long double**>::is_reference);

    CHECK_FALSE(TypeReference<Huge const*>::is_reference);
    CHECK_FALSE(TypeReference<void const*>::is_reference);
    CHECK_FALSE(TypeReference<bool const*>::is_reference);
    CHECK_FALSE(TypeReference<char const*>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t const*>::is_reference);
    CHECK_FALSE(TypeReference<signed char const*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char const*>::is_reference);
    CHECK_FALSE(TypeReference<signed short const*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short const*>::is_reference);
    CHECK_FALSE(TypeReference<signed int const*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int const*>::is_reference);
    CHECK_FALSE(TypeReference<signed long int const*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int const*>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int const*>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int const*>::is_reference);
    CHECK_FALSE(TypeReference<float const*>::is_reference);
    CHECK_FALSE(TypeReference<double const*>::is_reference);
    CHECK_FALSE(TypeReference<long double const*>::is_reference);

    CHECK_FALSE(TypeReference<Huge const* const>::is_reference);
    CHECK_FALSE(TypeReference<void const* const>::is_reference);
    CHECK_FALSE(TypeReference<bool const* const>::is_reference);
    CHECK_FALSE(TypeReference<char const* const>::is_reference);
    CHECK_FALSE(TypeReference<wchar_t const* const>::is_reference);
    CHECK_FALSE(TypeReference<signed char const* const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned char const* const>::is_reference);
    CHECK_FALSE(TypeReference<signed short const* const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned short const* const>::is_reference);
    CHECK_FALSE(TypeReference<signed int const* const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned int const* const>::is_reference);
    CHECK_FALSE(TypeReference<signed long int const* const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long int const* const>::is_reference);
    CHECK_FALSE(TypeReference<signed long long int const* const>::is_reference);
    CHECK_FALSE(TypeReference<unsigned long long int const* const>::is_reference);
    CHECK_FALSE(TypeReference<float const* const>::is_reference);
    CHECK_FALSE(TypeReference<double const* const>::is_reference);
    CHECK_FALSE(TypeReference<long double const* const>::is_reference);
}
