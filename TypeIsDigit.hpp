/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEISDIGIT_HPP
#define OCL_GUARD_TYPEINFO_TYPEISDIGIT_HPP

#include "TypeDigits.hpp"

namespace ocl
{

template<typename CharType>
struct TypeIsDigit
{
    typedef CharType char_type;

    static bool IsDigit(char_type ch) noexcept
    {
        typedef TypeDigits<char_type> type_digits_type;

        switch (ch)
        {
        case type_digits_type::Zero:
        case type_digits_type::One:
        case type_digits_type::Two:
        case type_digits_type::Three:
        case type_digits_type::Four:
        case type_digits_type::Five:
        case type_digits_type::Six:
        case type_digits_type::Seven:
        case type_digits_type::Eight:
        case type_digits_type::Nine:
            return true;
        }
        return false;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEISDIGIT_HPP
