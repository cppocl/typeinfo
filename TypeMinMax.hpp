/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEMINMAX_HPP
#define OCL_GUARD_TYPEINFO_TYPEMINMAX_HPP

#include "TypeIn.hpp"

namespace ocl
{

template<typename Type>
struct TypeMinMax
{
    typedef Type type;
    typedef typename TypeIn<Type>::in_type in_type;

    static Type Min(in_type a, in_type b)
    {
        // Always return a when <= b.
        return b < a ? b : a;
    }

    static Type Max(in_type a, in_type b)
    {
        // Only return a when > b.
        return b < a ? a : b;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEMINMAX_HPP
