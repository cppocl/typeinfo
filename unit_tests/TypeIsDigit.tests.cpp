/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIsDigit.hpp"

TEST_MEMBER_FUNCTION(TypeIsDigit, IsDigit, char_type)
{
    {
        typedef ocl::TypeIsDigit<char> type_is_digit_type;
        CHECK_TRUE(type_is_digit_type::IsDigit('0'));
        CHECK_TRUE(type_is_digit_type::IsDigit('1'));
        CHECK_TRUE(type_is_digit_type::IsDigit('2'));
        CHECK_TRUE(type_is_digit_type::IsDigit('3'));
        CHECK_TRUE(type_is_digit_type::IsDigit('4'));
        CHECK_TRUE(type_is_digit_type::IsDigit('5'));
        CHECK_TRUE(type_is_digit_type::IsDigit('6'));
        CHECK_TRUE(type_is_digit_type::IsDigit('7'));
        CHECK_TRUE(type_is_digit_type::IsDigit('8'));
        CHECK_TRUE(type_is_digit_type::IsDigit('9'));
    }

    {
        typedef ocl::TypeIsDigit<wchar_t> type_is_digit_type;
        CHECK_TRUE(type_is_digit_type::IsDigit(L'0'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'1'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'2'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'3'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'4'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'5'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'6'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'7'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'8'));
        CHECK_TRUE(type_is_digit_type::IsDigit(L'9'));
    }
}
