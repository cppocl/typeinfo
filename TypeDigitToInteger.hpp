/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEDIGITTOINTEGER_HPP
#define OCL_GUARD_TYPEINFO_TYPEDIGITTOINTEGER_HPP

#include "TypeIsInteger.hpp"
#include "TypeDigits.hpp"

namespace ocl
{

template<typename CharType, typename IntType>
struct TypeDigitToInteger
{
    typedef CharType             char_type;
    typedef IntType              int_type;
    typedef TypeDigits<CharType> type_digits_type;

    /// As only 0 to 9 can be returned by ToInteger,
    /// 16 will always be a valid value for all integer types,
    /// and so can be used to signify an error.
    /// Also if there is a need to test bits, then 16 (0x10) can also be
    /// used as an alternative way to test for errors.
    static const int_type invalid = 16;

    /// Convert an integer value between 0 and 9 to the equivalent character.
    /// If the value is not within range then the invalid value is returned.
    static IntType ToInteger(char_type ch) noexcept
    {
        switch (ch)
        {
            case type_digits_type::Zero:  return static_cast<IntType>(0);
            case type_digits_type::One:   return static_cast<IntType>(1);
            case type_digits_type::Two:   return static_cast<IntType>(2);
            case type_digits_type::Three: return static_cast<IntType>(3);
            case type_digits_type::Four:  return static_cast<IntType>(4);
            case type_digits_type::Five:  return static_cast<IntType>(5);
            case type_digits_type::Six:   return static_cast<IntType>(6);
            case type_digits_type::Seven: return static_cast<IntType>(7);
            case type_digits_type::Eight: return static_cast<IntType>(8);
            case type_digits_type::Nine:  return static_cast<IntType>(9);
        }
        return invalid;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEDIGITTOINTEGER_HPP
