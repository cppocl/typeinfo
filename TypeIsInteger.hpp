/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEISINTEGER_HPP
#define OCL_GUARD_TYPEINFO_TYPEISINTEGER_HPP

namespace ocl
{

template<typename Type>
struct TypeIsInteger
{
    static bool const is_integer = false; // is integer type?
};

template<>
struct TypeIsInteger<signed char>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<unsigned char>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<signed short>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<unsigned short>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<signed int>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<unsigned int>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<signed long int>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<unsigned long int>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<signed long long int>
{
    static bool const is_integer = true;
};

template<>
struct TypeIsInteger<unsigned long long int>
{
    static bool const is_integer = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEISINTEGER_HPP
