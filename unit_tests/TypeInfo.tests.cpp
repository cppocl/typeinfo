/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeInfo.hpp"
#include <string.h>

using ocl::TypeInfo;

namespace
{

int widecmp(wchar_t const* str1, wchar_t const* str2)
{
    while ((*str1 != L'\0') && (*str2 != L'\0'))
    {
        ++str1;
        ++str2;
    }
    return static_cast<int>(*str1 - *str2);
}

struct Huge
{
    double data[1000]; // This won't be smaller than a void*
};

}

TEST_MEMBER_FUNCTION(TypeInfo, is_large, NA)
{
    CHECK_TRUE(TypeInfo<Huge>::is_large);
    CHECK_FALSE(TypeInfo<signed char>::is_large);
    CHECK_FALSE(TypeInfo<void*>::is_large);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_raw, NA)
{
    CHECK_TRUE(TypeInfo<Huge>::is_raw);
    CHECK_TRUE(TypeInfo<bool>::is_raw);
    CHECK_TRUE(TypeInfo<char>::is_raw);
    CHECK_TRUE(TypeInfo<wchar_t>::is_raw);
    CHECK_TRUE(TypeInfo<signed char>::is_raw);
    CHECK_TRUE(TypeInfo<unsigned char>::is_raw);
    CHECK_TRUE(TypeInfo<signed short>::is_raw);
    CHECK_TRUE(TypeInfo<unsigned short>::is_raw);
    CHECK_TRUE(TypeInfo<signed int>::is_raw);
    CHECK_TRUE(TypeInfo<unsigned int>::is_raw);
    CHECK_TRUE(TypeInfo<signed long int>::is_raw);
    CHECK_TRUE(TypeInfo<unsigned long int>::is_raw);
    CHECK_TRUE(TypeInfo<signed long long int>::is_raw);
    CHECK_TRUE(TypeInfo<unsigned long long int>::is_raw);
    CHECK_TRUE(TypeInfo<float>::is_raw);
    CHECK_TRUE(TypeInfo<double>::is_raw);
    CHECK_TRUE(TypeInfo<long double>::is_raw);

    CHECK_FALSE(TypeInfo<Huge const>::is_raw);
    CHECK_FALSE(TypeInfo<bool const>::is_raw);
    CHECK_FALSE(TypeInfo<char const>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t const>::is_raw);
    CHECK_FALSE(TypeInfo<signed char const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char const>::is_raw);
    CHECK_FALSE(TypeInfo<signed short const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short const>::is_raw);
    CHECK_FALSE(TypeInfo<signed int const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int const>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int const>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int const>::is_raw);
    CHECK_FALSE(TypeInfo<float const>::is_raw);
    CHECK_FALSE(TypeInfo<double const>::is_raw);
    CHECK_FALSE(TypeInfo<long double const>::is_raw);

    CHECK_FALSE(TypeInfo<Huge volatile>::is_raw);
    CHECK_FALSE(TypeInfo<bool volatile>::is_raw);
    CHECK_FALSE(TypeInfo<char volatile>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t volatile>::is_raw);
    CHECK_FALSE(TypeInfo<signed char volatile>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char volatile>::is_raw);
    CHECK_FALSE(TypeInfo<signed short volatile>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short volatile>::is_raw);
    CHECK_FALSE(TypeInfo<signed int volatile>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int volatile>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int volatile>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int volatile>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int volatile>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int volatile>::is_raw);
    CHECK_FALSE(TypeInfo<float volatile>::is_raw);
    CHECK_FALSE(TypeInfo<double volatile>::is_raw);
    CHECK_FALSE(TypeInfo<long double volatile>::is_raw);

    CHECK_FALSE(TypeInfo<Huge&>::is_raw);
    CHECK_FALSE(TypeInfo<bool&>::is_raw);
    CHECK_FALSE(TypeInfo<char&>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t&>::is_raw);
    CHECK_FALSE(TypeInfo<signed char&>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char&>::is_raw);
    CHECK_FALSE(TypeInfo<signed short&>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short&>::is_raw);
    CHECK_FALSE(TypeInfo<signed int&>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int&>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int&>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int&>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int&>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int&>::is_raw);
    CHECK_FALSE(TypeInfo<float&>::is_raw);
    CHECK_FALSE(TypeInfo<double&>::is_raw);
    CHECK_FALSE(TypeInfo<long double&>::is_raw);

    CHECK_FALSE(TypeInfo<Huge*>::is_raw);
    CHECK_FALSE(TypeInfo<void*>::is_raw);
    CHECK_FALSE(TypeInfo<bool*>::is_raw);
    CHECK_FALSE(TypeInfo<char*>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t*>::is_raw);
    CHECK_FALSE(TypeInfo<signed char*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char*>::is_raw);
    CHECK_FALSE(TypeInfo<signed short*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short*>::is_raw);
    CHECK_FALSE(TypeInfo<signed int*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int*>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int*>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int*>::is_raw);
    CHECK_FALSE(TypeInfo<float*>::is_raw);
    CHECK_FALSE(TypeInfo<double*>::is_raw);
    CHECK_FALSE(TypeInfo<long double*>::is_raw);

    CHECK_FALSE(TypeInfo<Huge**>::is_raw);
    CHECK_FALSE(TypeInfo<void**>::is_raw);
    CHECK_FALSE(TypeInfo<bool**>::is_raw);
    CHECK_FALSE(TypeInfo<char**>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t**>::is_raw);
    CHECK_FALSE(TypeInfo<signed char**>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char**>::is_raw);
    CHECK_FALSE(TypeInfo<signed short**>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short**>::is_raw);
    CHECK_FALSE(TypeInfo<signed int**>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int**>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int**>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int**>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int**>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int**>::is_raw);
    CHECK_FALSE(TypeInfo<float**>::is_raw);
    CHECK_FALSE(TypeInfo<double**>::is_raw);
    CHECK_FALSE(TypeInfo<long double**>::is_raw);

    CHECK_FALSE(TypeInfo<Huge const*>::is_raw);
    CHECK_FALSE(TypeInfo<void const*>::is_raw);
    CHECK_FALSE(TypeInfo<bool const*>::is_raw);
    CHECK_FALSE(TypeInfo<char const*>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t const*>::is_raw);
    CHECK_FALSE(TypeInfo<signed char const*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char const*>::is_raw);
    CHECK_FALSE(TypeInfo<signed short const*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short const*>::is_raw);
    CHECK_FALSE(TypeInfo<signed int const*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int const*>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int const*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int const*>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int const*>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int const*>::is_raw);
    CHECK_FALSE(TypeInfo<float const*>::is_raw);
    CHECK_FALSE(TypeInfo<double const*>::is_raw);
    CHECK_FALSE(TypeInfo<long double const*>::is_raw);

    CHECK_FALSE(TypeInfo<Huge const* const>::is_raw);
    CHECK_FALSE(TypeInfo<void const* const>::is_raw);
    CHECK_FALSE(TypeInfo<bool const* const>::is_raw);
    CHECK_FALSE(TypeInfo<char const* const>::is_raw);
    CHECK_FALSE(TypeInfo<wchar_t const* const>::is_raw);
    CHECK_FALSE(TypeInfo<signed char const* const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned char const* const>::is_raw);
    CHECK_FALSE(TypeInfo<signed short const* const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned short const* const>::is_raw);
    CHECK_FALSE(TypeInfo<signed int const* const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned int const* const>::is_raw);
    CHECK_FALSE(TypeInfo<signed long int const* const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long int const* const>::is_raw);
    CHECK_FALSE(TypeInfo<signed long long int const* const>::is_raw);
    CHECK_FALSE(TypeInfo<unsigned long long int const* const>::is_raw);
    CHECK_FALSE(TypeInfo<float const* const>::is_raw);
    CHECK_FALSE(TypeInfo<double const* const>::is_raw);
    CHECK_FALSE(TypeInfo<long double const* const>::is_raw);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_pod, NA)
{
    CHECK_TRUE(TypeInfo<bool>::is_pod);
    CHECK_TRUE(TypeInfo<char>::is_pod);
    CHECK_TRUE(TypeInfo<wchar_t>::is_pod);
    CHECK_TRUE(TypeInfo<signed char>::is_pod);
    CHECK_TRUE(TypeInfo<unsigned char>::is_pod);
    CHECK_TRUE(TypeInfo<signed short>::is_pod);
    CHECK_TRUE(TypeInfo<unsigned short>::is_pod);
    CHECK_TRUE(TypeInfo<signed int>::is_pod);
    CHECK_TRUE(TypeInfo<unsigned int>::is_pod);
    CHECK_TRUE(TypeInfo<signed long int>::is_pod);
    CHECK_TRUE(TypeInfo<unsigned long int>::is_pod);
    CHECK_TRUE(TypeInfo<signed long long int>::is_pod);
    CHECK_TRUE(TypeInfo<unsigned long long int>::is_pod);
    CHECK_TRUE(TypeInfo<float>::is_pod);
    CHECK_TRUE(TypeInfo<double>::is_pod);
    CHECK_TRUE(TypeInfo<long double>::is_pod);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_constant, NA)
{
    CHECK_TRUE(TypeInfo<Huge const>::is_constant);
    CHECK_TRUE(TypeInfo<bool const>::is_constant);
    CHECK_TRUE(TypeInfo<char const>::is_constant);
    CHECK_TRUE(TypeInfo<wchar_t const>::is_constant);
    CHECK_TRUE(TypeInfo<signed char const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned char const>::is_constant);
    CHECK_TRUE(TypeInfo<signed short const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned short const>::is_constant);
    CHECK_TRUE(TypeInfo<signed int const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned int const>::is_constant);
    CHECK_TRUE(TypeInfo<signed long int const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned long int const>::is_constant);
    CHECK_TRUE(TypeInfo<signed long long int const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned long long int const>::is_constant);
    CHECK_TRUE(TypeInfo<float const>::is_constant);
    CHECK_TRUE(TypeInfo<double const>::is_constant);
    CHECK_TRUE(TypeInfo<long double const>::is_constant);

    CHECK_TRUE(TypeInfo<Huge const* const>::is_constant);
    CHECK_TRUE(TypeInfo<void const* const>::is_constant);
    CHECK_TRUE(TypeInfo<bool const* const>::is_constant);
    CHECK_TRUE(TypeInfo<char const* const>::is_constant);
    CHECK_TRUE(TypeInfo<wchar_t const* const>::is_constant);
    CHECK_TRUE(TypeInfo<signed char const* const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned char const* const>::is_constant);
    CHECK_TRUE(TypeInfo<signed short const* const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned short const* const>::is_constant);
    CHECK_TRUE(TypeInfo<signed int const* const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned int const* const>::is_constant);
    CHECK_TRUE(TypeInfo<signed long int const* const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned long int const* const>::is_constant);
    CHECK_TRUE(TypeInfo<signed long long int const* const>::is_constant);
    CHECK_TRUE(TypeInfo<unsigned long long int const* const>::is_constant);
    CHECK_TRUE(TypeInfo<float const* const>::is_constant);
    CHECK_TRUE(TypeInfo<double const* const>::is_constant);
    CHECK_TRUE(TypeInfo<long double const* const>::is_constant);

    CHECK_FALSE(TypeInfo<Huge>::is_constant);
    CHECK_FALSE(TypeInfo<bool>::is_constant);
    CHECK_FALSE(TypeInfo<char>::is_constant);
    CHECK_FALSE(TypeInfo<wchar_t>::is_constant);
    CHECK_FALSE(TypeInfo<signed char>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned char>::is_constant);
    CHECK_FALSE(TypeInfo<signed short>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned short>::is_constant);
    CHECK_FALSE(TypeInfo<signed int>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned int>::is_constant);
    CHECK_FALSE(TypeInfo<signed long int>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long int>::is_constant);
    CHECK_FALSE(TypeInfo<signed long long int>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long long int>::is_constant);
    CHECK_FALSE(TypeInfo<float>::is_constant);
    CHECK_FALSE(TypeInfo<double>::is_constant);
    CHECK_FALSE(TypeInfo<long double>::is_constant);

    CHECK_FALSE(TypeInfo<Huge&>::is_constant);
    CHECK_FALSE(TypeInfo<bool&>::is_constant);
    CHECK_FALSE(TypeInfo<char&>::is_constant);
    CHECK_FALSE(TypeInfo<wchar_t&>::is_constant);
    CHECK_FALSE(TypeInfo<signed char&>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned char&>::is_constant);
    CHECK_FALSE(TypeInfo<signed short&>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned short&>::is_constant);
    CHECK_FALSE(TypeInfo<signed int&>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned int&>::is_constant);
    CHECK_FALSE(TypeInfo<signed long int&>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long int&>::is_constant);
    CHECK_FALSE(TypeInfo<signed long long int&>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long long int&>::is_constant);
    CHECK_FALSE(TypeInfo<float&>::is_constant);
    CHECK_FALSE(TypeInfo<double&>::is_constant);
    CHECK_FALSE(TypeInfo<long double&>::is_constant);

    CHECK_FALSE(TypeInfo<Huge volatile>::is_constant);
    CHECK_FALSE(TypeInfo<bool volatile>::is_constant);
    CHECK_FALSE(TypeInfo<char volatile>::is_constant);
    CHECK_FALSE(TypeInfo<wchar_t volatile>::is_constant);
    CHECK_FALSE(TypeInfo<signed char volatile>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned char volatile>::is_constant);
    CHECK_FALSE(TypeInfo<signed short volatile>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned short volatile>::is_constant);
    CHECK_FALSE(TypeInfo<signed int volatile>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned int volatile>::is_constant);
    CHECK_FALSE(TypeInfo<signed long int volatile>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long int volatile>::is_constant);
    CHECK_FALSE(TypeInfo<signed long long int volatile>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long long int volatile>::is_constant);
    CHECK_FALSE(TypeInfo<float volatile>::is_constant);
    CHECK_FALSE(TypeInfo<double volatile>::is_constant);
    CHECK_FALSE(TypeInfo<long double volatile>::is_constant);

    CHECK_FALSE(TypeInfo<Huge*>::is_constant);
    CHECK_FALSE(TypeInfo<void*>::is_constant);
    CHECK_FALSE(TypeInfo<bool*>::is_constant);
    CHECK_FALSE(TypeInfo<char*>::is_constant);
    CHECK_FALSE(TypeInfo<wchar_t*>::is_constant);
    CHECK_FALSE(TypeInfo<signed char*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned char*>::is_constant);
    CHECK_FALSE(TypeInfo<signed short*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned short*>::is_constant);
    CHECK_FALSE(TypeInfo<signed int*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned int*>::is_constant);
    CHECK_FALSE(TypeInfo<signed long int*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long int*>::is_constant);
    CHECK_FALSE(TypeInfo<signed long long int*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long long int*>::is_constant);
    CHECK_FALSE(TypeInfo<float*>::is_constant);
    CHECK_FALSE(TypeInfo<double*>::is_constant);
    CHECK_FALSE(TypeInfo<long double*>::is_constant);

    CHECK_FALSE(TypeInfo<Huge**>::is_constant);
    CHECK_FALSE(TypeInfo<void**>::is_constant);
    CHECK_FALSE(TypeInfo<bool**>::is_constant);
    CHECK_FALSE(TypeInfo<char**>::is_constant);
    CHECK_FALSE(TypeInfo<wchar_t**>::is_constant);
    CHECK_FALSE(TypeInfo<signed char**>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned char**>::is_constant);
    CHECK_FALSE(TypeInfo<signed short**>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned short**>::is_constant);
    CHECK_FALSE(TypeInfo<signed int**>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned int**>::is_constant);
    CHECK_FALSE(TypeInfo<signed long int**>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long int**>::is_constant);
    CHECK_FALSE(TypeInfo<signed long long int**>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long long int**>::is_constant);
    CHECK_FALSE(TypeInfo<float**>::is_constant);
    CHECK_FALSE(TypeInfo<double**>::is_constant);
    CHECK_FALSE(TypeInfo<long double**>::is_constant);

    CHECK_FALSE(TypeInfo<Huge const*>::is_constant);
    CHECK_FALSE(TypeInfo<void const*>::is_constant);
    CHECK_FALSE(TypeInfo<bool const*>::is_constant);
    CHECK_FALSE(TypeInfo<char const*>::is_constant);
    CHECK_FALSE(TypeInfo<wchar_t const*>::is_constant);
    CHECK_FALSE(TypeInfo<signed char const*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned char const*>::is_constant);
    CHECK_FALSE(TypeInfo<signed short const*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned short const*>::is_constant);
    CHECK_FALSE(TypeInfo<signed int const*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned int const*>::is_constant);
    CHECK_FALSE(TypeInfo<signed long int const*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long int const*>::is_constant);
    CHECK_FALSE(TypeInfo<signed long long int const*>::is_constant);
    CHECK_FALSE(TypeInfo<unsigned long long int const*>::is_constant);
    CHECK_FALSE(TypeInfo<float const*>::is_constant);
    CHECK_FALSE(TypeInfo<double const*>::is_constant);
    CHECK_FALSE(TypeInfo<long double const*>::is_constant);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_pointer, NA)
{
    CHECK_TRUE(TypeInfo<Huge*>::is_pointer);
    CHECK_TRUE(TypeInfo<void*>::is_pointer);
    CHECK_TRUE(TypeInfo<bool*>::is_pointer);
    CHECK_TRUE(TypeInfo<char*>::is_pointer);
    CHECK_TRUE(TypeInfo<wchar_t*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed char*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned char*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed short*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned short*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed int*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned int*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed long int*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned long int*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed long long int*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned long long int*>::is_pointer);
    CHECK_TRUE(TypeInfo<float*>::is_pointer);
    CHECK_TRUE(TypeInfo<double*>::is_pointer);
    CHECK_TRUE(TypeInfo<long double*>::is_pointer);

    CHECK_TRUE(TypeInfo<Huge**>::is_pointer);
    CHECK_TRUE(TypeInfo<void**>::is_pointer);
    CHECK_TRUE(TypeInfo<bool**>::is_pointer);
    CHECK_TRUE(TypeInfo<char**>::is_pointer);
    CHECK_TRUE(TypeInfo<wchar_t**>::is_pointer);
    CHECK_TRUE(TypeInfo<signed char**>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned char**>::is_pointer);
    CHECK_TRUE(TypeInfo<signed short**>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned short**>::is_pointer);
    CHECK_TRUE(TypeInfo<signed int**>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned int**>::is_pointer);
    CHECK_TRUE(TypeInfo<signed long int**>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned long int**>::is_pointer);
    CHECK_TRUE(TypeInfo<signed long long int**>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned long long int**>::is_pointer);
    CHECK_TRUE(TypeInfo<float**>::is_pointer);
    CHECK_TRUE(TypeInfo<double**>::is_pointer);
    CHECK_TRUE(TypeInfo<long double**>::is_pointer);

    CHECK_TRUE(TypeInfo<Huge const*>::is_pointer);
    CHECK_TRUE(TypeInfo<void const*>::is_pointer);
    CHECK_TRUE(TypeInfo<bool const*>::is_pointer);
    CHECK_TRUE(TypeInfo<char const*>::is_pointer);
    CHECK_TRUE(TypeInfo<wchar_t const*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed char const*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned char const*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed short const*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned short const*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed int const*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned int const*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed long int const*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned long int const*>::is_pointer);
    CHECK_TRUE(TypeInfo<signed long long int const*>::is_pointer);
    CHECK_TRUE(TypeInfo<unsigned long long int const*>::is_pointer);
    CHECK_TRUE(TypeInfo<float const*>::is_pointer);
    CHECK_TRUE(TypeInfo<double const*>::is_pointer);
    CHECK_TRUE(TypeInfo<long double const*>::is_pointer);

    CHECK_FALSE(TypeInfo<Huge>::is_pointer);
    CHECK_FALSE(TypeInfo<bool>::is_pointer);
    CHECK_FALSE(TypeInfo<char>::is_pointer);
    CHECK_FALSE(TypeInfo<wchar_t>::is_pointer);
    CHECK_FALSE(TypeInfo<signed char>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned char>::is_pointer);
    CHECK_FALSE(TypeInfo<signed short>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned short>::is_pointer);
    CHECK_FALSE(TypeInfo<signed int>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned int>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long int>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long int>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long long int>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long long int>::is_pointer);
    CHECK_FALSE(TypeInfo<float>::is_pointer);
    CHECK_FALSE(TypeInfo<double>::is_pointer);
    CHECK_FALSE(TypeInfo<long double>::is_pointer);

    CHECK_FALSE(TypeInfo<Huge const>::is_pointer);
    CHECK_FALSE(TypeInfo<bool const>::is_pointer);
    CHECK_FALSE(TypeInfo<char const>::is_pointer);
    CHECK_FALSE(TypeInfo<wchar_t const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed char const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned char const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed short const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned short const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed int const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned int const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long int const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long int const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long long int const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long long int const>::is_pointer);
    CHECK_FALSE(TypeInfo<float const>::is_pointer);
    CHECK_FALSE(TypeInfo<double const>::is_pointer);
    CHECK_FALSE(TypeInfo<long double const>::is_pointer);

    CHECK_FALSE(TypeInfo<Huge volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<bool volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<char volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<wchar_t volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<signed char volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned char volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<signed short volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned short volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<signed int volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned int volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long int volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long int volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long long int volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long long int volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<float volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<double volatile>::is_pointer);
    CHECK_FALSE(TypeInfo<long double volatile>::is_pointer);

    CHECK_FALSE(TypeInfo<Huge&>::is_pointer);
    CHECK_FALSE(TypeInfo<bool&>::is_pointer);
    CHECK_FALSE(TypeInfo<char&>::is_pointer);
    CHECK_FALSE(TypeInfo<wchar_t&>::is_pointer);
    CHECK_FALSE(TypeInfo<signed char&>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned char&>::is_pointer);
    CHECK_FALSE(TypeInfo<signed short&>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned short&>::is_pointer);
    CHECK_FALSE(TypeInfo<signed int&>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned int&>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long int&>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long int&>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long long int&>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long long int&>::is_pointer);
    CHECK_FALSE(TypeInfo<float&>::is_pointer);
    CHECK_FALSE(TypeInfo<double&>::is_pointer);
    CHECK_FALSE(TypeInfo<long double&>::is_pointer);

    CHECK_FALSE(TypeInfo<Huge const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<void const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<bool const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<char const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<wchar_t const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed char const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned char const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed short const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned short const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed int const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned int const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long int const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long int const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<signed long long int const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<unsigned long long int const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<float const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<double const* const>::is_pointer);
    CHECK_FALSE(TypeInfo<long double const* const>::is_pointer);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_reference, NA)
{
    CHECK_TRUE(TypeInfo<Huge&>::is_reference);
    CHECK_TRUE(TypeInfo<bool&>::is_reference);
    CHECK_TRUE(TypeInfo<char&>::is_reference);
    CHECK_TRUE(TypeInfo<wchar_t&>::is_reference);
    CHECK_TRUE(TypeInfo<signed char&>::is_reference);
    CHECK_TRUE(TypeInfo<unsigned char&>::is_reference);
    CHECK_TRUE(TypeInfo<signed short&>::is_reference);
    CHECK_TRUE(TypeInfo<unsigned short&>::is_reference);
    CHECK_TRUE(TypeInfo<signed int&>::is_reference);
    CHECK_TRUE(TypeInfo<unsigned int&>::is_reference);
    CHECK_TRUE(TypeInfo<signed long int&>::is_reference);
    CHECK_TRUE(TypeInfo<unsigned long int&>::is_reference);
    CHECK_TRUE(TypeInfo<signed long long int&>::is_reference);
    CHECK_TRUE(TypeInfo<unsigned long long int&>::is_reference);
    CHECK_TRUE(TypeInfo<float&>::is_reference);
    CHECK_TRUE(TypeInfo<double&>::is_reference);
    CHECK_TRUE(TypeInfo<long double&>::is_reference);

    CHECK_FALSE(TypeInfo<Huge>::is_reference);
    CHECK_FALSE(TypeInfo<bool>::is_reference);
    CHECK_FALSE(TypeInfo<char>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t>::is_reference);
    CHECK_FALSE(TypeInfo<signed char>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char>::is_reference);
    CHECK_FALSE(TypeInfo<signed short>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short>::is_reference);
    CHECK_FALSE(TypeInfo<signed int>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int>::is_reference);
    CHECK_FALSE(TypeInfo<float>::is_reference);
    CHECK_FALSE(TypeInfo<double>::is_reference);
    CHECK_FALSE(TypeInfo<long double>::is_reference);

    CHECK_FALSE(TypeInfo<Huge const>::is_reference);
    CHECK_FALSE(TypeInfo<bool const>::is_reference);
    CHECK_FALSE(TypeInfo<char const>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t const>::is_reference);
    CHECK_FALSE(TypeInfo<signed char const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char const>::is_reference);
    CHECK_FALSE(TypeInfo<signed short const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short const>::is_reference);
    CHECK_FALSE(TypeInfo<signed int const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int const>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int const>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int const>::is_reference);
    CHECK_FALSE(TypeInfo<float const>::is_reference);
    CHECK_FALSE(TypeInfo<double const>::is_reference);
    CHECK_FALSE(TypeInfo<long double const>::is_reference);

    CHECK_FALSE(TypeInfo<Huge volatile>::is_reference);
    CHECK_FALSE(TypeInfo<bool volatile>::is_reference);
    CHECK_FALSE(TypeInfo<char volatile>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t volatile>::is_reference);
    CHECK_FALSE(TypeInfo<signed char volatile>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char volatile>::is_reference);
    CHECK_FALSE(TypeInfo<signed short volatile>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short volatile>::is_reference);
    CHECK_FALSE(TypeInfo<signed int volatile>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int volatile>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int volatile>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int volatile>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int volatile>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int volatile>::is_reference);
    CHECK_FALSE(TypeInfo<float volatile>::is_reference);
    CHECK_FALSE(TypeInfo<double volatile>::is_reference);
    CHECK_FALSE(TypeInfo<long double volatile>::is_reference);

    CHECK_FALSE(TypeInfo<Huge*>::is_reference);
    CHECK_FALSE(TypeInfo<void*>::is_reference);
    CHECK_FALSE(TypeInfo<bool*>::is_reference);
    CHECK_FALSE(TypeInfo<char*>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t*>::is_reference);
    CHECK_FALSE(TypeInfo<signed char*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char*>::is_reference);
    CHECK_FALSE(TypeInfo<signed short*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short*>::is_reference);
    CHECK_FALSE(TypeInfo<signed int*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int*>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int*>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int*>::is_reference);
    CHECK_FALSE(TypeInfo<float*>::is_reference);
    CHECK_FALSE(TypeInfo<double*>::is_reference);
    CHECK_FALSE(TypeInfo<long double*>::is_reference);

    CHECK_FALSE(TypeInfo<Huge**>::is_reference);
    CHECK_FALSE(TypeInfo<void**>::is_reference);
    CHECK_FALSE(TypeInfo<bool**>::is_reference);
    CHECK_FALSE(TypeInfo<char**>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t**>::is_reference);
    CHECK_FALSE(TypeInfo<signed char**>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char**>::is_reference);
    CHECK_FALSE(TypeInfo<signed short**>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short**>::is_reference);
    CHECK_FALSE(TypeInfo<signed int**>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int**>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int**>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int**>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int**>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int**>::is_reference);
    CHECK_FALSE(TypeInfo<float**>::is_reference);
    CHECK_FALSE(TypeInfo<double**>::is_reference);
    CHECK_FALSE(TypeInfo<long double**>::is_reference);

    CHECK_FALSE(TypeInfo<Huge const*>::is_reference);
    CHECK_FALSE(TypeInfo<void const*>::is_reference);
    CHECK_FALSE(TypeInfo<bool const*>::is_reference);
    CHECK_FALSE(TypeInfo<char const*>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t const*>::is_reference);
    CHECK_FALSE(TypeInfo<signed char const*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char const*>::is_reference);
    CHECK_FALSE(TypeInfo<signed short const*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short const*>::is_reference);
    CHECK_FALSE(TypeInfo<signed int const*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int const*>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int const*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int const*>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int const*>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int const*>::is_reference);
    CHECK_FALSE(TypeInfo<float const*>::is_reference);
    CHECK_FALSE(TypeInfo<double const*>::is_reference);
    CHECK_FALSE(TypeInfo<long double const*>::is_reference);

    CHECK_FALSE(TypeInfo<Huge const* const>::is_reference);
    CHECK_FALSE(TypeInfo<void const* const>::is_reference);
    CHECK_FALSE(TypeInfo<bool const* const>::is_reference);
    CHECK_FALSE(TypeInfo<char const* const>::is_reference);
    CHECK_FALSE(TypeInfo<wchar_t const* const>::is_reference);
    CHECK_FALSE(TypeInfo<signed char const* const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned char const* const>::is_reference);
    CHECK_FALSE(TypeInfo<signed short const* const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned short const* const>::is_reference);
    CHECK_FALSE(TypeInfo<signed int const* const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned int const* const>::is_reference);
    CHECK_FALSE(TypeInfo<signed long int const* const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long int const* const>::is_reference);
    CHECK_FALSE(TypeInfo<signed long long int const* const>::is_reference);
    CHECK_FALSE(TypeInfo<unsigned long long int const* const>::is_reference);
    CHECK_FALSE(TypeInfo<float const* const>::is_reference);
    CHECK_FALSE(TypeInfo<double const* const>::is_reference);
    CHECK_FALSE(TypeInfo<long double const* const>::is_reference);
}

TEST_MEMBER_FUNCTION(TypeInfo, has_previous, NA)
{
    CHECK_FALSE(TypeInfo<bool>::has_previous);
    CHECK_FALSE(TypeInfo<char>::has_previous);
    CHECK_FALSE(TypeInfo<wchar_t>::has_previous);
    CHECK_FALSE(TypeInfo<signed char>::has_previous);
    CHECK_FALSE(TypeInfo<unsigned char>::has_previous);
    CHECK_TRUE(TypeInfo<signed short>::has_previous);
    CHECK_TRUE(TypeInfo<unsigned short>::has_previous);
    CHECK_TRUE(TypeInfo<signed int>::has_previous);
    CHECK_TRUE(TypeInfo<unsigned int>::has_previous);
    CHECK_TRUE(TypeInfo<signed long int>::has_previous);
    CHECK_TRUE(TypeInfo<unsigned long int>::has_previous);
    CHECK_TRUE(TypeInfo<signed long long int>::has_previous);
    CHECK_TRUE(TypeInfo<unsigned long long int>::has_previous);
    CHECK_TRUE(TypeInfo<double>::has_previous);
    CHECK_TRUE(TypeInfo<long double>::has_previous);
    CHECK_FALSE(TypeInfo<float>::has_previous);
    CHECK_FALSE(TypeInfo<Huge>::has_previous);
}

TEST_MEMBER_FUNCTION(TypeInfo, has_next, NA)
{
    CHECK_FALSE(TypeInfo<bool>::has_next);
    CHECK_FALSE(TypeInfo<char>::has_next);
    CHECK_FALSE(TypeInfo<wchar_t>::has_next);
    CHECK_TRUE(TypeInfo<signed char>::has_next);
    CHECK_TRUE(TypeInfo<unsigned char>::has_next);
    CHECK_TRUE(TypeInfo<signed short>::has_next);
    CHECK_TRUE(TypeInfo<unsigned short>::has_next);
    CHECK_TRUE(TypeInfo<signed int>::has_next);
    CHECK_TRUE(TypeInfo<unsigned int>::has_next);
    CHECK_TRUE(TypeInfo<signed long int>::has_next);
    CHECK_TRUE(TypeInfo<unsigned long int>::has_next);
    CHECK_TRUE(TypeInfo<float>::has_next);
    CHECK_TRUE(TypeInfo<double>::has_next);
    CHECK_FALSE(TypeInfo<signed long long int>::has_next);
    CHECK_FALSE(TypeInfo<unsigned long long int>::has_next);
    CHECK_FALSE(TypeInfo<long double>::has_next);
    CHECK_FALSE(TypeInfo<Huge>::has_next);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_integer, NA)
{
    CHECK_FALSE(TypeInfo<Huge>::is_integer);
    CHECK_FALSE(TypeInfo<bool>::is_integer);
    CHECK_FALSE(TypeInfo<char>::is_integer);
    CHECK_FALSE(TypeInfo<wchar_t>::is_integer);
    CHECK_FALSE(TypeInfo<float>::is_integer);
    CHECK_FALSE(TypeInfo<double>::is_integer);
    CHECK_FALSE(TypeInfo<long double>::is_integer);
    CHECK_TRUE(TypeInfo<signed char>::is_integer);
    CHECK_TRUE(TypeInfo<unsigned char>::is_integer);
    CHECK_TRUE(TypeInfo<signed short>::is_integer);
    CHECK_TRUE(TypeInfo<unsigned short>::is_integer);
    CHECK_TRUE(TypeInfo<signed int>::is_integer);
    CHECK_TRUE(TypeInfo<unsigned int>::is_integer);
    CHECK_TRUE(TypeInfo<signed long int>::is_integer);
    CHECK_TRUE(TypeInfo<unsigned long int>::is_integer);
    CHECK_TRUE(TypeInfo<signed long long int>::is_integer);
    CHECK_TRUE(TypeInfo<unsigned long long int>::is_integer);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_decimal, NA)
{
    CHECK_FALSE(TypeInfo<Huge>::is_decimal);
    CHECK_FALSE(TypeInfo<bool>::is_decimal);
    CHECK_FALSE(TypeInfo<char>::is_decimal);
    CHECK_FALSE(TypeInfo<wchar_t>::is_decimal);
    CHECK_TRUE(TypeInfo<float>::is_decimal);
    CHECK_TRUE(TypeInfo<double>::is_decimal);
    CHECK_TRUE(TypeInfo<long double>::is_decimal);
    CHECK_FALSE(TypeInfo<signed char>::is_decimal);
    CHECK_FALSE(TypeInfo<unsigned char>::is_decimal);
    CHECK_FALSE(TypeInfo<signed short>::is_decimal);
    CHECK_FALSE(TypeInfo<unsigned short>::is_decimal);
    CHECK_FALSE(TypeInfo<signed int>::is_decimal);
    CHECK_FALSE(TypeInfo<unsigned int>::is_decimal);
    CHECK_FALSE(TypeInfo<signed long int>::is_decimal);
    CHECK_FALSE(TypeInfo<unsigned long int>::is_decimal);
    CHECK_FALSE(TypeInfo<signed long long int>::is_decimal);
    CHECK_FALSE(TypeInfo<unsigned long long int>::is_decimal);
}

TEST_MEMBER_FUNCTION(TypeInfo, is_numeric, NA)
{
    CHECK_FALSE(TypeInfo<Huge>::is_numeric);
    CHECK_FALSE(TypeInfo<bool>::is_numeric);
    CHECK_FALSE(TypeInfo<char>::is_numeric);
    CHECK_FALSE(TypeInfo<wchar_t>::is_numeric);
    CHECK_TRUE(TypeInfo<float>::is_numeric);
    CHECK_TRUE(TypeInfo<double>::is_numeric);
    CHECK_TRUE(TypeInfo<long double>::is_numeric);
    CHECK_TRUE(TypeInfo<signed char>::is_numeric);
    CHECK_TRUE(TypeInfo<unsigned char>::is_numeric);
    CHECK_TRUE(TypeInfo<signed short>::is_numeric);
    CHECK_TRUE(TypeInfo<unsigned short>::is_numeric);
    CHECK_TRUE(TypeInfo<signed int>::is_numeric);
    CHECK_TRUE(TypeInfo<unsigned int>::is_numeric);
    CHECK_TRUE(TypeInfo<signed long int>::is_numeric);
    CHECK_TRUE(TypeInfo<unsigned long int>::is_numeric);
    CHECK_TRUE(TypeInfo<signed long long int>::is_numeric);
    CHECK_TRUE(TypeInfo<unsigned long long int>::is_numeric);
}

TEST_MEMBER_FUNCTION(TypeInfo, GetName, NA)
{
    CHECK_EQUAL(::strcmp(TypeInfo<bool>::GetName(), "bool"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<char>::GetName(), "char"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<wchar_t>::GetName(), "wchar_t"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed char>::GetName(), "signed char"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned char>::GetName(), "unsigned char"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed short>::GetName(), "signed short"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned short>::GetName(), "unsigned short"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed int>::GetName(), "signed int"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned int>::GetName(), "unsigned int"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed long int>::GetName(), "signed long int"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned long int>::GetName(), "unsigned long int"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed long long int>::GetName(), "signed long long int"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned long long int>::GetName(), "unsigned long long int"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<float>::GetName(), "float"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<double>::GetName(), "double"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<long double>::GetName(), "long double"), 0);

    CHECK_EQUAL(::strcmp(TypeInfo<bool const*>::GetName(), "bool const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<char const*>::GetName(), "char const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<wchar_t const*>::GetName(), "wchar_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed char const*>::GetName(), "signed char const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned char const*>::GetName(), "unsigned char const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed short const*>::GetName(), "signed short const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned short const*>::GetName(), "unsigned short const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed int const*>::GetName(), "signed int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned int const*>::GetName(), "unsigned int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed long int const*>::GetName(), "signed long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned long int const*>::GetName(), "unsigned long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed long long int const*>::GetName(), "signed long long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned long long int const*>::GetName(), "unsigned long long int const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<float const*>::GetName(), "float const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<double const*>::GetName(), "double const*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<long double const*>::GetName(), "long double const*"), 0);

    CHECK_EQUAL(::strcmp(TypeInfo<bool*>::GetName(), "bool*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<char*>::GetName(), "char*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<wchar_t*>::GetName(), "wchar_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed char*>::GetName(), "signed char*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned char*>::GetName(), "unsigned char*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed short*>::GetName(), "signed short*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned short*>::GetName(), "unsigned short*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed int*>::GetName(), "signed int*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned int*>::GetName(), "unsigned int*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed long int*>::GetName(), "signed long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned long int*>::GetName(), "unsigned long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<signed long long int*>::GetName(), "signed long long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<unsigned long long int*>::GetName(), "unsigned long long int*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<float*>::GetName(), "float*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<double*>::GetName(), "double*"), 0);
    CHECK_EQUAL(::strcmp(TypeInfo<long double*>::GetName(), "long double*"), 0);
}

TEST_MEMBER_FUNCTION(TypeInfo, GetWideName, NA)
{
    CHECK_EQUAL(widecmp(TypeInfo<bool>::GetWideName(), L"bool"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<char>::GetWideName(), L"char"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<wchar_t>::GetWideName(), L"wchar_t"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed char>::GetWideName(), L"signed char"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned char>::GetWideName(), L"unsigned char"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed short>::GetWideName(), L"signed short"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned short>::GetWideName(), L"unsigned short"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed int>::GetWideName(), L"signed int"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned int>::GetWideName(), L"unsigned int"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed long int>::GetWideName(), L"signed long int"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned long int>::GetWideName(), L"unsigned long int"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed long long int>::GetWideName(), L"signed long long int"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned long long int>::GetWideName(), L"unsigned long long int"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<float>::GetWideName(), L"float"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<double>::GetWideName(), L"double"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<long double>::GetWideName(), L"long double"), 0);

    CHECK_EQUAL(widecmp(TypeInfo<bool const*>::GetWideName(), L"bool const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<char const*>::GetWideName(), L"char const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<wchar_t const*>::GetWideName(), L"wchar_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed char const*>::GetWideName(), L"signed char const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned char const*>::GetWideName(), L"unsigned char const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed short const*>::GetWideName(), L"signed short const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned short const*>::GetWideName(), L"unsigned short const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed int const*>::GetWideName(), L"signed int const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned int const*>::GetWideName(), L"unsigned int const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed long int const*>::GetWideName(), L"signed long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned long int const*>::GetWideName(), L"unsigned long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed long long int const*>::GetWideName(), L"signed long long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned long long int const*>::GetWideName(), L"unsigned long long int const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<float const*>::GetWideName(), L"float const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<double const*>::GetWideName(), L"double const*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<long double const*>::GetWideName(), L"long double const*"), 0);

    CHECK_EQUAL(widecmp(TypeInfo<bool*>::GetWideName(), L"bool*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<char*>::GetWideName(), L"char*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<wchar_t*>::GetWideName(), L"wchar_t*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed char*>::GetWideName(), L"signed char*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned char*>::GetWideName(), L"unsigned char*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed short*>::GetWideName(), L"signed short*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned short*>::GetWideName(), L"unsigned short*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed int*>::GetWideName(), L"signed int*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned int*>::GetWideName(), L"unsigned int*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed long int*>::GetWideName(), L"signed long int*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned long int*>::GetWideName(), L"unsigned long int*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<signed long long int*>::GetWideName(), L"signed long long int*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<unsigned long long int*>::GetWideName(), L"unsigned long long int*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<float*>::GetWideName(), L"float*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<double*>::GetWideName(), L"double*"), 0);
    CHECK_EQUAL(widecmp(TypeInfo<long double*>::GetWideName(), L"long double*"), 0);
}
