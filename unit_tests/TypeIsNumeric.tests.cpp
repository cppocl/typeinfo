/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIsNumeric.hpp"

using ocl::TypeIsNumeric;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeIsNumeric, is_integer, NA)
{
    CHECK_FALSE(TypeIsNumeric<Huge>::is_integer);
    CHECK_FALSE(TypeIsNumeric<bool>::is_integer);
    CHECK_FALSE(TypeIsNumeric<char>::is_integer);
    CHECK_FALSE(TypeIsNumeric<wchar_t>::is_integer);
    CHECK_FALSE(TypeIsNumeric<float>::is_integer);
    CHECK_FALSE(TypeIsNumeric<double>::is_integer);
    CHECK_FALSE(TypeIsNumeric<long double>::is_integer);
    CHECK_TRUE(TypeIsNumeric<signed char>::is_integer);
    CHECK_TRUE(TypeIsNumeric<unsigned char>::is_integer);
    CHECK_TRUE(TypeIsNumeric<signed short>::is_integer);
    CHECK_TRUE(TypeIsNumeric<unsigned short>::is_integer);
    CHECK_TRUE(TypeIsNumeric<signed int>::is_integer);
    CHECK_TRUE(TypeIsNumeric<unsigned int>::is_integer);
    CHECK_TRUE(TypeIsNumeric<signed long int>::is_integer);
    CHECK_TRUE(TypeIsNumeric<unsigned long int>::is_integer);
    CHECK_TRUE(TypeIsNumeric<signed long long int>::is_integer);
    CHECK_TRUE(TypeIsNumeric<unsigned long long int>::is_integer);
}

TEST_MEMBER_FUNCTION(TypeIsNumeric, is_decimal, NA)
{
    CHECK_FALSE(TypeIsNumeric<Huge>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<bool>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<char>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<wchar_t>::is_decimal);
    CHECK_TRUE(TypeIsNumeric<float>::is_decimal);
    CHECK_TRUE(TypeIsNumeric<double>::is_decimal);
    CHECK_TRUE(TypeIsNumeric<long double>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<signed char>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<unsigned char>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<signed short>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<unsigned short>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<signed int>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<unsigned int>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<signed long int>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<unsigned long int>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<signed long long int>::is_decimal);
    CHECK_FALSE(TypeIsNumeric<unsigned long long int>::is_decimal);
}

TEST_MEMBER_FUNCTION(TypeIsNumeric, is_numeric, NA)
{
    CHECK_FALSE(TypeIsNumeric<Huge>::is_numeric);
    CHECK_FALSE(TypeIsNumeric<bool>::is_numeric);
    CHECK_FALSE(TypeIsNumeric<char>::is_numeric);
    CHECK_FALSE(TypeIsNumeric<wchar_t>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<float>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<double>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<long double>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<signed char>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<unsigned char>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<signed short>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<unsigned short>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<signed int>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<unsigned int>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<signed long int>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<unsigned long int>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<signed long long int>::is_numeric);
    CHECK_TRUE(TypeIsNumeric<unsigned long long int>::is_numeric);
}
