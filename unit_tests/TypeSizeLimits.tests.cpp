/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeLimits.hpp"
#include <cstdint>
#include <cstddef>

using ocl::TypeSizeLimits;

TEST_MEMBER_FUNCTION(TypeSizeLimits, MIN, NA)
{
    std::int8_t i8 = TypeSizeLimits<sizeof(int8_t) * 8, true>::MIN;
    CHECK_EQUAL(i8, static_cast<int8_t>(-128));
    std::uint8_t ui8 = TypeSizeLimits<sizeof(uint8_t) * 8, false>::MIN;
    CHECK_EQUAL(ui8, static_cast<uint8_t>(0));
    std::int16_t i16 = TypeSizeLimits<sizeof(int16_t) * 8, true>::MIN;
    CHECK_EQUAL(i16, static_cast<int16_t>(-32768));
    std::uint16_t ui16 = TypeSizeLimits<sizeof(uint16_t) * 8, false>::MIN;
    CHECK_EQUAL(ui16, static_cast<uint16_t>(0));
    std::int32_t i32 = TypeSizeLimits<sizeof(int32_t) * 8, true>::MIN;
    CHECK_EQUAL(i32, static_cast<int32_t>(-2147483647l - 1l));
    std::uint32_t ui32 = TypeSizeLimits<sizeof(uint32_t) * 8, false>::MIN;
    CHECK_EQUAL(ui32, static_cast<uint32_t>(0));
    std::int64_t i64 = TypeSizeLimits<sizeof(int64_t) * 8, true>::MIN;
    CHECK_EQUAL(i64, static_cast<int64_t>(-9223372036854775807ll - 1ll));
    std::uint64_t ui64 = TypeSizeLimits<sizeof(uint64_t) * 8, false>::MIN;
    CHECK_EQUAL(ui64, static_cast<uint64_t>(0));
}

TEST_MEMBER_FUNCTION(TypeSizeLimits, MAX, NA)
{
    std::int8_t i8 = TypeSizeLimits<sizeof(int8_t) * 8, true>::MAX;
    CHECK_EQUAL(i8, static_cast<int8_t>(127));
    std::uint8_t ui8 = TypeSizeLimits<sizeof(uint8_t) * 8, false>::MAX;
    CHECK_EQUAL(ui8, static_cast<uint8_t>(255));
    std::int16_t i16 = TypeSizeLimits<sizeof(int16_t) * 8, true>::MAX;
    CHECK_EQUAL(i16, static_cast<int16_t>(32767));
    std::uint16_t ui16 = TypeSizeLimits<sizeof(uint16_t) * 8, false>::MAX;
    CHECK_EQUAL(ui16, static_cast<uint16_t>(0xffff));
    std::int32_t i32 = TypeSizeLimits<sizeof(int32_t) * 8, true>::MAX;
    CHECK_EQUAL(i32, static_cast<int32_t>(2147483647l));
    std::uint32_t ui32 = TypeSizeLimits<sizeof(uint32_t) * 8, false>::MAX;
    CHECK_EQUAL(ui32, static_cast<uint32_t>(0xffffffff));
    std::int64_t i64 = TypeSizeLimits<sizeof(int64_t) * 8, true>::MAX;
    CHECK_EQUAL(i64, static_cast<int64_t>(9223372036854775807ll));
    std::uint64_t ui64 = TypeSizeLimits<sizeof(uint64_t) * 8, false>::MAX;
    CHECK_EQUAL(ui64, static_cast<uint64_t>(0xffffffffffffffffull));
}

TEST_MEMBER_FUNCTION(TypeSizeLimits, NUM_BITS, NA)
{
    std::size_t i8 = TypeSizeLimits<sizeof(int8_t) * 8, true>::NUM_BITS;
    CHECK_EQUAL(i8, 8);
    std::size_t ui8 = TypeSizeLimits<sizeof(uint8_t) * 8, false>::NUM_BITS;
    CHECK_EQUAL(ui8, 8);
    std::size_t i16 = TypeSizeLimits<sizeof(int16_t) * 8, true>::NUM_BITS;
    CHECK_EQUAL(i16, 16);
    std::size_t ui16 = TypeSizeLimits<sizeof(uint16_t) * 8, false>::NUM_BITS;
    CHECK_EQUAL(ui16, 16);
    std::size_t i32 = TypeSizeLimits<sizeof(int32_t) * 8, true>::NUM_BITS;
    CHECK_EQUAL(i32, 32);
    std::size_t ui32 = TypeSizeLimits<sizeof(uint32_t) * 8, false>::NUM_BITS;
    CHECK_EQUAL(ui32, 32);
    std::size_t i64 = TypeSizeLimits<sizeof(int64_t) * 8, true>::NUM_BITS;
    CHECK_EQUAL(i64, 64);
    std::size_t ui64 = TypeSizeLimits<sizeof(uint64_t) * 8, false>::NUM_BITS;
    CHECK_EQUAL(ui64, 64);
}

TEST_MEMBER_FUNCTION(TypeSizeLimits, MAX_CHARS, NA)
{
    std::size_t num_chars = TypeSizeLimits<sizeof(int8_t) * 8, true>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 4U);
    num_chars = TypeSizeLimits<sizeof(uint8_t) * 8, false>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 3U);
    num_chars = TypeSizeLimits<sizeof(int16_t) * 8, true>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 6U);
    num_chars = TypeSizeLimits<sizeof(uint16_t) * 8, false>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 5U);
    num_chars = TypeSizeLimits<sizeof(int32_t) * 8, true>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 11U);
    num_chars = TypeSizeLimits<sizeof(uint32_t) * 8, false>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 10U);
    num_chars = TypeSizeLimits<sizeof(int64_t) * 8, true>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 20U);
    num_chars = TypeSizeLimits<sizeof(uint64_t) * 8, false>::MAX_CHARS;
    CHECK_EQUAL(num_chars, 20U);
}

TEST_MEMBER_FUNCTION(TypeSizeLimits, TOP_BIT, NA)
{
    std::int8_t top_bit_i8 = TypeSizeLimits<sizeof(int8_t) * 8, true>::TOP_BIT;
    CHECK_EQUAL(top_bit_i8, static_cast<int8_t>(0x40));
    std::uint8_t top_bit_ui8 = TypeSizeLimits<sizeof(uint8_t) * 8, false>::TOP_BIT;
    CHECK_EQUAL(top_bit_ui8, static_cast<uint8_t>(0x80));
    std::int16_t top_bit_i16 = TypeSizeLimits<sizeof(int16_t) * 8, true>::TOP_BIT;
    CHECK_EQUAL(top_bit_i16, static_cast<int16_t>(0x4000));
    std::uint16_t top_bit_ui16 = TypeSizeLimits<sizeof(uint16_t) * 8, false>::TOP_BIT;
    CHECK_EQUAL(top_bit_ui16, static_cast<uint16_t>(0x8000));
    std::int32_t top_bit_i32 = TypeSizeLimits<sizeof(int32_t) * 8, true>::TOP_BIT;
    CHECK_EQUAL(top_bit_i32, static_cast<int32_t>(0x40000000ul));
    std::uint32_t top_bit_ui32 = TypeSizeLimits<sizeof(uint32_t) * 8, false>::TOP_BIT;
    CHECK_EQUAL(top_bit_ui32, static_cast<uint32_t>(0x80000000ul));
    std::int64_t top_bit_i64 = TypeSizeLimits<sizeof(int64_t) * 8, true>::TOP_BIT;
    CHECK_EQUAL(top_bit_i64, static_cast<int64_t>(0x4000000000000000ull));
    std::uint64_t top_bit_ui64 = TypeSizeLimits<sizeof(uint64_t) * 8, false>::TOP_BIT;
    CHECK_EQUAL(top_bit_ui64, static_cast<uint64_t>(0x8000000000000000ull));
}

TEST_MEMBER_FUNCTION(TypeSizeLimits, GetByteCount, type)
{
    typedef TypeSizeLimits<8, true> size_limits8_signed_type;
    typedef TypeSizeLimits<8, false> size_limits8_unsigned_type;
    typedef TypeSizeLimits<16, true> size_limits16_signed_type;
    typedef TypeSizeLimits<16, false> size_limits16_unsigned_type;
    typedef TypeSizeLimits<32, true> size_limits32_signed_type;
    typedef TypeSizeLimits<32, false> size_limits32_unsigned_type;
    typedef TypeSizeLimits<64, true> size_limits64_signed_type;
    typedef TypeSizeLimits<64, false> size_limits64_unsigned_type;

    static const std::int8_t int8_min_value = TypeSizeLimits<8, true>::MIN;
    static const std::int8_t int8_max_value = TypeSizeLimits<8, true>::MAX;
    static const std::int16_t int16_min_value = TypeSizeLimits<16, true>::MIN;
    static const std::int16_t int16_max_value = TypeSizeLimits<16, true>::MAX;
    static const std::int32_t int32_min_value = TypeSizeLimits<32, true>::MIN;
    static const std::int32_t int32_max_value = TypeSizeLimits<32, true>::MAX;
    static const std::int64_t int64_min_value = TypeSizeLimits<64, true>::MIN;
    static const std::int64_t int64_max_value = TypeSizeLimits<64, true>::MAX;

    static const std::uint8_t uint8_min_value = TypeSizeLimits<8, false>::MIN;
    static const std::uint8_t uint8_max_value = TypeSizeLimits<8, false>::MAX;
    static const std::uint16_t uint16_min_value = TypeSizeLimits<16, false>::MIN;
    static const std::uint16_t uint16_max_value = TypeSizeLimits<16, false>::MAX;
    static const std::uint32_t uint32_min_value = TypeSizeLimits<32, false>::MIN;
    static const std::uint32_t uint32_max_value = TypeSizeLimits<32, false>::MAX;
    static const std::uint64_t uint64_min_value = TypeSizeLimits<64, false>::MIN;
    static const std::uint64_t uint64_max_value = TypeSizeLimits<64, false>::MAX;

    CHECK_EQUAL(size_limits8_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits8_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int16_min_value), 2);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits16_signed_type::GetByteCount(int16_max_value), 2);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int16_min_value), 2);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int32_min_value), 4);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int16_max_value), 2);
    CHECK_EQUAL(size_limits32_signed_type::GetByteCount(int32_max_value), 4);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int8_min_value), 1);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int16_min_value), 2);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int32_min_value), 4);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int64_min_value), 8);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int8_max_value), 1);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int16_max_value), 2);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int32_max_value), 4);
    CHECK_EQUAL(size_limits64_signed_type::GetByteCount(int64_max_value), 8);

    CHECK_EQUAL(size_limits8_unsigned_type::GetByteCount(uint8_min_value), 1);
    CHECK_EQUAL(size_limits8_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits16_unsigned_type::GetByteCount(uint16_min_value), 1);
    CHECK_EQUAL(size_limits16_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits16_unsigned_type::GetByteCount(uint16_max_value), 2);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint32_min_value), 1);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint16_max_value), 2);
    CHECK_EQUAL(size_limits32_unsigned_type::GetByteCount(uint32_max_value), 4);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint64_min_value), 1);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint8_max_value), 1);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint16_max_value), 2);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint32_max_value), 4);
    CHECK_EQUAL(size_limits64_unsigned_type::GetByteCount(uint64_max_value), 8);
}
