/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPESIGNED_HPP
#define OCL_GUARD_TYPEINFO_TYPESIGNED_HPP

namespace ocl
{

template<typename Type>
struct TypeSigned
{
    typedef Type type;

    static bool const is_sign_known = false;
    static bool const is_sign_fixed = false; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<bool>
{
    typedef bool type;

    static bool const is_sign_known = false;
    static bool const is_sign_fixed = false; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<char>
{
    typedef char type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = false; // Does the sign remain consistent for all compilers?
    static bool const is_signed = static_cast<type>(-1) < static_cast<type>(0);
};

template<>
struct TypeSigned<wchar_t>
{
    typedef wchar_t type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = false; // Does the sign remain consistent for all compilers?
    static bool const is_signed = static_cast<type>(-1) < static_cast<type>(0);
};

template<>
struct TypeSigned<signed char>
{
    typedef signed char type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<unsigned char>
{
    typedef unsigned char type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<signed short>
{
    typedef signed short type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<unsigned short>
{
    typedef unsigned short type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<signed int>
{
    typedef signed int type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<unsigned int>
{
    typedef unsigned int type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<signed long int>
{
    typedef signed long int type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<unsigned long int>
{
    typedef unsigned long int type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<signed long long int>
{
    typedef signed long long int type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<unsigned long long int>
{
    typedef unsigned long long int type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = false;
};

template<>
struct TypeSigned<float>
{
    typedef float type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<double>
{
    typedef double type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

template<>
struct TypeSigned<long double>
{
    typedef double type;

    static bool const is_sign_known = true;
    static bool const is_sign_fixed = true; // Does the sign remain consistent for all compilers?
    static bool const is_signed = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPESIGNED_HPP
