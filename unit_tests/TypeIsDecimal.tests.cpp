/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIsDecimal.hpp"

using ocl::TypeIsDecimal;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeIsDecimal, is_decimal, NA)
{
    CHECK_FALSE(TypeIsDecimal<Huge>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<bool>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<char>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<wchar_t>::is_decimal);
    CHECK_TRUE(TypeIsDecimal<float>::is_decimal);
    CHECK_TRUE(TypeIsDecimal<double>::is_decimal);
    CHECK_TRUE(TypeIsDecimal<long double>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<signed char>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<unsigned char>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<signed short>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<unsigned short>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<signed int>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<unsigned int>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<signed long int>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<unsigned long int>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<signed long long int>::is_decimal);
    CHECK_FALSE(TypeIsDecimal<unsigned long long int>::is_decimal);
}
