/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_INTERNAL_INTERNALTYPEIN_HPP
#define OCL_GUARD_TYPEINFO_INTERNAL_INTERNALTYPEIN_HPP

namespace ocl
{

/// Identify a type as being smaller or equal to the size of a pointer,
/// which can then be passed in by value.
template<typename Type, bool const is_const_ref>
struct InternalTypeIn
{
    typedef Type type;
    typedef Type in_type;
};

/// Types larger than a pointer can be passed in by reference.
template<typename Type>
struct InternalTypeIn<Type, true>
{
    typedef Type type;
    typedef Type const& in_type;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_INTERNAL_INTERNALTYPEIN_HPP
