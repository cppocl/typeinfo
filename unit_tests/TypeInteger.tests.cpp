/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeInteger.hpp"

using ocl::TypeInteger;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeInteger, is_integer, NA)
{
    CHECK_FALSE(TypeInteger<Huge>::is_integer);
    CHECK_FALSE(TypeInteger<bool>::is_integer);
    CHECK_FALSE(TypeInteger<char>::is_integer);
    CHECK_FALSE(TypeInteger<wchar_t>::is_integer);
    CHECK_FALSE(TypeInteger<float>::is_integer);
    CHECK_FALSE(TypeInteger<double>::is_integer);
    CHECK_FALSE(TypeInteger<long double>::is_integer);
    CHECK_TRUE(TypeInteger<signed char>::is_integer);
    CHECK_TRUE(TypeInteger<unsigned char>::is_integer);
    CHECK_TRUE(TypeInteger<signed short>::is_integer);
    CHECK_TRUE(TypeInteger<unsigned short>::is_integer);
    CHECK_TRUE(TypeInteger<signed int>::is_integer);
    CHECK_TRUE(TypeInteger<unsigned int>::is_integer);
    CHECK_TRUE(TypeInteger<signed long int>::is_integer);
    CHECK_TRUE(TypeInteger<unsigned long int>::is_integer);
    CHECK_TRUE(TypeInteger<signed long long int>::is_integer);
    CHECK_TRUE(TypeInteger<unsigned long long int>::is_integer);
}
