/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeMinMax.hpp"

using ocl::TypeMinMax;

TEST_MEMBER_FUNCTION(TypeMinMax, Min, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");
    typedef int int_type;
    CHECK_EQUAL(TypeMinMax<int_type>::Min(0, 0), 0);
    CHECK_EQUAL(TypeMinMax<int_type>::Min(1, 0), 0);
    CHECK_EQUAL(TypeMinMax<int_type>::Min(0, 1), 0);
}

TEST_MEMBER_FUNCTION(TypeMinMax, Max, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");
    typedef int int_type;
    CHECK_EQUAL(TypeMinMax<int_type>::Max(0, 0), 0);
    CHECK_EQUAL(TypeMinMax<int_type>::Max(1, 0), 1);
    CHECK_EQUAL(TypeMinMax<int_type>::Max(0, 1), 1);
}
