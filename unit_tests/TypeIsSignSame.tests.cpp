/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIsSignSame.hpp"

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeIsSignSame, is_sign_same, NA)
{
    bool is_same;

    // Boolean is not considered to have a sign.
    is_same = ocl::TypeIsSignSame<bool, bool>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed int*, signed int*>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int*, unsigned int*>::is_sign_same;
    CHECK_FALSE(is_same);

    // Test types that could be signed or unsigned.
    is_same = ocl::TypeIsSignSame<char, char>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<wchar_t, wchar_t>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<char&, char&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<wchar_t&, wchar_t&>::is_sign_same;
    CHECK_TRUE(is_same);

    // Test types that will have the same type and be signed.
    is_same = ocl::TypeIsSignSame<signed char, signed char>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed short int, signed short int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed int, signed int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed long int, signed long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed long long int, signed long long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned char, unsigned char>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned short int, unsigned short int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int, unsigned int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long int, unsigned long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long long int, unsigned long long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<float, float>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<double, double>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<long double, long double>::is_sign_same;
    CHECK_TRUE(is_same);

    // Test reference types that will have the same type and be signed.
    is_same = ocl::TypeIsSignSame<signed char&, signed char&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed short int&, signed short int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed int&, signed int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed long int&, signed long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<signed long long int&, signed long long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned char&, unsigned char&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned short int&, unsigned short int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int&, unsigned int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long int&, unsigned long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long long int&, unsigned long long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<float&, float&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<double&, double&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<long double&, long double&>::is_sign_same;
    CHECK_TRUE(is_same);

    // Test types that will have the same type and be unsigned.
    is_same = ocl::TypeIsSignSame<unsigned char, unsigned char>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned short int, unsigned short int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int, unsigned int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long int, unsigned long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long long int, unsigned long long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned char, unsigned char>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned short int, unsigned short int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int, unsigned int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long int, unsigned long int>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long long int, unsigned long long int>::is_sign_same;
    CHECK_TRUE(is_same);

    // Test reference types that will have the same type and be unsigned.
    is_same = ocl::TypeIsSignSame<unsigned char&, unsigned char&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned short int&, unsigned short int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int&, unsigned int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long int&, unsigned long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long long int&, unsigned long long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned char&, unsigned char&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned short int&, unsigned short int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned int&, unsigned int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long int&, unsigned long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    is_same = ocl::TypeIsSignSame<unsigned long long int&, unsigned long long int&>::is_sign_same;
    CHECK_TRUE(is_same);

    // Test types that will have a type that differs by sign.
    is_same = ocl::TypeIsSignSame<signed char, unsigned char>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed short int, unsigned short int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed int, unsigned int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long int, unsigned long int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long long int, unsigned long long int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed char, unsigned char>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed short int, unsigned short int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed int, unsigned int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long int, unsigned long int>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long long int, unsigned long long int>::is_sign_same;
    CHECK_FALSE(is_same);

    // Test reference types that will have a type that differs by sign.
    is_same = ocl::TypeIsSignSame<signed char&, unsigned char&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed short int&, unsigned short int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed int&, unsigned int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long int&, unsigned long int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long long int&, unsigned long long int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed char&, unsigned char&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed short int&, unsigned short int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed int&, unsigned int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long int&, unsigned long int&>::is_sign_same;
    CHECK_FALSE(is_same);

    is_same = ocl::TypeIsSignSame<signed long long int&, unsigned long long int&>::is_sign_same;
    CHECK_FALSE(is_same);
}
