/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypePointer.hpp"

using ocl::TypePointer;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypePointer, is_pointer, NA)
{
    CHECK_TRUE(TypePointer<Huge*>::is_pointer);
    CHECK_TRUE(TypePointer<void*>::is_pointer);
    CHECK_TRUE(TypePointer<bool*>::is_pointer);
    CHECK_TRUE(TypePointer<char*>::is_pointer);
    CHECK_TRUE(TypePointer<wchar_t*>::is_pointer);
    CHECK_TRUE(TypePointer<signed char*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned char*>::is_pointer);
    CHECK_TRUE(TypePointer<signed short*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned short*>::is_pointer);
    CHECK_TRUE(TypePointer<signed int*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned int*>::is_pointer);
    CHECK_TRUE(TypePointer<signed long int*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned long int*>::is_pointer);
    CHECK_TRUE(TypePointer<signed long long int*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned long long int*>::is_pointer);
    CHECK_TRUE(TypePointer<float*>::is_pointer);
    CHECK_TRUE(TypePointer<double*>::is_pointer);
    CHECK_TRUE(TypePointer<long double*>::is_pointer);

    CHECK_TRUE(TypePointer<Huge**>::is_pointer);
    CHECK_TRUE(TypePointer<void**>::is_pointer);
    CHECK_TRUE(TypePointer<bool**>::is_pointer);
    CHECK_TRUE(TypePointer<char**>::is_pointer);
    CHECK_TRUE(TypePointer<wchar_t**>::is_pointer);
    CHECK_TRUE(TypePointer<signed char**>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned char**>::is_pointer);
    CHECK_TRUE(TypePointer<signed short**>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned short**>::is_pointer);
    CHECK_TRUE(TypePointer<signed int**>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned int**>::is_pointer);
    CHECK_TRUE(TypePointer<signed long int**>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned long int**>::is_pointer);
    CHECK_TRUE(TypePointer<signed long long int**>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned long long int**>::is_pointer);
    CHECK_TRUE(TypePointer<float**>::is_pointer);
    CHECK_TRUE(TypePointer<double**>::is_pointer);
    CHECK_TRUE(TypePointer<long double**>::is_pointer);

    CHECK_TRUE(TypePointer<Huge const*>::is_pointer);
    CHECK_TRUE(TypePointer<void const*>::is_pointer);
    CHECK_TRUE(TypePointer<bool const*>::is_pointer);
    CHECK_TRUE(TypePointer<char const*>::is_pointer);
    CHECK_TRUE(TypePointer<wchar_t const*>::is_pointer);
    CHECK_TRUE(TypePointer<signed char const*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned char const*>::is_pointer);
    CHECK_TRUE(TypePointer<signed short const*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned short const*>::is_pointer);
    CHECK_TRUE(TypePointer<signed int const*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned int const*>::is_pointer);
    CHECK_TRUE(TypePointer<signed long int const*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned long int const*>::is_pointer);
    CHECK_TRUE(TypePointer<signed long long int const*>::is_pointer);
    CHECK_TRUE(TypePointer<unsigned long long int const*>::is_pointer);
    CHECK_TRUE(TypePointer<float const*>::is_pointer);
    CHECK_TRUE(TypePointer<double const*>::is_pointer);
    CHECK_TRUE(TypePointer<long double const*>::is_pointer);

    CHECK_FALSE(TypePointer<Huge>::is_pointer);
    CHECK_FALSE(TypePointer<bool>::is_pointer);
    CHECK_FALSE(TypePointer<char>::is_pointer);
    CHECK_FALSE(TypePointer<wchar_t>::is_pointer);
    CHECK_FALSE(TypePointer<signed char>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned char>::is_pointer);
    CHECK_FALSE(TypePointer<signed short>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned short>::is_pointer);
    CHECK_FALSE(TypePointer<signed int>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned int>::is_pointer);
    CHECK_FALSE(TypePointer<signed long int>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long int>::is_pointer);
    CHECK_FALSE(TypePointer<signed long long int>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long long int>::is_pointer);
    CHECK_FALSE(TypePointer<float>::is_pointer);
    CHECK_FALSE(TypePointer<double>::is_pointer);
    CHECK_FALSE(TypePointer<long double>::is_pointer);

    CHECK_FALSE(TypePointer<Huge const>::is_pointer);
    CHECK_FALSE(TypePointer<bool const>::is_pointer);
    CHECK_FALSE(TypePointer<char const>::is_pointer);
    CHECK_FALSE(TypePointer<wchar_t const>::is_pointer);
    CHECK_FALSE(TypePointer<signed char const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned char const>::is_pointer);
    CHECK_FALSE(TypePointer<signed short const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned short const>::is_pointer);
    CHECK_FALSE(TypePointer<signed int const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned int const>::is_pointer);
    CHECK_FALSE(TypePointer<signed long int const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long int const>::is_pointer);
    CHECK_FALSE(TypePointer<signed long long int const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long long int const>::is_pointer);
    CHECK_FALSE(TypePointer<float const>::is_pointer);
    CHECK_FALSE(TypePointer<double const>::is_pointer);
    CHECK_FALSE(TypePointer<long double const>::is_pointer);

    CHECK_FALSE(TypePointer<Huge volatile>::is_pointer);
    CHECK_FALSE(TypePointer<bool volatile>::is_pointer);
    CHECK_FALSE(TypePointer<char volatile>::is_pointer);
    CHECK_FALSE(TypePointer<wchar_t volatile>::is_pointer);
    CHECK_FALSE(TypePointer<signed char volatile>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned char volatile>::is_pointer);
    CHECK_FALSE(TypePointer<signed short volatile>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned short volatile>::is_pointer);
    CHECK_FALSE(TypePointer<signed int volatile>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned int volatile>::is_pointer);
    CHECK_FALSE(TypePointer<signed long int volatile>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long int volatile>::is_pointer);
    CHECK_FALSE(TypePointer<signed long long int volatile>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long long int volatile>::is_pointer);
    CHECK_FALSE(TypePointer<float volatile>::is_pointer);
    CHECK_FALSE(TypePointer<double volatile>::is_pointer);
    CHECK_FALSE(TypePointer<long double volatile>::is_pointer);

    CHECK_FALSE(TypePointer<Huge&>::is_pointer);
    CHECK_FALSE(TypePointer<bool&>::is_pointer);
    CHECK_FALSE(TypePointer<char&>::is_pointer);
    CHECK_FALSE(TypePointer<wchar_t&>::is_pointer);
    CHECK_FALSE(TypePointer<signed char&>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned char&>::is_pointer);
    CHECK_FALSE(TypePointer<signed short&>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned short&>::is_pointer);
    CHECK_FALSE(TypePointer<signed int&>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned int&>::is_pointer);
    CHECK_FALSE(TypePointer<signed long int&>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long int&>::is_pointer);
    CHECK_FALSE(TypePointer<signed long long int&>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long long int&>::is_pointer);
    CHECK_FALSE(TypePointer<float&>::is_pointer);
    CHECK_FALSE(TypePointer<double&>::is_pointer);
    CHECK_FALSE(TypePointer<long double&>::is_pointer);

    CHECK_FALSE(TypePointer<Huge const* const>::is_pointer);
    CHECK_FALSE(TypePointer<void const* const>::is_pointer);
    CHECK_FALSE(TypePointer<bool const* const>::is_pointer);
    CHECK_FALSE(TypePointer<char const* const>::is_pointer);
    CHECK_FALSE(TypePointer<wchar_t const* const>::is_pointer);
    CHECK_FALSE(TypePointer<signed char const* const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned char const* const>::is_pointer);
    CHECK_FALSE(TypePointer<signed short const* const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned short const* const>::is_pointer);
    CHECK_FALSE(TypePointer<signed int const* const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned int const* const>::is_pointer);
    CHECK_FALSE(TypePointer<signed long int const* const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long int const* const>::is_pointer);
    CHECK_FALSE(TypePointer<signed long long int const* const>::is_pointer);
    CHECK_FALSE(TypePointer<unsigned long long int const* const>::is_pointer);
    CHECK_FALSE(TypePointer<float const* const>::is_pointer);
    CHECK_FALSE(TypePointer<double const* const>::is_pointer);
    CHECK_FALSE(TypePointer<long double const* const>::is_pointer);
}
