/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeName.hpp"
#include "../TypeSizePrevious.hpp"
#include <string.h>

using ocl::TypeSizePrevious;
using ocl::TypeSizeName;

TEST_MEMBER_FUNCTION(TypeSizePrevious, previous_type, NA)
{
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizePrevious<int16_t>::previous_type>::GetName(), "int8_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizePrevious<uint16_t>::previous_type>::GetName(), "uint8_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizePrevious<int32_t>::previous_type>::GetName(), "int16_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizePrevious<uint32_t>::previous_type>::GetName(), "uint16_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizePrevious<int64_t>::previous_type>::GetName(), "int32_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizePrevious<uint64_t>::previous_type>::GetName(), "uint32_t"), 0);
}
