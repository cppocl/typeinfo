/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSigned.hpp"

using ocl::TypeSigned;

namespace
{

struct Huge
{
    double v1[1000]; // This won't be smaller than a void*
};

}

TEST_MEMBER_FUNCTION(TypeSigned, is_sign_known, NA)
{
    CHECK_FALSE(TypeSigned<bool>::is_sign_known);
    CHECK_TRUE(TypeSigned<char>::is_sign_known);
    CHECK_TRUE(TypeSigned<wchar_t>::is_sign_known);
    CHECK_TRUE(TypeSigned<signed char>::is_sign_known);
    CHECK_TRUE(TypeSigned<unsigned char>::is_sign_known);
    CHECK_TRUE(TypeSigned<signed short>::is_sign_known);
    CHECK_TRUE(TypeSigned<unsigned short>::is_sign_known);
    CHECK_TRUE(TypeSigned<signed int>::is_sign_known);
    CHECK_TRUE(TypeSigned<unsigned int>::is_sign_known);
    CHECK_TRUE(TypeSigned<signed long int>::is_sign_known);
    CHECK_TRUE(TypeSigned<unsigned long int>::is_sign_known);
    CHECK_TRUE(TypeSigned<signed long long int>::is_sign_known);
    CHECK_TRUE(TypeSigned<unsigned long long int>::is_sign_known);
    CHECK_TRUE(TypeSigned<float>::is_sign_known);
    CHECK_TRUE(TypeSigned<double>::is_sign_known);
    CHECK_TRUE(TypeSigned<long double>::is_sign_known);
    CHECK_FALSE(TypeSigned<Huge>::is_sign_known);
}

TEST_MEMBER_FUNCTION(TypeSigned, is_sign_fixed, NA)
{
    CHECK_FALSE(TypeSigned<bool>::is_sign_fixed);
    CHECK_FALSE(TypeSigned<char>::is_sign_fixed);
    CHECK_FALSE(TypeSigned<wchar_t>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<signed char>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<unsigned char>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<signed short>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<unsigned short>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<signed int>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<unsigned int>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<signed long int>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<unsigned long int>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<signed long long int>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<unsigned long long int>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<float>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<double>::is_sign_fixed);
    CHECK_TRUE(TypeSigned<long double>::is_sign_fixed);
    CHECK_FALSE(TypeSigned<Huge>::is_sign_fixed);
}

TEST_MEMBER_FUNCTION(TypeSigned, is_signed, NA)
{
    CHECK_FALSE(TypeSigned<bool>::is_signed);
    CHECK_TRUE(TypeSigned<signed char>::is_signed);
    CHECK_FALSE(TypeSigned<unsigned char>::is_signed);
    CHECK_TRUE(TypeSigned<signed short>::is_signed);
    CHECK_FALSE(TypeSigned<unsigned short>::is_signed);
    CHECK_TRUE(TypeSigned<signed int>::is_signed);
    CHECK_FALSE(TypeSigned<unsigned int>::is_signed);
    CHECK_TRUE(TypeSigned<signed long int>::is_signed);
    CHECK_FALSE(TypeSigned<unsigned long int>::is_signed);
    CHECK_TRUE(TypeSigned<signed long long int>::is_signed);
    CHECK_FALSE(TypeSigned<unsigned long long int>::is_signed);
    CHECK_TRUE(TypeSigned<float>::is_signed);
    CHECK_TRUE(TypeSigned<double>::is_signed);
    CHECK_TRUE(TypeSigned<long double>::is_signed);
    CHECK_FALSE(TypeSigned<Huge>::is_signed);
}
