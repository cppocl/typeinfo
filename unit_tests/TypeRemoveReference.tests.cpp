/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeRemoveReference.hpp"

using ocl::TypeRemoveReference;

TEST_MEMBER_FUNCTION(TypeRemoveReference, is_reference, NA)
{
    CHECK_TRUE(TypeRemoveReference<int&>::is_reference);
    CHECK_TRUE(TypeRemoveReference<int const&>::is_reference);
    CHECK_FALSE(TypeRemoveReference<int>::is_reference);

    typedef typename TypeRemoveReference<int&>::type no_ref_type;
    CHECK_FALSE(TypeRemoveReference<no_ref_type>::is_reference);
}
