/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPENEXT_HPP
#define OCL_GUARD_TYPEINFO_TYPENEXT_HPP

namespace ocl
{

template<typename Type>
struct TypeNext;

template<>
struct TypeNext<signed char>
{
    typedef signed char type;
    typedef signed short next_type;
};

template<>
struct TypeNext<unsigned char>
{
    typedef unsigned char type;
    typedef unsigned short next_type;
};

template<>
struct TypeNext<signed short>
{
    typedef signed short type;
    typedef signed int next_type;
};

template<>
struct TypeNext<unsigned short>
{
    typedef unsigned short type;
    typedef unsigned int next_type;
};

template<>
struct TypeNext<signed int>
{
    typedef signed int type;
    typedef signed long int next_type;
};

template<>
struct TypeNext<unsigned int>
{
    typedef unsigned int type;
    typedef unsigned long int next_type;
};

template<>
struct TypeNext<signed long int>
{
    typedef signed long int type;
    typedef signed long long int next_type;
};

template<>
struct TypeNext<unsigned long int>
{
    typedef unsigned long int type;
    typedef unsigned long long int next_type;
};

template<>
struct TypeNext<float>
{
    typedef float type;
    typedef double next_type;
};

template<>
struct TypeNext<double>
{
    typedef double type;
    typedef long double next_type;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPENEXT_HPP
