/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIsInteger.hpp"

using ocl::TypeIsInteger;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeIsInteger, is_integer, NA)
{
    CHECK_FALSE(TypeIsInteger<Huge>::is_integer);
    CHECK_FALSE(TypeIsInteger<bool>::is_integer);
    CHECK_FALSE(TypeIsInteger<char>::is_integer);
    CHECK_FALSE(TypeIsInteger<wchar_t>::is_integer);
    CHECK_FALSE(TypeIsInteger<float>::is_integer);
    CHECK_FALSE(TypeIsInteger<double>::is_integer);
    CHECK_FALSE(TypeIsInteger<long double>::is_integer);
    CHECK_TRUE(TypeIsInteger<signed char>::is_integer);
    CHECK_TRUE(TypeIsInteger<unsigned char>::is_integer);
    CHECK_TRUE(TypeIsInteger<signed short>::is_integer);
    CHECK_TRUE(TypeIsInteger<unsigned short>::is_integer);
    CHECK_TRUE(TypeIsInteger<signed int>::is_integer);
    CHECK_TRUE(TypeIsInteger<unsigned int>::is_integer);
    CHECK_TRUE(TypeIsInteger<signed long int>::is_integer);
    CHECK_TRUE(TypeIsInteger<unsigned long int>::is_integer);
    CHECK_TRUE(TypeIsInteger<signed long long int>::is_integer);
    CHECK_TRUE(TypeIsInteger<unsigned long long int>::is_integer);
}
