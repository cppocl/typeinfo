/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEINCREMENT_HPP
#define OCL_GUARD_TYPEINFO_TYPEINCREMENT_HPP

#include "TypeLimits.hpp"
#include <cfloat>

namespace ocl
{

/// Decrement a value without wrapping below the minimum value.
/// The decrement functions can either return void or bool.
/// If the return type is bool and the decrement would go past
/// the minimum value then the return would be false.
template<typename Type, typename RetType = void>
struct TypeIncrement
{
    typedef Type type;
    typedef RetType ret_type;

    // Increment value by 1 without wrapping past the maximum value.
    static ret_type Increment(type& value) noexcept
    {
        if (value < TypeLimits<type>::GetMax())
            ++value;
    }

    // Increment value without wrapping past the maximum value.
    static ret_type Increment(type& value, type by) noexcept
    {
        if (value < TypeLimits<type>::GetMax() - by)
            value += by;
        else
            value = TypeLimits<type>::GetMax();
    }
};

template<typename Type>
struct TypeIncrement<Type, bool>
{
    typedef Type type;
    typedef bool ret_type;

    // Increment value by 1 without wrapping past the maximum value.
    static ret_type Increment(type& value) noexcept
    {
        bool const success = value < TypeLimits<type>::GetMax();
        if (success)
            ++value;
        return success;
    }

    // Increment value without wrapping past the maximum value.
    static ret_type Increment(type& value, type by) noexcept
    {
        bool const success = value <= TypeLimits<type>::GetMax() - by;
        if (success)
            value += by;
        else
            value = TypeLimits<type>::GetMax();
        return success;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEINCREMENT_HPP
