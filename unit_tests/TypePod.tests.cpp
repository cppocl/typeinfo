/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypePod.hpp"

using ocl::TypePod;

TEST_MEMBER_FUNCTION(TypePod, is_pos, NA)
{
    CHECK_TRUE(TypePod<bool>::is_pod);
    CHECK_TRUE(TypePod<char>::is_pod);
    CHECK_TRUE(TypePod<wchar_t>::is_pod);
    CHECK_TRUE(TypePod<signed char>::is_pod);
    CHECK_TRUE(TypePod<unsigned char>::is_pod);
    CHECK_TRUE(TypePod<signed short>::is_pod);
    CHECK_TRUE(TypePod<unsigned short>::is_pod);
    CHECK_TRUE(TypePod<signed int>::is_pod);
    CHECK_TRUE(TypePod<unsigned int>::is_pod);
    CHECK_TRUE(TypePod<signed long int>::is_pod);
    CHECK_TRUE(TypePod<unsigned long int>::is_pod);
    CHECK_TRUE(TypePod<signed long long int>::is_pod);
    CHECK_TRUE(TypePod<unsigned long long int>::is_pod);
    CHECK_TRUE(TypePod<float>::is_pod);
    CHECK_TRUE(TypePod<double>::is_pod);
    CHECK_TRUE(TypePod<long double>::is_pod);
}
