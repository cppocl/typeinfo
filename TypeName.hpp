/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPENAME_HPP
#define OCL_GUARD_TYPEINFO_TYPENAME_HPP

namespace ocl
{

/// TypeName cane be used as debug aid for obtaining the type from a template class.
template<typename Type>
struct TypeName
{
    typedef Type type;

    static char const* GetName() throw() { return NULL; }
    static wchar_t const* GetWideName() throw() { return NULL; }
};

template<>
struct TypeName<bool>
{
    typedef bool type;

    static char const* GetName() throw() { return "bool"; }
    static wchar_t const* GetWideName() throw() { return L"bool"; }
};

template<>
struct TypeName<char>
{
    typedef char type;

    static char const* GetName() throw() { return "char"; }
    static wchar_t const* GetWideName() throw() { return L"char"; }
};

template<>
struct TypeName<wchar_t>
{
    typedef wchar_t type;

    static char const* GetName() throw() { return "wchar_t"; }
    static wchar_t const* GetWideName() throw() { return L"wchar_t"; }
};

template<>
struct TypeName<signed char>
{
    typedef signed char type;

    static char const* GetName() throw() { return "signed char"; }
    static wchar_t const* GetWideName() throw() { return L"signed char"; }
};

template<>
struct TypeName<unsigned char>
{
    typedef unsigned char type;

    static char const* GetName() throw() { return "unsigned char"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned char"; }
};

template<>
struct TypeName<signed short>
{
    typedef signed short type;

    static char const* GetName() throw() { return "signed short"; }
    static wchar_t const* GetWideName() throw() { return L"signed short"; }
};

template<>
struct TypeName<unsigned short>
{
    typedef unsigned short type;

    static char const* GetName() throw() { return "unsigned short"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned short"; }
};

template<>
struct TypeName<signed int>
{
    typedef signed int type;

    static char const* GetName() throw() { return "signed int"; }
    static wchar_t const* GetWideName() throw() { return L"signed int"; }
};

template<>
struct TypeName<unsigned int>
{
    typedef unsigned int type;

    static char const* GetName() throw() { return "unsigned int"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned int"; }
};

template<>
struct TypeName<signed long int>
{
    typedef signed long int type;

    static char const* GetName() throw() { return "signed long int"; }
    static wchar_t const* GetWideName() throw() { return L"signed long int"; }
};

template<>
struct TypeName<unsigned long int>
{
    typedef unsigned long int type;

    static char const* GetName() throw() { return "unsigned long int"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned long int"; }
};

template<>
struct TypeName<signed long long int>
{
    typedef signed long long int type;

    static char const* GetName() throw() { return "signed long long int"; }
    static wchar_t const* GetWideName() throw() { return L"signed long long int"; }
};

template<>
struct TypeName<unsigned long long int>
{
    typedef unsigned long long int type;

    static char const* GetName() throw() { return "unsigned long long int"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned long long int"; }
};

template<>
struct TypeName<float>
{
    typedef float type;

    static char const* GetName() throw() { return "float"; }
    static wchar_t const* GetWideName() throw() { return L"float"; }
};

template<>
struct TypeName<double>
{
    typedef double type;

    static char const* GetName() throw() { return "double"; }
    static wchar_t const* GetWideName() throw() { return L"double"; }
};

template<>
struct TypeName<long double>
{
    typedef long double type;

    static char const* GetName() throw() { return "long double"; }
    static wchar_t const* GetWideName() throw() { return L"long double"; }
};

template<>
struct TypeName<bool const*>
{
    typedef bool const* type;

    static char const* GetName() throw() { return "bool const*"; }
    static wchar_t const* GetWideName() throw() { return L"bool const*"; }
};

template<>
struct TypeName<char const*>
{
    typedef char const* type;

    static char const* GetName() throw() { return "char const*"; }
    static wchar_t const* GetWideName() throw() { return L"char const*"; }
};

template<>
struct TypeName<wchar_t const*>
{
    typedef wchar_t const* type;

    static char const* GetName() throw() { return "wchar_t const*"; }
    static wchar_t const* GetWideName() throw() { return L"wchar_t const*"; }
};

template<>
struct TypeName<signed char const*>
{
    typedef signed char const* type;

    static char const* GetName() throw() { return "signed char const*"; }
    static wchar_t const* GetWideName() throw() { return L"signed char const*"; }
};

template<>
struct TypeName<unsigned char const*>
{
    typedef unsigned char const* type;

    static char const* GetName() throw() { return "unsigned char const*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned char const*"; }
};

template<>
struct TypeName<signed short const*>
{
    typedef signed short const* type;

    static char const* GetName() throw() { return "signed short const*"; }
    static wchar_t const* GetWideName() throw() { return L"signed short const*"; }
};

template<>
struct TypeName<unsigned short const*>
{
    typedef unsigned short const* type;

    static char const* GetName() throw() { return "unsigned short const*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned short const*"; }
};

template<>
struct TypeName<signed int const*>
{
    typedef signed int const* type;

    static char const* GetName() throw() { return "signed int const*"; }
    static wchar_t const* GetWideName() throw() { return L"signed int const*"; }
};

template<>
struct TypeName<unsigned int const*>
{
    typedef unsigned int const* type;

    static char const* GetName() throw() { return "unsigned int const*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned int const*"; }
};

template<>
struct TypeName<signed long int const*>
{
    typedef signed long int const* type;

    static char const* GetName() throw() { return "signed long int const*"; }
    static wchar_t const* GetWideName() throw() { return L"signed long int const*"; }
};

template<>
struct TypeName<unsigned long int const*>
{
    typedef unsigned long int const* type;

    static char const* GetName() throw() { return "unsigned long int const*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned long int const*"; }
};

template<>
struct TypeName<signed long long int const*>
{
    typedef signed long long int const* type;

    static char const* GetName() throw() { return "signed long long int const*"; }
    static wchar_t const* GetWideName() throw() { return L"signed long long int const*"; }
};

template<>
struct TypeName<unsigned long long int const*>
{
    typedef unsigned long long int const* type;

    static char const* GetName() throw() { return "unsigned long long int const*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned long long int const*"; }
};

template<>
struct TypeName<float const*>
{
    typedef float const* type;

    static char const* GetName() throw() { return "float const*"; }
    static wchar_t const* GetWideName() throw() { return L"float const*"; }
};

template<>
struct TypeName<double const*>
{
    typedef double const* type;

    static char const* GetName() throw() { return "double const*"; }
    static wchar_t const* GetWideName() throw() { return L"double const*"; }
};

template<>
struct TypeName<long double const*>
{
    typedef long double const* type;

    static char const* GetName() throw() { return "long double const*"; }
    static wchar_t const* GetWideName() throw() { return L"long double const*"; }
};

template<>
struct TypeName<void const*>
{
    typedef void const* type;

    static char const* GetName() throw() { return "void const*"; }
    static wchar_t const* GetWideName() throw() { return L"void const*"; }
};

template<>
struct TypeName<bool*>
{
    typedef bool* type;

    static char const* GetName() throw() { return "bool*"; }
    static wchar_t const* GetWideName() throw() { return L"bool*"; }
};

template<>
struct TypeName<char*>
{
    typedef char* type;

    static char const* GetName() throw() { return "char*"; }
    static wchar_t const* GetWideName() throw() { return L"char*"; }
};

template<>
struct TypeName<wchar_t*>
{
    typedef wchar_t* type;

    static char const* GetName() throw() { return "wchar_t*"; }
    static wchar_t const* GetWideName() throw() { return L"wchar_t*"; }
};

template<>
struct TypeName<signed char*>
{
    typedef signed char* type;

    static char const* GetName() throw() { return "signed char*"; }
    static wchar_t const* GetWideName() throw() { return L"signed char*"; }
};

template<>
struct TypeName<unsigned char*>
{
    typedef unsigned char* type;

    static char const* GetName() throw() { return "unsigned char*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned char*"; }
};

template<>
struct TypeName<signed short*>
{
    typedef signed short* type;

    static char const* GetName() throw() { return "signed short*"; }
    static wchar_t const* GetWideName() throw() { return L"signed short*"; }
};

template<>
struct TypeName<unsigned short*>
{
    typedef unsigned short* type;

    static char const* GetName() throw() { return "unsigned short*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned short*"; }
};

template<>
struct TypeName<signed int*>
{
    typedef signed int* type;

    static char const* GetName() throw() { return "signed int*"; }
    static wchar_t const* GetWideName() throw() { return L"signed int*"; }
};

template<>
struct TypeName<unsigned int*>
{
    typedef unsigned int* type;

    static char const* GetName() throw() { return "unsigned int*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned int*"; }
};

template<>
struct TypeName<signed long int*>
{
    typedef signed long int* type;

    static char const* GetName() throw() { return "signed long int*"; }
    static wchar_t const* GetWideName() throw() { return L"signed long int*"; }
};

template<>
struct TypeName<unsigned long int*>
{
    typedef unsigned long int* type;

    static char const* GetName() throw() { return "unsigned long int*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned long int*"; }
};

template<>
struct TypeName<signed long long int*>
{
    typedef signed long long int* type;

    static char const* GetName() throw() { return "signed long long int*"; }
    static wchar_t const* GetWideName() throw() { return L"signed long long int*"; }
};

template<>
struct TypeName<unsigned long long int*>
{
    typedef unsigned long long int* type;

    static char const* GetName() throw() { return "unsigned long long int*"; }
    static wchar_t const* GetWideName() throw() { return L"unsigned long long int*"; }
};

template<>
struct TypeName<float*>
{
    typedef float* type;

    static char const* GetName() throw() { return "float*"; }
    static wchar_t const* GetWideName() throw() { return L"float*"; }
};

template<>
struct TypeName<double*>
{
    typedef double* type;

    static char const* GetName() throw() { return "double*"; }
    static wchar_t const* GetWideName() throw() { return L"double*"; }
};

template<>
struct TypeName<long double*>
{
    typedef long double* type;

    static char const* GetName() throw() { return "long double*"; }
    static wchar_t const* GetWideName() throw() { return L"long double*"; }
};

template<>
struct TypeName<void*>
{
    typedef void* type;

    static char const* GetName() throw() { return "void*"; }
    static wchar_t const* GetWideName() throw() { return L"void*"; }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPENAME_HPP
