# typeinfo

![](header_image.jpg)

## Overview

Type information template classes for C++

There will be overlap between this project and the standard library, such as some parts of type traits.

## Code Examples

```cpp

#include "../TypeIn.hpp"
#include "../TypeLimits.hpp"
#include "../TypeSizeNext.hpp"
#include <cstdint>
#include <string>
#include <iostream>

template<typename T>
struct Maths
{
    /// If T was a uint16_t then next_type would be a uint32_t.
    /// The signed or unsigned property will not change.
    ///
    /// If T was a int64_t or uint64_t then there would be a compiler error,
    /// as there is no bigger integer type.
    typedef typename ocl::TypeSizeNext<T>::next_type next_type;

    // Always add two values, that cannot overflow.
    static next_type SafeAdd(T a, T b) noexcept
    {
        return static_cast<next_type>(a) + static_cast<next_type>(b);
    }

    /// If you want the return type to be the same as the input, it's also possible
    /// to provide an alternative safe function that will return true or false,
    /// and return the result as a parameter.'
    static bool SafeAdd(T a, T b, T& res) noexcept
    {
        next_type temp = static_cast<next_type>(a) + static_cast<next_type>(b);
        res = static_cast<T>(temp);

        // Note you could also use std::numeric_limits<T>::min() and
        // std::numeric_limits<T>::max(), but some 3rd party libraries
        // #define min and max, and some compilers fail to #undef these macros.
        // TypeLimits offers an alternative for these situations.
        return (temp >= ocl::TypeLimits<T>::GetMin()) && (temp <= ocl::TypeLimits<T>::GetMax());
    }
};

template<typename T>
struct Algorithms
{
    typedef typename ocl::TypeIn<T>::in_type in_type;

    /// If T was an object like std::string, then in_type would be defined as const std::string&,
    /// otherwise if the type was a primitive type like an int, it would be defined as const int.
    static T GetMin(in_type a, in_type b)
    {
        return a < b ? a : b;
    }
};

int main(int /*argc*/, char * /*argv*/[])
{
    std::int32_t a = INT_MAX;
    std::int32_t b = INT_MAX;

    // next_type would be deduced at compile time to be std::uint64_t.
    Maths<int>::next_type res = Maths<int>::SafeAdd(a, b);
    std::string min_str = Algorithms<std::string>::GetMin("A", "B");

    // 2147483647 + 2147483647 = 4294967294 would overflow int32_t, but will fit int64_t.
    std::cout << res << std::endl;
    std::cout << min_str.c_str() << std::endl;

    // This example will overflow an int.
    int r;
    if (Maths<int>::SafeAdd(a, b, r))
        std::cout << r << std::endl;
    else
        std::cout << "Overflow!" << std::endl;

    return 0;
}

```
