/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEIN_HPP
#define OCL_GUARD_TYPEINFO_TYPEIN_HPP

#include "internal\InternalTypeIn.hpp"
#include "TypeRemoveConst.hpp"
#include "TypePod.hpp"

namespace ocl
{

template<typename Type>
struct TypeIn
{
private:
    // When the type is large or not a pod type, it will be passed as a const reference.
    static bool const is_large = sizeof(Type) > sizeof(void*);
    static bool const is_pod = TypePod<Type>::is_pod;
    static bool const is_const_ref = is_large && !is_pod;

public:
    // The in type will be a local value, which can be modified,
    // although pointers cannot modify values.
    typedef typename InternalTypeIn<Type, is_const_ref>::in_type in_type;
};

template<typename Type>
struct TypeIn<Type&>
{
    // Remove reference and continue to define in type through other versions of TypeIn.
    typedef typename TypeIn<Type>::in_type in_type;
};

template<typename Type>
struct TypeIn<Type*>
{
    // Ensure in type cannot modify what is being pointed too.
    typedef typename Type const* in_type;
};

template<typename Type>
struct TypeIn<Type const>
{
    // Ensure in type cannot modify what is being pointed too (keeping the const),
    typedef typename TypeIn<Type>::in_type const in_type;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEIN_HPP
