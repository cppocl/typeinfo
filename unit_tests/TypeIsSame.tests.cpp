/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeIsSame.hpp"

using ocl::TypeIsSame;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeIsSame, is_same, NA)
{
    bool is_same;

    // test matches.
    is_same = TypeIsSame<bool, bool>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<char, char>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<wchar_t, wchar_t>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<signed char, signed char>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<unsigned char, unsigned char>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<signed short, signed short>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<unsigned short, unsigned short>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<signed int, signed int>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<unsigned int, unsigned int>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<signed long int, signed long int>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<unsigned long int, unsigned long int>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<signed long long int, signed long long int>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<unsigned long long int, unsigned long long int>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<float, float>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<double, double>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<long double, long double>::is_same;
    CHECK_TRUE(is_same);
    is_same = TypeIsSame<Huge, Huge>::is_same;
    CHECK_TRUE(is_same);

    // test not matching.
    is_same = TypeIsSame<char, wchar_t>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<char, signed char>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<signed char, char>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<char, unsigned char>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<unsigned char, char>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<int, bool>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<signed int, float>::is_same;
    CHECK_FALSE(is_same);
    is_same = TypeIsSame<double, float>::is_same;
    CHECK_FALSE(is_same);
}
