/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEISSIGNSAME_HPP
#define OCL_GUARD_TYPEINFO_TYPEISSIGNSAME_HPP

#include "TypeSigned.hpp"
#include "TypeReference.hpp"

namespace ocl
{

template<typename Type1, typename Type2>
struct TypeIsSignSame
{
    typedef typename TypeReference<Type1>::no_ref_type type1;
    typedef typename TypeReference<Type2>::no_ref_type type2;

    // Only consider primitive numeric or char types as comparable.
    static const bool is_sign_same = TypeSigned<type1>::is_sign_known &&
                                     TypeSigned<type2>::is_sign_known &&
                                     TypeSigned<type1>::is_signed == TypeSigned<type2>::is_signed;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEISSIGNSAME_HPP
