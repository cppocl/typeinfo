/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeLimits.hpp"
#include "../TypeIncrement.hpp"

using ocl::TypeLimits;
using ocl::TypeIncrement;

TEST_MEMBER_FUNCTION(TypeIncrement, Increment, type_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME("Increment<type, void>");
    TEST_OVERRIDE_ARGS("type&");

    {
        typedef signed char type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned char type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed short type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned short type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long long int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long long int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef float type;

        type value = 0.0f;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1.0f);
    }

    {
        typedef double type;

        type value = 0.0;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1.0);
    }

    {
        typedef long double type;

        type value = 0.0l;
        TypeIncrement<type>::Increment(value);
        CHECK_EQUAL(value, 1.0l);
    }
}

TEST_MEMBER_FUNCTION(TypeIncrement, Increment_bool, type_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME("Increment<type, bool>");
    TEST_OVERRIDE_ARGS("type&");

    {
        typedef signed char type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned char type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed short type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned short type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef float type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0.0f;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1.0f);
    }

    {
        typedef double type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0.0;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1.0);
    }

    {
        typedef long double type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0.0l;
        CHECK_TRUE(type_increment_type::Increment(value));
        CHECK_EQUAL(value, 1.0l);
    }
}

TEST_MEMBER_FUNCTION(TypeIncrement, Increment, type_ref_type)
{
    TEST_OVERRIDE_FUNCTION_NAME("Increment<type, void>");
    TEST_OVERRIDE_ARGS("type&, type");

    {
        typedef signed char type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned char type;

        type value = 0;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, 1U);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed short type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned short type;

        type value = 0;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, 1U);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned int type;

        type value = 0;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, 1U);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long int type;

        type value = 0;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, 1UL);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long long int type;

        type value = TypeLimits<type>::MIN;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long long int type;

        type value = 0;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, 1ULL);

        value = TypeLimits<type>::MAX - 1;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        TypeIncrement<type>::Increment(value, 2U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        TypeIncrement<type>::Increment(value, 1U);
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef float type;

        type value = 0.0f;
        TypeIncrement<type>::Increment(value, 1.0f);
        CHECK_EQUAL(value, 1.0f);
    }

    {
        typedef double type;

        type value = 0.0;
        TypeIncrement<type>::Increment(value, 1.0);
        CHECK_EQUAL(value, 1.0);
    }

    {
        typedef long double type;

        type value = 0.0l;
        TypeIncrement<type>::Increment(value, 1.0l);
        CHECK_EQUAL(value, 1.0l);
    }
}

TEST_MEMBER_FUNCTION(TypeIncrement, Increment_bool, type_ref_type)
{
    TEST_OVERRIDE_FUNCTION_NAME("Increment<type, bool>");
    TEST_OVERRIDE_ARGS("type&, type");

    {
        typedef signed char type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned char type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, 1U);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed short type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned short type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, 1U);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, 1U);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, 1UL);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef signed long long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = TypeLimits<type>::MIN;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MIN + 1);

        value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, 1);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef unsigned long long int type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, 1ULL);

        value = TypeLimits<type>::MAX - 1;
        CHECK_TRUE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX - 2;
        CHECK_TRUE(type_increment_type::Increment(value, 2U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);

        value = TypeLimits<type>::MAX;
        CHECK_FALSE(type_increment_type::Increment(value, 1U));
        CHECK_EQUAL(value, TypeLimits<type>::MAX);
    }

    {
        typedef float type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0.0f;
        CHECK_TRUE(type_increment_type::Increment(value, 1.0f));
        CHECK_EQUAL(value, 1.0f);
    }

    {
        typedef double type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0.0;
        CHECK_TRUE(type_increment_type::Increment(value, 1.0));
        CHECK_EQUAL(value, 1.0);
    }

    {
        typedef long double type;
        typedef TypeIncrement<type, bool> type_increment_type;

        type value = 0.0l;
        CHECK_TRUE(type_increment_type::Increment(value, 1.0l));
        CHECK_EQUAL(value, 1.0l);
    }
}
