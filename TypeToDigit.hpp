/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPETODIGIT_HPP
#define OCL_GUARD_TYPEINFO_TYPETODIGIT_HPP

#include "TypeIsInteger.hpp"
#include "TypeDigits.hpp"

namespace ocl
{

template<typename CharType>
struct TypeToDigit
{
    typedef CharType             char_type;
    typedef TypeDigits<CharType> type_digits_type;

    /// Return character from '0' to '9' or '\0' the value is outside the range of 0 to 9.
    template<typename IntType>
    static CharType ToDigit(IntType value) noexcept
    {
        switch (value)
        {
            case 0: return type_digits_type::Zero;
            case 1: return type_digits_type::One;
            case 2: return type_digits_type::Two;
            case 3: return type_digits_type::Three;
            case 4: return type_digits_type::Four;
            case 5: return type_digits_type::Five;
            case 6: return type_digits_type::Six;
            case 7: return type_digits_type::Seven;
            case 8: return type_digits_type::Eight;
            case 9: return type_digits_type::Nine;
        }
        return type_digits_type::Null;
    }

};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPETODIGIT_HPP
