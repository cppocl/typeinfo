/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEDIGITS_HPP
#define OCL_GUARD_TYPEINFO_TYPEDIGITS_HPP

namespace ocl
{

template<typename CharType>
struct TypeDigits
{
};

template<>
struct TypeDigits<char>
{
    typedef char char_type;

    static const char_type Null  = '\0';

    static const char_type Zero  = '0';
    static const char_type One   = '1';
    static const char_type Two   = '2';
    static const char_type Three = '3';
    static const char_type Four  = '4';
    static const char_type Five  = '5';
    static const char_type Six   = '6';
    static const char_type Seven = '7';
    static const char_type Eight = '8';
    static const char_type Nine  = '9';

    static constexpr char_type const* GetDigits() noexcept
    {
        return "0123456789";
    }
};

template<>
struct TypeDigits<wchar_t>
{
    typedef wchar_t char_type;

    static const char_type Null  = L'\0';

    static const char_type Zero  = L'0';
    static const char_type One   = L'1';
    static const char_type Two   = L'2';
    static const char_type Three = L'3';
    static const char_type Four  = L'4';
    static const char_type Five  = L'5';
    static const char_type Six   = L'6';
    static const char_type Seven = L'7';
    static const char_type Eight = L'8';
    static const char_type Nine  = L'9';

    static constexpr char_type const* GetDigits() noexcept
    {
        return L"0123456789";
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEDIGITS_HPP
