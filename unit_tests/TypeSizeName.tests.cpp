/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeName.hpp"
#include <string.h>

using ocl::TypeSizeName;

namespace
{

int widecmp(wchar_t const* str1, wchar_t const* str2)
{
    while ((*str1 != L'\0') && (*str2 != L'\0'))
    {
        ++str1;
        ++str2;
    }
    return static_cast<int>(*str1 - *str2);
}

}

TEST_MEMBER_FUNCTION(TypeSizeName, GetName, NA)
{
    char const* null_ptr = NULL;

    CHECK_EQUAL(TypeSizeName<bool>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<char>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<wchar_t>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<float>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<double>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<long double>::GetName(), null_ptr);
    CHECK_EQUAL(::strcmp(TypeSizeName<int8_t>::GetName(), "int8_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint8_t>::GetName(), "uint8_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int16_t>::GetName(), "int16_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint16_t>::GetName(), "uint16_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int32_t>::GetName(), "int32_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint32_t>::GetName(), "uint32_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int64_t>::GetName(), "int64_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint64_t>::GetName(), "uint64_t"), 0);

    CHECK_EQUAL(TypeSizeName<bool const*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<char const*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<wchar_t const*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<float const*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<double const*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<long double const*>::GetName(), null_ptr);
    CHECK_EQUAL(::strcmp(TypeSizeName<int8_t const*>::GetName(), "int8_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint8_t const*>::GetName(), "uint8_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int16_t const*>::GetName(), "int16_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint16_t const*>::GetName(), "uint16_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int32_t const*>::GetName(), "int32_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint32_t const*>::GetName(), "uint32_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int64_t const*>::GetName(), "int64_t const*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint64_t const*>::GetName(), "uint64_t const*"), 0);

    CHECK_EQUAL(TypeSizeName<bool*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<char*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<wchar_t*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<float*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<double*>::GetName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<long double*>::GetName(), null_ptr);
    CHECK_EQUAL(::strcmp(TypeSizeName<int8_t*>::GetName(), "int8_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint8_t*>::GetName(), "uint8_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int16_t*>::GetName(), "int16_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint16_t*>::GetName(), "uint16_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int32_t*>::GetName(), "int32_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint32_t*>::GetName(), "uint32_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<int64_t*>::GetName(), "int64_t*"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<uint64_t*>::GetName(), "uint64_t*"), 0);
}

TEST_MEMBER_FUNCTION(TypeSizeName, GetWideName, NA)
{
    wchar_t const* null_ptr = NULL;

    CHECK_EQUAL(TypeSizeName<bool>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<char>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<wchar_t>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<float>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<double>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<long double>::GetWideName(), null_ptr);
    CHECK_EQUAL(widecmp(TypeSizeName<int8_t>::GetWideName(), L"int8_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint8_t>::GetWideName(), L"uint8_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int16_t>::GetWideName(), L"int16_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint16_t>::GetWideName(), L"uint16_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int32_t>::GetWideName(), L"int32_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint32_t>::GetWideName(), L"uint32_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int64_t>::GetWideName(), L"int64_t"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint64_t>::GetWideName(), L"uint64_t"), 0);

    CHECK_EQUAL(TypeSizeName<bool const*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<char const*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<wchar_t const*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<float const*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<double const*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<long double const*>::GetWideName(), null_ptr);
    CHECK_EQUAL(widecmp(TypeSizeName<int8_t const*>::GetWideName(), L"int8_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint8_t const*>::GetWideName(), L"uint8_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int16_t const*>::GetWideName(), L"int16_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint16_t const*>::GetWideName(), L"uint16_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int32_t const*>::GetWideName(), L"int32_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint32_t const*>::GetWideName(), L"uint32_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int64_t const*>::GetWideName(), L"int64_t const*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint64_t const*>::GetWideName(), L"uint64_t const*"), 0);

    CHECK_EQUAL(TypeSizeName<bool*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<char*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<wchar_t*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<float*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<double*>::GetWideName(), null_ptr);
    CHECK_EQUAL(TypeSizeName<long double*>::GetWideName(), null_ptr);
    CHECK_EQUAL(widecmp(TypeSizeName<int8_t*>::GetWideName(), L"int8_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint8_t*>::GetWideName(), L"uint8_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int16_t*>::GetWideName(), L"int16_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint16_t*>::GetWideName(), L"uint16_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int32_t*>::GetWideName(), L"int32_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint32_t*>::GetWideName(), L"uint32_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<int64_t*>::GetWideName(), L"int64_t*"), 0);
    CHECK_EQUAL(widecmp(TypeSizeName<uint64_t*>::GetWideName(), L"uint64_t*"), 0);
}
