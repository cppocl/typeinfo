/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeNumeric.hpp"

using ocl::TypeNumeric;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeNumeric, is_integer, NA)
{
    CHECK_FALSE(TypeNumeric<Huge>::is_integer);
    CHECK_FALSE(TypeNumeric<bool>::is_integer);
    CHECK_FALSE(TypeNumeric<char>::is_integer);
    CHECK_FALSE(TypeNumeric<wchar_t>::is_integer);
    CHECK_FALSE(TypeNumeric<float>::is_integer);
    CHECK_FALSE(TypeNumeric<double>::is_integer);
    CHECK_FALSE(TypeNumeric<long double>::is_integer);
    CHECK_TRUE(TypeNumeric<signed char>::is_integer);
    CHECK_TRUE(TypeNumeric<unsigned char>::is_integer);
    CHECK_TRUE(TypeNumeric<signed short>::is_integer);
    CHECK_TRUE(TypeNumeric<unsigned short>::is_integer);
    CHECK_TRUE(TypeNumeric<signed int>::is_integer);
    CHECK_TRUE(TypeNumeric<unsigned int>::is_integer);
    CHECK_TRUE(TypeNumeric<signed long int>::is_integer);
    CHECK_TRUE(TypeNumeric<unsigned long int>::is_integer);
    CHECK_TRUE(TypeNumeric<signed long long int>::is_integer);
    CHECK_TRUE(TypeNumeric<unsigned long long int>::is_integer);
}

TEST_MEMBER_FUNCTION(TypeNumeric, is_decimal, NA)
{
    CHECK_FALSE(TypeNumeric<Huge>::is_decimal);
    CHECK_FALSE(TypeNumeric<bool>::is_decimal);
    CHECK_FALSE(TypeNumeric<char>::is_decimal);
    CHECK_FALSE(TypeNumeric<wchar_t>::is_decimal);
    CHECK_TRUE(TypeNumeric<float>::is_decimal);
    CHECK_TRUE(TypeNumeric<double>::is_decimal);
    CHECK_TRUE(TypeNumeric<long double>::is_decimal);
    CHECK_FALSE(TypeNumeric<signed char>::is_decimal);
    CHECK_FALSE(TypeNumeric<unsigned char>::is_decimal);
    CHECK_FALSE(TypeNumeric<signed short>::is_decimal);
    CHECK_FALSE(TypeNumeric<unsigned short>::is_decimal);
    CHECK_FALSE(TypeNumeric<signed int>::is_decimal);
    CHECK_FALSE(TypeNumeric<unsigned int>::is_decimal);
    CHECK_FALSE(TypeNumeric<signed long int>::is_decimal);
    CHECK_FALSE(TypeNumeric<unsigned long int>::is_decimal);
    CHECK_FALSE(TypeNumeric<signed long long int>::is_decimal);
    CHECK_FALSE(TypeNumeric<unsigned long long int>::is_decimal);
}

TEST_MEMBER_FUNCTION(TypeNumeric, is_numeric, NA)
{
    CHECK_FALSE(TypeNumeric<Huge>::is_numeric);
    CHECK_FALSE(TypeNumeric<bool>::is_numeric);
    CHECK_FALSE(TypeNumeric<char>::is_numeric);
    CHECK_FALSE(TypeNumeric<wchar_t>::is_numeric);
    CHECK_TRUE(TypeNumeric<float>::is_numeric);
    CHECK_TRUE(TypeNumeric<double>::is_numeric);
    CHECK_TRUE(TypeNumeric<long double>::is_numeric);
    CHECK_TRUE(TypeNumeric<signed char>::is_numeric);
    CHECK_TRUE(TypeNumeric<unsigned char>::is_numeric);
    CHECK_TRUE(TypeNumeric<signed short>::is_numeric);
    CHECK_TRUE(TypeNumeric<unsigned short>::is_numeric);
    CHECK_TRUE(TypeNumeric<signed int>::is_numeric);
    CHECK_TRUE(TypeNumeric<unsigned int>::is_numeric);
    CHECK_TRUE(TypeNumeric<signed long int>::is_numeric);
    CHECK_TRUE(TypeNumeric<unsigned long int>::is_numeric);
    CHECK_TRUE(TypeNumeric<signed long long int>::is_numeric);
    CHECK_TRUE(TypeNumeric<unsigned long long int>::is_numeric);
}
