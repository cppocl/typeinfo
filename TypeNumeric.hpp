/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPENUMERIC_HPP
#define OCL_GUARD_TYPEINFO_TYPENUMERIC_HPP

#include "TypeInteger.hpp" // also includes TypeIsInteger.
#include "TypeDecimal.hpp" // also includes TypeIsDecimal.
#include "TypeIsNumeric.hpp"

namespace ocl
{

template<typename Type>
struct TypeNumeric
{
    typedef Type type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<signed char>
{
    typedef signed char                          type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<unsigned char>
{
    typedef unsigned char                        type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<signed short>
{
    typedef signed short                         type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<unsigned short>
{
    typedef unsigned short                       type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<signed int>
{
    typedef signed int                           type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<unsigned int>
{
    typedef unsigned int                         type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<signed long int>
{
    typedef signed long int                      type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<unsigned long int>
{
    typedef unsigned long int                    type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<signed long long int>
{
    typedef signed long long int                 type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<unsigned long long int>
{
    typedef unsigned long long int               type;
    typedef TypeInteger<type>::signed_type       signed_type;
    typedef TypeInteger<type>::unsigned_type     unsigned_type;
    typedef TypeInteger<type>::reverse_sign_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<float>
{
    typedef float                           type;
    typedef TypeDecimal<type>::decimal_type signed_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<double>
{
    typedef double                          type;
    typedef TypeDecimal<type>::decimal_type signed_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

template<>
struct TypeNumeric<long double>
{
    typedef long double                     type;
    typedef TypeDecimal<type>::decimal_type signed_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
    static bool const is_decimal = TypeIsDecimal<type>::is_decimal;
    static bool const is_numeric = TypeIsNumeric<type>::is_numeric;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPENUMERIC_HPP
