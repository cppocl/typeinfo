/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeHasPrevious.hpp"

namespace
{

struct Huge
{
    double data[1000];
};

}

using ocl::TypeSizeHasPrevious;

TEST_MEMBER_FUNCTION(TypeSizeHasPrevious, has_previous, NA)
{
    CHECK_FALSE(TypeSizeHasPrevious<bool>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<char>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<wchar_t>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<int8_t>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<uint8_t>::has_previous);
    CHECK_TRUE(TypeSizeHasPrevious<int16_t>::has_previous);
    CHECK_TRUE(TypeSizeHasPrevious<uint16_t>::has_previous);
    CHECK_TRUE(TypeSizeHasPrevious<int32_t>::has_previous);
    CHECK_TRUE(TypeSizeHasPrevious<uint32_t>::has_previous);
    CHECK_TRUE(TypeSizeHasPrevious<int64_t>::has_previous);
    CHECK_TRUE(TypeSizeHasPrevious<uint64_t>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<float>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<double>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<long double>::has_previous);
    CHECK_FALSE(TypeSizeHasPrevious<Huge>::has_previous);
}
