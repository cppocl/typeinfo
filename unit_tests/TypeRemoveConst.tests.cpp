/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeRemoveConst.hpp"

using ocl::TypeRemoveConst;

TEST_MEMBER_FUNCTION(TypeRemoveReference, is_constant, NA)
{
    CHECK_TRUE(TypeRemoveConst<int const>::is_constant);
    CHECK_FALSE(TypeRemoveConst<int>::is_constant);

    typedef typename TypeRemoveConst<int const>::type no_const_type;
    CHECK_FALSE(TypeRemoveConst<no_const_type>::is_constant);
}
