/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEHASNEXT_HPP
#define OCL_GUARD_TYPEINFO_TYPEHASNEXT_HPP

namespace ocl
{

template<typename Type>
struct TypeHasNext
{
    typedef Type type;

    static bool const has_next = false;
};

template<>
struct TypeHasNext<signed char>
{
    typedef signed char type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<unsigned char>
{
    typedef unsigned char type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<signed short>
{
    typedef signed short type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<unsigned short>
{
    typedef unsigned short type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<signed int>
{
    typedef signed int type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<unsigned int>
{
    typedef unsigned int type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<signed long int>
{
    typedef signed long int type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<unsigned long int>
{
    typedef unsigned long int type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<float>
{
    typedef float type;

    static bool const has_next = true;
};

template<>
struct TypeHasNext<double>
{
    typedef double type;

    static bool const has_next = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEHASNEXT_HPP
