/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEHASPREVIOUS_HPP
#define OCL_GUARD_TYPEINFO_TYPEHASPREVIOUS_HPP

namespace ocl
{

template<typename Type>
struct TypeHasPrevious
{
    typedef Type type;

    static bool const has_previous = false;
};

template<>
struct TypeHasPrevious<signed short>
{
    typedef signed short type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<unsigned short>
{
    typedef unsigned short type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<signed int>
{
    typedef signed int type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<unsigned int>
{
    typedef unsigned int type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<signed long int>
{
    typedef signed long int type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<unsigned long int>
{
    typedef unsigned long int type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<signed long long int>
{
    typedef signed long long int type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<unsigned long long int>
{
    typedef unsigned long long int type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<double>
{
    typedef double type;

    static bool const has_previous = true;
};

template<>
struct TypeHasPrevious<long double>
{
    typedef long double type;

    static bool const has_previous = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEHASPREVIOUS_HPP
