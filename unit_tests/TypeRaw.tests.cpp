/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeRaw.hpp"

using ocl::TypeRaw;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeRaw, is_raw, NA)
{
    CHECK_TRUE(TypeRaw<Huge>::is_raw);
    CHECK_TRUE(TypeRaw<void>::is_raw);
    CHECK_TRUE(TypeRaw<bool>::is_raw);
    CHECK_TRUE(TypeRaw<char>::is_raw);
    CHECK_TRUE(TypeRaw<wchar_t>::is_raw);
    CHECK_TRUE(TypeRaw<signed char>::is_raw);
    CHECK_TRUE(TypeRaw<unsigned char>::is_raw);
    CHECK_TRUE(TypeRaw<signed short>::is_raw);
    CHECK_TRUE(TypeRaw<unsigned short>::is_raw);
    CHECK_TRUE(TypeRaw<signed int>::is_raw);
    CHECK_TRUE(TypeRaw<unsigned int>::is_raw);
    CHECK_TRUE(TypeRaw<signed long int>::is_raw);
    CHECK_TRUE(TypeRaw<unsigned long int>::is_raw);
    CHECK_TRUE(TypeRaw<signed long long int>::is_raw);
    CHECK_TRUE(TypeRaw<unsigned long long int>::is_raw);
    CHECK_TRUE(TypeRaw<float>::is_raw);
    CHECK_TRUE(TypeRaw<double>::is_raw);
    CHECK_TRUE(TypeRaw<long double>::is_raw);

    CHECK_FALSE(TypeRaw<Huge const>::is_raw);
    CHECK_FALSE(TypeRaw<bool const>::is_raw);
    CHECK_FALSE(TypeRaw<char const>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t const>::is_raw);
    CHECK_FALSE(TypeRaw<signed char const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char const>::is_raw);
    CHECK_FALSE(TypeRaw<signed short const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short const>::is_raw);
    CHECK_FALSE(TypeRaw<signed int const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int const>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int const>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int const>::is_raw);
    CHECK_FALSE(TypeRaw<float const>::is_raw);
    CHECK_FALSE(TypeRaw<double const>::is_raw);
    CHECK_FALSE(TypeRaw<long double const>::is_raw);

    CHECK_FALSE(TypeRaw<Huge volatile>::is_raw);
    CHECK_FALSE(TypeRaw<bool volatile>::is_raw);
    CHECK_FALSE(TypeRaw<char volatile>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t volatile>::is_raw);
    CHECK_FALSE(TypeRaw<signed char volatile>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char volatile>::is_raw);
    CHECK_FALSE(TypeRaw<signed short volatile>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short volatile>::is_raw);
    CHECK_FALSE(TypeRaw<signed int volatile>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int volatile>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int volatile>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int volatile>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int volatile>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int volatile>::is_raw);
    CHECK_FALSE(TypeRaw<float volatile>::is_raw);
    CHECK_FALSE(TypeRaw<double volatile>::is_raw);
    CHECK_FALSE(TypeRaw<long double volatile>::is_raw);

    CHECK_FALSE(TypeRaw<Huge&>::is_raw);
    CHECK_FALSE(TypeRaw<bool&>::is_raw);
    CHECK_FALSE(TypeRaw<char&>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t&>::is_raw);
    CHECK_FALSE(TypeRaw<signed char&>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char&>::is_raw);
    CHECK_FALSE(TypeRaw<signed short&>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short&>::is_raw);
    CHECK_FALSE(TypeRaw<signed int&>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int&>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int&>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int&>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int&>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int&>::is_raw);
    CHECK_FALSE(TypeRaw<float&>::is_raw);
    CHECK_FALSE(TypeRaw<double&>::is_raw);
    CHECK_FALSE(TypeRaw<long double&>::is_raw);

    CHECK_FALSE(TypeRaw<Huge*>::is_raw);
    CHECK_FALSE(TypeRaw<void*>::is_raw);
    CHECK_FALSE(TypeRaw<bool*>::is_raw);
    CHECK_FALSE(TypeRaw<char*>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t*>::is_raw);
    CHECK_FALSE(TypeRaw<signed char*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char*>::is_raw);
    CHECK_FALSE(TypeRaw<signed short*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short*>::is_raw);
    CHECK_FALSE(TypeRaw<signed int*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int*>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int*>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int*>::is_raw);
    CHECK_FALSE(TypeRaw<float*>::is_raw);
    CHECK_FALSE(TypeRaw<double*>::is_raw);
    CHECK_FALSE(TypeRaw<long double*>::is_raw);

    CHECK_FALSE(TypeRaw<Huge**>::is_raw);
    CHECK_FALSE(TypeRaw<void**>::is_raw);
    CHECK_FALSE(TypeRaw<bool**>::is_raw);
    CHECK_FALSE(TypeRaw<char**>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t**>::is_raw);
    CHECK_FALSE(TypeRaw<signed char**>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char**>::is_raw);
    CHECK_FALSE(TypeRaw<signed short**>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short**>::is_raw);
    CHECK_FALSE(TypeRaw<signed int**>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int**>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int**>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int**>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int**>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int**>::is_raw);
    CHECK_FALSE(TypeRaw<float**>::is_raw);
    CHECK_FALSE(TypeRaw<double**>::is_raw);
    CHECK_FALSE(TypeRaw<long double**>::is_raw);

    CHECK_FALSE(TypeRaw<Huge const*>::is_raw);
    CHECK_FALSE(TypeRaw<void const*>::is_raw);
    CHECK_FALSE(TypeRaw<bool const*>::is_raw);
    CHECK_FALSE(TypeRaw<char const*>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t const*>::is_raw);
    CHECK_FALSE(TypeRaw<signed char const*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char const*>::is_raw);
    CHECK_FALSE(TypeRaw<signed short const*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short const*>::is_raw);
    CHECK_FALSE(TypeRaw<signed int const*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int const*>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int const*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int const*>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int const*>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int const*>::is_raw);
    CHECK_FALSE(TypeRaw<float const*>::is_raw);
    CHECK_FALSE(TypeRaw<double const*>::is_raw);
    CHECK_FALSE(TypeRaw<long double const*>::is_raw);

    CHECK_FALSE(TypeRaw<Huge const* const>::is_raw);
    CHECK_FALSE(TypeRaw<void const* const>::is_raw);
    CHECK_FALSE(TypeRaw<bool const* const>::is_raw);
    CHECK_FALSE(TypeRaw<char const* const>::is_raw);
    CHECK_FALSE(TypeRaw<wchar_t const* const>::is_raw);
    CHECK_FALSE(TypeRaw<signed char const* const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned char const* const>::is_raw);
    CHECK_FALSE(TypeRaw<signed short const* const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned short const* const>::is_raw);
    CHECK_FALSE(TypeRaw<signed int const* const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned int const* const>::is_raw);
    CHECK_FALSE(TypeRaw<signed long int const* const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long int const* const>::is_raw);
    CHECK_FALSE(TypeRaw<signed long long int const* const>::is_raw);
    CHECK_FALSE(TypeRaw<unsigned long long int const* const>::is_raw);
    CHECK_FALSE(TypeRaw<float const* const>::is_raw);
    CHECK_FALSE(TypeRaw<double const* const>::is_raw);
    CHECK_FALSE(TypeRaw<long double const* const>::is_raw);
}

TEST_MEMBER_FUNCTION(TypeRaw, depth, NA)
{
    CHECK_EQUAL(TypeRaw<Huge>::depth, 1U);
    CHECK_EQUAL(TypeRaw<void>::depth, 1U);
    CHECK_EQUAL(TypeRaw<bool>::depth, 1U);
    CHECK_EQUAL(TypeRaw<char>::depth, 1U);
    CHECK_EQUAL(TypeRaw<wchar_t>::depth, 1U);
    CHECK_EQUAL(TypeRaw<signed char>::depth, 1U);
    CHECK_EQUAL(TypeRaw<unsigned char>::depth, 1U);
    CHECK_EQUAL(TypeRaw<signed short>::depth, 1U);
    CHECK_EQUAL(TypeRaw<unsigned short>::depth, 1U);
    CHECK_EQUAL(TypeRaw<signed int>::depth, 1U);
    CHECK_EQUAL(TypeRaw<unsigned int>::depth, 1U);
    CHECK_EQUAL(TypeRaw<signed long int>::depth, 1U);
    CHECK_EQUAL(TypeRaw<unsigned long int>::depth, 1U);
    CHECK_EQUAL(TypeRaw<signed long long int>::depth, 1U);
    CHECK_EQUAL(TypeRaw<unsigned long long int>::depth, 1U);
    CHECK_EQUAL(TypeRaw<float>::depth, 1U);
    CHECK_EQUAL(TypeRaw<double>::depth, 1U);
    CHECK_EQUAL(TypeRaw<long double>::depth, 1U);

    CHECK_EQUAL(TypeRaw<Huge*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<void*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<bool*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<char*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<wchar_t*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed char*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned char*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed short*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned short*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed long long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned long long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<float*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<double*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<long double*>::depth, 2U);

    CHECK_EQUAL(TypeRaw<Huge*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<void*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<bool*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<char*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<wchar_t*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed char*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned char*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed short*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned short*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed long long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned long long int*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<float*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<double*>::depth, 2U);
    CHECK_EQUAL(TypeRaw<long double*>::depth, 2U);

    CHECK_EQUAL(TypeRaw<Huge&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<bool&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<char&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<wchar_t&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed char&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned char&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed short&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned short&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed int&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned int&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed long int&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned long int&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<signed long long int&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<unsigned long long int&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<float&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<double&>::depth, 2U);
    CHECK_EQUAL(TypeRaw<long double&>::depth, 2U);

    CHECK_EQUAL(TypeRaw<Huge**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<void**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<bool**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<char**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<wchar_t**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed char**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned char**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed short**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned short**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed int**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned int**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed long int**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned long int**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed long long int**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned long long int**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<float**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<double**>::depth, 3U);
    CHECK_EQUAL(TypeRaw<long double**>::depth, 3U);

    CHECK_EQUAL(TypeRaw<Huge const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<void const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<bool const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<char const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<wchar_t const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed char const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned char const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed short const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned short const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed int const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned int const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed long int const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned long int const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed long long int const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned long long int const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<float const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<double const*>::depth, 3U);
    CHECK_EQUAL(TypeRaw<long double const*>::depth, 3U);

    CHECK_EQUAL(TypeRaw<Huge const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<bool const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<char const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<wchar_t const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed char const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned char const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed short const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned short const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed int const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned int const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed long int const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned long int const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<signed long long int const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<unsigned long long int const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<float const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<double const&>::depth, 3U);
    CHECK_EQUAL(TypeRaw<long double const&>::depth, 3U);

    CHECK_EQUAL(TypeRaw<Huge const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<void const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<bool const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<char const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<wchar_t const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<signed char const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<unsigned char const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<signed short const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<unsigned short const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<signed int const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<unsigned int const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<signed long int const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<unsigned long int const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<signed long long int const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<unsigned long long int const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<float const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<double const* const>::depth, 4U);
    CHECK_EQUAL(TypeRaw<long double const* const>::depth, 4U);
}
