/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeName.hpp"
#include "../TypeSizeNext.hpp"
#include <string.h>

using ocl::TypeSizeNext;
using ocl::TypeSizeName;

TEST_MEMBER_FUNCTION(TypeSizeNext, next_type, NA)
{
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizeNext<int8_t>::next_type>::GetName(), "int16_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizeNext<uint8_t>::next_type>::GetName(), "uint16_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizeNext<int16_t>::next_type>::GetName(), "int32_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizeNext<uint16_t>::next_type>::GetName(), "uint32_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizeNext<int32_t>::next_type>::GetName(), "int64_t"), 0);
    CHECK_EQUAL(::strcmp(TypeSizeName<TypeSizeNext<uint32_t>::next_type>::GetName(), "uint64_t"), 0);
}
