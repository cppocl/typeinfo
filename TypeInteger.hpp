/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEINTEGER_HPP
#define OCL_GUARD_TYPEINFO_TYPEINTEGER_HPP

#include "TypeIsInteger.hpp"

namespace ocl
{

template<typename Type>
struct TypeInteger
{
    typedef Type type;

    static bool const is_integer = TypeIsInteger<type>::is_integer; // is integer type?
};

template<>
struct TypeInteger<signed char>
{
    typedef signed char   type;
    typedef type          signed_type;
    typedef unsigned char unsigned_type;
    typedef unsigned_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<unsigned char>
{
    typedef unsigned char type;
    typedef signed char   signed_type;
    typedef type          unsigned_type;
    typedef signed_type   reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<signed short>
{
    typedef signed short   type;
    typedef type           signed_type;
    typedef unsigned short unsigned_type;
    typedef unsigned_type  reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<unsigned short>
{
    typedef unsigned short type;
    typedef signed short   signed_type;
    typedef type           unsigned_type;
    typedef signed_type    reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<signed int>
{
    typedef signed int    type;
    typedef type          signed_type;
    typedef unsigned int  unsigned_type;
    typedef unsigned_type reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<unsigned int>
{
    typedef unsigned int type;
    typedef signed int   signed_type;
    typedef type         unsigned_type;
    typedef signed_type  reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<signed long int>
{
    typedef signed long int   type;
    typedef type              signed_type;
    typedef unsigned long int unsigned_type;
    typedef unsigned_type     reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<unsigned long int>
{
    typedef unsigned long int type;
    typedef signed long int   signed_type;
    typedef type              unsigned_type;
    typedef signed_type       reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<signed long long int>
{
    typedef signed long long int   type;
    typedef type                   signed_type;
    typedef unsigned long long int unsigned_type;
    typedef unsigned_type          reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

template<>
struct TypeInteger<unsigned long long int>
{
    typedef unsigned long long int type;
    typedef signed long long int   signed_type;
    typedef type                   unsigned_type;
    typedef signed_type            reverse_sign_type;

    static bool const is_integer = TypeIsInteger<type>::is_integer;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEINTEGER_HPP
