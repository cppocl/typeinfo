/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeDecimal.hpp"

using ocl::TypeDecimal;

namespace
{

struct Huge
{
    double data[1000];
};

}

TEST_MEMBER_FUNCTION(TypeDecimal, is_decimal, NA)
{
    CHECK_FALSE(TypeDecimal<Huge>::is_decimal);
    CHECK_FALSE(TypeDecimal<bool>::is_decimal);
    CHECK_FALSE(TypeDecimal<char>::is_decimal);
    CHECK_FALSE(TypeDecimal<wchar_t>::is_decimal);
    CHECK_TRUE(TypeDecimal<float>::is_decimal);
    CHECK_TRUE(TypeDecimal<double>::is_decimal);
    CHECK_TRUE(TypeDecimal<long double>::is_decimal);
    CHECK_FALSE(TypeDecimal<signed char>::is_decimal);
    CHECK_FALSE(TypeDecimal<unsigned char>::is_decimal);
    CHECK_FALSE(TypeDecimal<signed short>::is_decimal);
    CHECK_FALSE(TypeDecimal<unsigned short>::is_decimal);
    CHECK_FALSE(TypeDecimal<signed int>::is_decimal);
    CHECK_FALSE(TypeDecimal<unsigned int>::is_decimal);
    CHECK_FALSE(TypeDecimal<signed long int>::is_decimal);
    CHECK_FALSE(TypeDecimal<unsigned long int>::is_decimal);
    CHECK_FALSE(TypeDecimal<signed long long int>::is_decimal);
    CHECK_FALSE(TypeDecimal<unsigned long long int>::is_decimal);
}
