/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeHasPrevious.hpp"

namespace
{

struct Huge
{
    double data[1000];
};

}

using ocl::TypeHasPrevious;

TEST_MEMBER_FUNCTION(TypeHasPrevious, has_previous, NA)
{
    CHECK_FALSE(TypeHasPrevious<bool>::has_previous);
    CHECK_FALSE(TypeHasPrevious<char>::has_previous);
    CHECK_FALSE(TypeHasPrevious<wchar_t>::has_previous);
    CHECK_FALSE(TypeHasPrevious<signed char>::has_previous);
    CHECK_FALSE(TypeHasPrevious<unsigned char>::has_previous);
    CHECK_TRUE(TypeHasPrevious<signed short>::has_previous);
    CHECK_TRUE(TypeHasPrevious<unsigned short>::has_previous);
    CHECK_TRUE(TypeHasPrevious<signed int>::has_previous);
    CHECK_TRUE(TypeHasPrevious<unsigned int>::has_previous);
    CHECK_TRUE(TypeHasPrevious<signed long int>::has_previous);
    CHECK_TRUE(TypeHasPrevious<unsigned long int>::has_previous);
    CHECK_TRUE(TypeHasPrevious<signed long long int>::has_previous);
    CHECK_TRUE(TypeHasPrevious<unsigned long long int>::has_previous);
    CHECK_TRUE(TypeHasPrevious<double>::has_previous);
    CHECK_TRUE(TypeHasPrevious<long double>::has_previous);
    CHECK_FALSE(TypeHasPrevious<float>::has_previous);
    CHECK_FALSE(TypeHasPrevious<Huge>::has_previous);
}
