/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEDECREMENT_HPP
#define OCL_GUARD_TYPEINFO_TYPEDECREMENT_HPP

#include "TypeLimits.hpp"
#include <cfloat>

namespace ocl
{

/// Default implementation assumes integer types.
template<typename Type, typename RetType = void>
struct TypeDecrement
{
    typedef Type type;
    typedef RetType ret_type;

    // Decrement value by 1 without wrapping past the minimum value.
    static ret_type Decrement(type& value)
    {
        if (value > TypeLimits<type>::GetMin())
            --value;
    }

    // Decrement value without wrapping past the minimum value.
    static ret_type Decrement(type& value, type by)
    {
        if (value > TypeLimits<type>::GetMin() + by)
            value -= by;
        else
            value = TypeLimits<type>::GetMin();
    }
};

template<typename Type>
struct TypeDecrement<Type, bool>
{
    typedef Type type;
    typedef bool ret_type;

    // Decrement value by 1 without wrapping past the minimum value.
    static ret_type Decrement(type& value)
    {
        bool const success = value > TypeLimits<type>::GetMin();
        if (success)
            --value;
        return success;
    }

    // Decrement value without wrapping past the minimum value.
    static ret_type Decrement(type& value, type by)
    {
        bool const success = value >= TypeLimits<type>::GetMin() + by;
        if (success)
            value -= by;
        else
            value = TypeLimits<type>::GetMin();
        return success;
    }
};
} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEDECREMENT_HPP
