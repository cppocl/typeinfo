/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TypeSizeInteger.hpp"
#include "../TypeSigned.hpp"
#include <cstddef>

using ocl::TypeSizeInteger;
using ocl::TypeSigned;

TEST_MEMBER_FUNCTION(TypeSizeInteger, types, NA)
{
    {
        typedef typename TypeSizeInteger<8, true>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 8);
        CHECK_TRUE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<8, false>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 8);
        CHECK_FALSE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<16, true>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 16);
        CHECK_TRUE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<16, false>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 16);
        CHECK_FALSE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<32, true>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 32);
        CHECK_TRUE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<32, false>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 32);
        CHECK_FALSE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<64, true>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 64);
        CHECK_TRUE(TypeSigned<type>::is_signed);
    }

    {
        typedef typename TypeSizeInteger<64, false>::type type;
        std::size_t size = sizeof(type) * CHAR_BIT;
        CHECK_EQUAL(size, 64);
        CHECK_FALSE(TypeSigned<type>::is_signed);
    }
}
