/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPESIZEHASPREVIOUS_HPP
#define OCL_GUARD_TYPEINFO_TYPESIZEHASPREVIOUS_HPP

#include <cstdint>

namespace ocl
{

template<typename Type>
struct TypeSizeHasPrevious
{
    typedef Type type;

    static bool const has_previous = false;
};

template<>
struct TypeSizeHasPrevious<std::int16_t>
{
    typedef std::int16_t type;

    static bool const has_previous = true;
};

template<>
struct TypeSizeHasPrevious<std::uint16_t>
{
    typedef std::uint16_t type;

    static bool const has_previous = true;
};

template<>
struct TypeSizeHasPrevious<std::int32_t>
{
    typedef std::int32_t type;

    static bool const has_previous = true;
};

template<>
struct TypeSizeHasPrevious<std::uint32_t>
{
    typedef std::uint32_t type;

    static bool const has_previous = true;
};

template<>
struct TypeSizeHasPrevious<std::int64_t>
{
    typedef std::int64_t type;

    static bool const has_previous = true;
};

template<>
struct TypeSizeHasPrevious<std::uint64_t>
{
    typedef std::int64_t type;

    static bool const has_previous = true;
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPESIZEHASPREVIOUS_HPP
