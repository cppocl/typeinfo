/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPELIMITS_HPP
#define OCL_GUARD_TYPEINFO_TYPELIMITS_HPP

#include "TypeSizeLimits.hpp"
#include "TypeSigned.hpp"
#include <cstddef>
#include <cfloat>

namespace ocl
{

/// Define MIN, MAX, BITS and 
template<typename Type>
struct TypeLimits
{
    typedef Type type;

    static bool const is_signed       = TypeSigned<type>::is_signed;
    static std::size_t const NUM_BITS = sizeof(type) * CHAR_BIT;

    static type const   MIN           = static_cast<type>(TypeSizeLimits<NUM_BITS, is_signed>::MIN);
    static type const   MAX           = static_cast<type>(TypeSizeLimits<NUM_BITS, is_signed>::MAX);
    static type const   TOP_BIT       = static_cast<type>(TypeSizeLimits<NUM_BITS, is_signed>::TOP_BIT);
    static size_t const MAX_CHARS     = static_cast<type>(TypeSizeLimits<NUM_BITS, is_signed>::MAX_CHARS);

    static constexpr type GetMin() noexcept
    {
        return MIN;
    }

    static constexpr type GetMax() noexcept
    {
        return MAX;
    }

    /// Return the smallest number of bytes required to store the value.
    static constexpr std::size_t GetByteCount(type value) noexcept
    {
        return TypeSizeLimits<NUM_BITS, is_signed>::GetByteCount(value);
    }
};

template<>
struct TypeLimits<float>
{
    typedef float type;

    static bool const is_signed = TypeSigned<type>::is_signed;
    static std::size_t const NUM_BITS = sizeof(type) * CHAR_BIT;

    static constexpr type GetMin() noexcept
    {
        return FLT_MAX * -1.0f;
    }

    static constexpr type GetMax() noexcept
    {
        return FLT_MAX;
    }
};

template<>
struct TypeLimits<double>
{
    typedef double type;

    static bool const is_signed = TypeSigned<type>::is_signed;
    static std::size_t const NUM_BITS = sizeof(type) * CHAR_BIT;

    static constexpr type GetMin() noexcept
    {
        return DBL_MAX * -1.0;
    }

    static constexpr type GetMax() noexcept
    {
        return DBL_MAX;
    }
};

template<>
struct TypeLimits<long double>
{
    typedef long double type;

    static bool const is_signed = TypeSigned<type>::is_signed;
    static std::size_t const NUM_BITS = sizeof(type) * CHAR_BIT;

    static constexpr type GetMin() noexcept
    {
        return LDBL_MAX * -1.0l;
    }

    static constexpr type GetMax() noexcept
    {
        return LDBL_MAX;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPELIMITS_HPP
