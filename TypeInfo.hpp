/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPEINFO_TYPEINFO_HPP
#define OCL_GUARD_TYPEINFO_TYPEINFO_HPP

#include "TypeIn.hpp"
#include "TypePod.hpp"
#include "TypeRaw.hpp"
#include "TypeConstant.hpp"
#include "TypePointer.hpp"
#include "TypeReference.hpp"
#include "TypeNumeric.hpp"
#include "TypeName.hpp"
#include "TypeHasNext.hpp"
#include "TypeHasPrevious.hpp"

namespace ocl
{

template<typename Type>
struct TypeInfo
{
// Types.
public:
    typedef Type                                                type;
    typedef typename TypeRaw<type>::raw_type                    raw_type;
    typedef typename TypeReference<type>::ref_type              ref_type;
    typedef typename TypeReference<type>::no_ref_type           no_ref_type;
    typedef typename TypeConstant<no_ref_type>::const_type      const_type;
    typedef typename TypeConstant<no_ref_type>::no_const_type   no_const_type;

    // For large type in_type is a const reference type, otherwise its a local value type.
    // If the type was previously const, then this is retained for in_type.
    typedef typename TypeIn<Type>::in_type                      in_type;

    typedef no_ref_type&                                        out_type;
    typedef out_type                                            in_out_type;
    typedef in_type                                             return_type;

// Constants.
public:
    static bool const is_large     = sizeof(type) > sizeof(void*);

    static bool const is_raw       = TypeRaw<type>::is_raw;
    static bool const is_pod       = TypePod<type>::is_pod; // Is plain old data?
    static bool const is_constant  = TypeConstant<type>::is_constant;
    static bool const is_pointer   = TypePointer<type>::is_pointer;
    static bool const is_reference = TypeReference<type>::is_reference;

    static bool const has_previous = TypeHasPrevious<type>::has_previous;
    static bool const has_next     = TypeHasNext<type>::has_next;

    static bool const is_integer   = TypeIsNumeric<type>::is_integer;
    static bool const is_decimal   = TypeIsNumeric<type>::is_decimal;
    static bool const is_numeric   = TypeIsNumeric<type>::is_numeric;

// Functions.
public:
    static char const* GetName() throw()        { return TypeName<type>::GetName(); }
    static wchar_t const* GetWideName() throw() { return TypeName<type>::GetWideName(); }
};

} // namespace ocl

#endif // OCL_GUARD_TYPEINFO_TYPEINFO_HPP
